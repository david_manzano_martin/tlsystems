<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>
<div id="litterRegistrationDiv" class="container">
   <div class="row">
      <div class="col-12  text-center">
         <h1>Litter registration</h1>
      </div>
   </div>
   <form  class="row" enctype="multipart/form-data">
      <div class="col-12 ">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="basic-addon1">Date of birth</span>
            </div>
            <input type="date" name="dateBirth" class="form-control" placeholder="Date of birth">
         </div>
      </div>
      <div class="col-12 ">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="basic-addon1">Number of Bitches (Female)</span>
            </div>
            <input type="number" class="form-control" name="numberBitches" placeholder="Number of Bitches (Female)">
         </div>
      </div>
      <div class="col-12 ">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="basic-addon1">Number of Dogs (Male)</span>
            </div>
            <input type="number" class="form-control" name="numberDogs" placeholder="Number of Dogs (Male)">
         </div>
      </div>
      <div class="col-12 ">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="basic-addon1">Sire ID</span>
            </div>
            <input type="number" class="form-control" name="sireId" placeholder="Sire ID">
         </div>
      </div>
      <div class="col-12 ">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="basic-addon1">Dam ID</span>
            </div>
            <input type="number" class="form-control" name="damId" placeholder="Dam ID">
         </div>
      </div>
      <div class="col-12 ">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="basic-addon1">Date available</span>
            </div>
            <input type="date" class="form-control" name="dataAvailable" placeholder="Date available">
         </div>
      </div>
      <div class="col-12 ">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="basic-addon1">Area</span>
            </div>
            <input type="text" class="form-control" name="area" placeholder="Area">
         </div>
      </div>
      <div class="col-12 ">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
            </div>
            <div class="custom-file">
               <input type="file" multiple name="pictures" class="custom-file-input">
               <label class="custom-file-label" for="inputGroupFile01">Pictures (x10)</label>
            </div>
         </div>
      </div>
      <div class="col-12  text-center">
         <button type="button" class="btn btn-primary">Send</button>
      </div>
   </form>
</div>
<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>