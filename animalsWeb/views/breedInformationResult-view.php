<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>
<div id="breadResult" class="container">
   <div class="row" id="headerResult">
      <div class="col-12  text-center">
         <h1><?= $breedObject->getBreedName() ?></h1>
      </div>
   </div>
   <hr>
   <div id="bodyResult" class="row">
      <div id="sameHeightDiv" class="row">
         <div id="imgDiv" class="col-12  col-lg-4">
            <img src="<?= $breedObject->getFirstImage() ?>" alt="">
         </div>
         <div id="introductionParagraph" class="col-12  col-lg-8">
            <?= $breedObject->getFirstDescription() ?>
         </div>
      </div>
      <div id="imagePreview1" class="col-12">
         <img src="<?= $breedObject->getSecondImage() ?>" alt="">
      </div>
      <div id="descriptionParagraph" class="col-12">
         <?= $breedObject->getSecondDescription() ?>
      </div>
      <div class="row col-12">
         <div class="col-12 text-center">
            <h1>Health tests</h1>
         </div>
         <div class="col-12 row">
            <?php
               if(count($allTests)>0){
               
               ?>
            <div class="col-12 text-left">
               <h3>Required</h3>
            </div>
            <div class="col-12">
               <p>
                  The next health tests are recommended to ....
               </p>
            </div>
            <div class="col-12">
               <ul>
                  <?php
                     $currentHealthTestsArray = BreedHealthTestRelation::getHealthTestsByBreedByImportance($db, $breedObject, "Recommended");
                     foreach($currentHealthTestsArray as $localTest){
                     
                     echo("<li><a href='" . $_SERVER["PHP_SELF"] . "?page=viewHealthTest&name=" . $localTest->getHealthTestName() . "'>". $localTest->getHealthTestName() . "</a></li>");
                     
                     }
                     
                     ?>
               </ul>
            </div>
            <div class="col-12 text-left">
               <h3>Recommended</h3>
            </div>
            <div class="col-12">
               <p>
                  The next health tests are required to ....
               </p>
            </div>
            <div class="col-12">
               <ul>
                  <?php
                     $currentHealthTestsArray = BreedHealthTestRelation::getHealthTestsByBreedByImportance($db, $breedObject, "Required");
                     foreach($currentHealthTestsArray as $localTest){
                     
                     echo("<li><a href='" . $_SERVER["PHP_SELF"] . "?page=viewHealthTest&name=" . $localTest->getHealthTestName() . "'>". $localTest->getHealthTestName() . "</a></li>");
                     
                     }
                     
                     ?>
               </ul>
            </div>
            <div class="col-12 text-left">
               <h3>Could have</h3>
            </div>
            <div class="col-12">
               <p>
                  The next health tests are required to ....
               </p>
            </div>
            <div class="col-12">
               <ul>
                  <?php
                     $currentHealthTestsArray = BreedHealthTestRelation::getHealthTestsByBreedByImportance($db, $breedObject, "Could have");
                     foreach($currentHealthTestsArray as $localTest){
                     
                     echo("<li><a href='" . $_SERVER["PHP_SELF"] . "?page=viewHealthTest&name=" . $localTest->getHealthTestName() . "'>". $localTest->getHealthTestName() . "</a></li>");
                     
                     }
                     
                     ?>
               </ul>
            </div>
            <?php
               } else{

					?>
					
					<div class="col-12">
						<p>
							This breed doesn't has any health test.
						</p>
					</div>
					
					<?php
               
               }
               
               ?>
         </div>
      </div>
   </div>
</div>
<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>