<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>
<div id="breedRegistrationDiv" class="container">
   <div id="breedHeader" class="row text-center">
      <div class="col-12">
         <h1>Breeder Registration</h1>
      </div>
   </div>
   <div id="breedBody" class="row">
      <form class="container-fluid" method="POST" enctype="multipart/form-data">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Breed name</span>
               </div>
               <input type="text" name="breedName" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupFileAddon01">First image (vertical image)</span>
               </div>
               <div class="custom-file">
                  <input type="file" name="img1" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                  <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">First description</span>
               </div>
               <textarea class="form-control" name="firstDescription" aria-label="With textarea"></textarea>
            </div>
         </div>
         <br>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupFileAddon01">Second image (horizontal image)</span>
               </div>
               <div class="custom-file">
                  <input type="file" name="img2" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                  <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Second description</span>
               </div>
               <textarea class="form-control" name="secondDescription" aria-label="With textarea"></textarea>
            </div>
         </div>
         <br>
         <div class="col-12 text-center">
         	<button class="btn btn-primary">Save</button>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>