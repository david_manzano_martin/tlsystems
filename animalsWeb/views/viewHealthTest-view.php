<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>

<div class="container">
	<div class="row">
		<div class="col-12 text-center">
			<h1><?= $healthTests->getHealthTestName() ?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<p>
				<?= $healthTests->getHealthTestDescription() ?>
			</p>
		</div>
	</div>
</div>

<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>