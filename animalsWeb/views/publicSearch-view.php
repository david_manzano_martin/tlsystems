<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>
<div id="resultsDiv" class="container">
   <div class="row">
      <!-- TEST TO MAKE IT RESPONSIVE -->
      <div id="filterDiv" class="col-12  col-md-4 text-center" style="">
         <div class="col-12 ">
            <div class="row">
               <div class="col-10 col-md-12">
                  <h4>Filter options</h4>
               </div>
               <div class="col-2 col-md-0">
                  <button class="collapseIcon navbar-toggler" type="button" data-toggle="collapse" data-target="#filterOptions" aria-controls="filterOptions" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-filter"></i>
                  </button>
               </div>
            </div>
         </div>
         <hr>
         <div class="row" id="filterOptions">
            <div class="col-12 ">
               <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <span class="input-group-text" id="basic-addon1">Name</span>
                  </div>
                  <input type="text" class="form-control" placeholder="" aria-label="Username" aria-describedby="basic-addon1">
               </div>
            </div>
            <div class="col-12 ">
               <div class="input-group mb-3">
                  <div class="input-group-prepend">
                     <label class="input-group-text" for="inputGroupSelect01">Breed</label>
                  </div>
                  <select class="custom-select" id="inputGroupSelect01">
                     <option selected>Choose...</option>
                     <option value="1">One</option>
                     <option value="2">Two</option>
                     <option value="3">Three</option>
                  </select>
               </div>
            </div>
         </div>
      </div>
      <div class="col-12  col-md-8" style="">
         <?php
         
            for($k=0; $k<5; $k++) {
            	require(__DIR__ . "/partials/resultSearch-partial-view.php");
            }
            
            ?>
      </div>
   </div>
</div>
<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>