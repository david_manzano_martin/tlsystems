<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>
<div id="searcherDiv" class="container-fluid">
   <div class="jumbotron text-center">
      <h1 class="display-4">Find information of any breed!</h1>
      <hr class="my-4">
      
         <form id="" action="?" autocomplete="off" method="GET" class="row" >
            <div class="col-12 col-md-10 text-left">
            	<input type="hidden" name="page" value="breedInformationResult"></input>
               <input type="text" id="searcherInput" name="breaeName" class="form-control" placeholder="Write the breed's name" aria-label="Username" aria-describedby="basic-addon1">
               <div id="results">
                  
               </div>
            </div>
            <div class="col-12 col-md-2">
               <button class="btn btn-primary">Search</button>
            </div>
         </form>
      
   </div>
</div>
<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>