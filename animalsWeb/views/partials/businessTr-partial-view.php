<tr>
   <th>  </th>
   <td><?= $business->getCompanyName() ?></td>
   <td class="text-right">
      <i dataToggle="<?= $business->getCompanyName() ?>" class="btnShow visible fas fa-sort-amount-down"></i>
      <i dataToggle="<?= $business->getCompanyName() ?>" class="btnHide invisible fas fa-sort-amount-up"></i>
   </td>
</tr>
<tr class="invisible <?= $business->getCompanyName() ?> data">
   <td colspan="3">
      <iframe name="iframe" src="?page=iframeBusiness&id=<?= $business->getIdBusiness() ?>"></iframe>
   </td>
</tr>