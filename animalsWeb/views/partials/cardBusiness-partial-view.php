<div class="row container-fluid">
   <div class="imgContainer col-12 col-md-4 removePadding">
      <img src="<?= $business->getRoute() ?>" alt="">
   </div>
   <div class="col-12 col-md-8 removePadding">
      <div class="card text-center">
         <div class="card-header">
            <?= $business->getCompanyName() ?>
         </div>
         <div class="card-body">
            <!-- <h5 class="card-title">Special title treatment</h5> -->
            <p class="card-text">Phone: <?= $business->getPhone() ?> | BusinessType: <?= BusinessTypesRelations::getBusinessTypeByBusinessName($db, $business->getCompanyName()) ?></p>
            <p class="card-text"><a href="http://<?= $business->getWebPage() ?>" target="_blank" >Webpage</a></p>
            <a href="?page=viewPublicBusiness&id=<?= $business->getIdBusiness()?>" class="btn btn-primary">View</a>
         </div>
      </div>
   </div>
</div>

<hr>