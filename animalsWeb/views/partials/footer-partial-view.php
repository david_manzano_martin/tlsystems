<!-- FOOTER -->
<footer id="footer" class="container-fluid">
   <div class="container">
      <div class="row">
         <div class="col-12 col-md-4">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
         </div>
         <div class="col-12 col-md-4">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
         </div>
         <div class="col-12 col-md-4 text-left">
            <div class="col-12">
               <h6>Contact us</h6>
            </div>
            <div class="col-12">
               <div id="logosRow" class="row">
                  <div class="col-2">
                     <a href="" ><i class="fab fa-instagram"></i></a>
                  </div>
                  <div class="col-10">
                     <a href="" >Instagram</a>
                  </div>
                  <div class="col-2">
                     <a href="" ><i class="fab fa-twitter"></i></a>
                  </div>
                  <div class="col-10">
                     <a href="" >Twitter</a>
                  </div>
                  <div class="col-2">
                     <a href="" ><i class="fab fa-google-plus-square"></i></i></a>
                  </div>
                  <div class="col-10">
                     <a href="" >Google +</a>
                  </div>
            </div>
         </div>
      </div>
   </div>
</footer>
</body>
</html>