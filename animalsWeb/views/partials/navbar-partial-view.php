

<!-- NAVBAR -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="#">Animal page</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav ml-auto">
			<li class="nav-item active">
				<a class="nav-link" href="?">Home 
					<span class="sr-only">(current)</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="?page=breedInformation">Cross breed information</a>
			</li>
			<!--
            <li class="nav-item"><a class="nav-link" href="?page=breederRegistration">Breeder registration</a></li>
            -->
         <li class="nav-item">
				<a class="nav-link" href="?page=businessSearch">Business search</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="?page=publicSearch">Public search</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Rescue dogs</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">About the register</a>
			</li>
			<?php
            if($user->getPermissionLevel()==0){
            
            ?>
			<li class="nav-item">
				<a class="nav-link" href="?page=userRegistration">Registration</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="?page=logIn">Log in</a>
			</li>
			<?php
            } else{

			?>
			<li class="nav-item dropleft">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $user->getUsername() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="?page=profile">View profile</a>
					<a class="dropdown-item" href="?page=waitingList">Waiting list</a>
					<div class="dropdown-divider"></div>
					<?php
						if(User::checkPermissionRequired(3, $user))
						{
					?>
							<a class="dropdown-item" href="?page=viewBusiness">View all business</a>
							<a class="dropdown-item" href="?page=viewBusiness&status=pending">View pending business</a>
							<a class="dropdown-item" href="?page=viewHealthTests">View health tests</a>
							<div class="dropdown-divider"></div>
					<?php
						}
					?>
					<a class="dropdown-item" href="?page=logOut">Log Out</a>
				</div>
			</li>
			<?php
            
            }
            
            ?>
		</ul>
	</div>
</nav>

