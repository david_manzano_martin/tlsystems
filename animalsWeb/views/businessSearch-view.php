<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>
<div class="container">
   <div id="header" class="row">
      <div class="col-12 text-center">
         <h1>Business search</h1>
      </div>
      <div class="col-12">
         <div class="input-group mb-3">
         <form action="<?= $_SERVER["PHP_SELF"] ?>?page=businessSearch" method="POST" class="input-group mb-3">

            <div class="input-group-prepend">
               <label class="input-group-text" for="inputGroupSelect01">Filter (business type)</label>
            </div>
            <select onchange="sendForm()" class="custom-select" id="inputGroupSelect01">
            	<option value="" selected>All</option>
               <?php
               
               	foreach($businessTypesArray as $businessType){

							?>
							
								<option value="<?= $businessType->getBusinessName() ?>"><?= $businessType->getBusinessName() ?></option>
							
							<?php
               	
               	}
               
               ?>
            </select>
            </form>
         </div>
      </div>
   </div>
   <div class="row justify-content-center" id="divContent">
      <?php
         foreach($businessArray as $business){
         
         	require(__DIR__ . "/partials/cardBusiness-partial-view.php");
         
         }
            
            ?>
   </div>
</div>
<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>