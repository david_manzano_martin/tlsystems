<div class="row container-fluid">
   <div class="imgContainer col-12 col-md-4 removePadding">
      <img src="<?= $business->getRoute() ?>" alt="">
   </div>
   <div class="col-12 col-md-8 removePadding">
      <div class="card text-center">
         <div class="card-header">
            <?= $business->getCompanyName() ?>
         </div>
         <div class="card-body">
            <!-- <h5 class="card-title">Special title treatment</h5> -->
            <p class="card-text">Phone: <?= $business->getPhone() ?></p>
            <p class="card-text"><a href="http://<?= $business->getWebPage() ?>" >Webpage</a></p>
            <a href="?page=detailedBusiness&id=<?= $business->getIdBusiness()?>" class="btn btn-primary">View</a>
            <a class="btn btn-light">Disable</a>
            <a class="btn btn-danger">Remove</a>
         </div>
      </div>
   </div>
</div>

<hr>