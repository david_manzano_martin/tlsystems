<div class="card">
  <div class="card-header">
    <?= $healthTest->getHealthTestName() ?>
  </div>
  <div class="card-body">
    <p class="card-text">
    	<?= $healthTest->getHealthTestDescription() ?>
    </p>
    <a href="<?= $_SERVER["PHP_SELF"] ?>?page=viewHealthTest&name=<?= $healthTest->getHealthTestName() ?>" class="btn btn-primary">View public style</a>
    <a href="<?= $_SERVER["PHP_SELF"] ?>?page=modifyHealthTest&name=<?= $healthTest->getHealthTestName() ?>" class="btn btn-secondary">Modify data</a>
  </div>
</div>