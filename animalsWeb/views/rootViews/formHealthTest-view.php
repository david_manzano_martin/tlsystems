

<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div class="container">
   <div class="row">
      <div class="col-12 text-center">
         <h1><?= $title ?></h1>
      </div>
   </div>
   <form class="row">
      <div class="col-12">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="basic-addon1">Health test name</span>
            </div>
            <input type="text" name="healthTestName" value="<?= $temporalHealthTest->getHealthTestName() ?>" class="form-control" placeholder="Enter the name">
         </div>
      </div>
      <div class="col-12">
         <div class="form-group">
            <label for="exampleFormControlTextarea1">Enter the description:</label>
            <textarea class="form-control" value="" name="description" rows="3"><?= $temporalHealthTest->getHealthTestDescription() ?></textarea>
         </div>
      </div>
      <?= $idField ?>
      <div class="col-12 text-center">
      	<a id="btnSend" type="button" class="btn btn-primary"><?= $buttonText ?></a>
      </div>
   </form>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>

