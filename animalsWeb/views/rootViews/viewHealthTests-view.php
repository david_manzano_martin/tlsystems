<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>

<div class="container">
	<div class="row">
		<div class="col-12 text-center">
			<h1>List of Health tests</h1>
			<h6><a href="<?= $_SERVER["PHP_SELF"] ?>?page=addHealthTest" >Add new test</a></h6>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<?php
		
				foreach($healthTestsArray as $healthTest){

					require(__DIR__ . "/rootPartials/healthTestCard-partial.php");
			
				}
		
			?>
		</div>
	</div>	
</div>

<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>