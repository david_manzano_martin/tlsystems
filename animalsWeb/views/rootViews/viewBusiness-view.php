<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div class="container">
   <div class="row text-center">
      <div class="col-12 ">
         <h1><?= $_GET["status"] ?> business</h1>
      </div>
   </div>
   <form action="<?= $_SERVER["PHP_SELF"] ?>?page=viewBusiness" method="POST" class="row">
   	<div class="col-12 col-md-10">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="basic-addon1">Business Name</span>
            </div>
            <input type="text" name="businessName" class="form-control" placeholder="Business Name">
         </div>
      </div>
      <div class="col-12 col-md-2">
      	<button id="searchButton" class="btn btn-primary" >Search</button>
      </div>
   </form>
   <div class="row justify-content-center" id="divContent">
      <?php
         foreach($bussinesArray as $business){
         
         	require(__DIR__ . "/rootPartials/cardBusiness-partial-view.php");
         
         }
            
            ?>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>