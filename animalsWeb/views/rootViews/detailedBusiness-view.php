<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div class="container">
   <div class="row text-center">
      <div class="col-12">
         <h1><?= $business->getCompanyName() ?></h1>
      </div>
   </div>
   <hr>
   <div class="row" id="divBody">
      <div class="businessData container-fluid">
         <div class="col-12 text-center">
            <img src="<?= $business->getRoute() ?>" alt="">
         </div>
         <div class="col-12">
            <form autocomplete="off" class="row" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>?page=detailedBusiness&id=<?= $_GET["id"] ?>" enctype="multipart/form-data">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Name</span>
               </div>
               <input required type="text" name="name" disabled value="<?= $business->getCompanyName() ?>" class="form-control" placeholder="Name / 1st / last">
            </div>
         </div>
         <div class="col-12 col-md-6">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Contact</span>
               </div>
               <input required type="text" value="<?= $business->getContact() ?>" name="contact" class="form-control" placeholder="Contact ">
            </div>
         </div>
         <div class="col-12 col-md-6">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">VAT</label>
               </div>
               <select class="custom-select" name="vat" id="inputGroupSelect01">
                  <option <?= checkSelected("yes", $business->getVAT()) ?> value="yes">Yes</option>
                  <option <?= checkSelected("no", $business->getVAT()) ?> value="no">No</option>
               </select>
            </div>
         </div>
         <div class="col-12 col-md-6">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Company</span>
               </div>
               <input required type="text" value="<?= $business->getCompany() ?>" name="company" class="form-control" placeholder="Company name">
            </div>
         </div>
         <div class="col-12 col-md-6">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">CO number</span>
               </div>
               <input required type="number" step="1" name="coNumber" value="<?= $business->getCoNumber() ?>" class="form-control" placeholder="Co number">
            </div>
         </div>
         <div class="col-12 col-md-6">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupFileAddon01">Logo</span>
               </div>
               <div class="custom-file">
                  <input type="file" name="logo" class="custom-file-input" id="inputGroupFile01">
                  <label required class="custom-file-label" for="inputGroupFile01">Choose file</label>
               </div>
            </div>
         </div>
         <div class="col-12 col-md-6">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Phone</span>
               </div>
               <input required type="number" step="1" name="phone" value="<?= $business->getPhone() ?>" class="form-control" placeholder="Phone number">
            </div>
         </div>
         <div class="col-12 col-md-6">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Email</span>
               </div>
               <input required type="email" name="email" class="form-control" value="<?= $business->getEmail() ?>" placeholder="Email">
            </div>
         </div>
         <div class="col-12 col-md-6">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Web Page</span>
               </div>
               <input required type="text" name="webPage" value="<?= $business->getWebPage() ?>" class="form-control" placeholder="Web page address">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Status</label>
               </div>
               <select class="custom-select" name="status" id="inputGroupSelect01">
						<option <?= checkSelected("active", $business->getStatus()) ?> value="active">Active</option>
               	<option <?= checkSelected("pending", $business->getStatus()) ?> value="pending">Pending</option>
               	<option <?= checkSelected("disabled", $business->getStatus()) ?> value="disabled">Disabled</option>               	
               </select>
            </div>
         </div>
         <div class="col-12 text-center">
            <a id="sendButton" class="btn btn-primary">Update</a>
         </div>
      </form>
         </div>
      </div>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>