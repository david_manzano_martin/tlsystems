<?php require(__DIR__ . "/partials/header-partial-view.php") ?>

<div id="formContainer" class="container-fluid">
	<form id="registationForm" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>"  autocomplete="off">
		<h3>Log in</h3>
		<input type="text" name="username" class="form-control" placeholder="Username">
		<input type="password" name="password" class="form-control" placeholder="Password">
		<a id="btnSend" class="btn btn-primary">Log in</a>
	</form>
</div>

<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>