<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>
<div class="container">
   <form action="?page=addWaitingModel" class="row text-center">
      <div class="col-12">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <label class="input-group-text" for="inputGroupSelect01">Choose breed</label>
            </div>
            <select class="custom-select" name="idBreed" id="inputGroupSelect01">
               <?php
               
               	foreach($arrayBreeds as $breed){

							?>
								<option value="<?= $breed->getIdBreed() ?>"><?= $breed->getBreedName() ?></option>
							<?php
               	
               	}
               
               ?>
               
            </select>
         </div>   
      </div>
      <div class="col-12">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <label class="input-group-text" for="inputGroupSelect01">Choose zone</label>
            </div>
            <select class="custom-select" name="zoneId" id="inputGroupSelect01">
               <?php
               
               	foreach($arrayUkRegions as $ukRegion){

							?>
								<option value="<?= $ukRegion->getIdUkRegion() ?>"><?= $ukRegion->getRegionName() ?></option>
							<?php
               	
               	}
               
               ?>
               
            </select>
         </div>
         
      </div>
      <div class="col-12">
      	<a id="addBtn" class="btn btn-primary">Add</a>
      </div>
   </div>
</div>
<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>