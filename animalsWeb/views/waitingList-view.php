

<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>
<div id="waitingListDiv" class="container">
   <div id="headerDiv" class="row">
      <div class="col-12 text-center">
         <h1>Waiting list</h1>
      </div>
   </div>
   <div id="bodyDiv" class="row text-center">
      <div class="col-12">
      <form action="<?= $_SERVER["PHP_SELF"] ?>?page=waitingList" method="POST">
         <table class="table text-center">
            <thead class="thead-light">
               <tr>
                  <th scope="col">#</th>
                  <th scope="col">Breed</th>
                  <th scope="col">Zone</th>
                  <th scope="col">Action</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  foreach($waitingListArray as $waitingList){
                  	require(__DIR__ . "/partials/waitingTr-partial-view.php");
                  }
                  
                  ?>
                  <tr class="invisible" id="addWaitingList">
                  
                     <td>
                     </td>
                     <td>
                        <div class="row">
                           <div class="col-12">
                              <div class="input-group mb-3">
                                 <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Choose breed</label>
                                 </div>
                                 <select class="custom-select" name="idBreed" id="inputGroupSelect01">
                                    <?php
                                       foreach($arrayBreeds as $breed){
                                       
                                       ?>
                                    <option value="<?= $breed->getIdBreed() ?>"><?= $breed->getBreedName() ?></option>
                                    <?php
                                       }
                                       
                                       ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </td>
                     <td>
                        <div class="row">
                           <div class="col-12">
                              <div class="input-group mb-3">
                                 <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Choose zone</label>
                                 </div>
                                 <select class="custom-select" name="zoneId" id="inputGroupSelect01">
                                    <?php
                                       foreach($arrayUkRegions as $ukRegion){
                                       
                                       ?>
                                    <option value="<?= $ukRegion->getIdUkRegion() ?>"><?= $ukRegion->getRegionName() ?></option>
                                    <?php
                                       }
                                       
                                       ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </td>
                     <td>
                        <a type="button" id="addBtn" class="btn btn-primary">Add</a>
                     </td>
               </tr>
               <tr class="text-center">
                  <td colspan="4"> <i id="btnAddBusiness" class="fas fa-plus-square"></i> </td>
               </tr>                
            </tbody>
         </table>
         </form>
      </div>
   </div>
</div>
<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>

