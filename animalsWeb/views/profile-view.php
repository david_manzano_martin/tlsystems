<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>
<div id="profileDiv" class="container">
   <div class="row">
      <div class="col-12 text-center">
         <h1>Profile</h1>
      </div>
   </div>
   <div id="bodyDiv" class="row">
      <div class="col-12 text-center">
         <img id="imageProfile" src="<?= $user->getImagePath() ?>" alt="">
      </div>
      <input type="file" name="inputLogo" style="display: none" id="inputImageProfile">
      <div class="col-12">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="basic-addon1">Username</span>
            </div>
            <input type="text" value="<?= $user->getUsername() ?>" class="form-control" disabled>
         </div>
      </div>
   </div>
   <div class="row" id="busienssDiv">
      <div class="col-12 text-center">
         <h2>My business</h2>
      </div>
      <div class="col-12 text-center">
         <table class="table">
            <thead class="thead-light">
               <tr>
                  <th scope="col">#</th>
                  <th scope="col">Business Name</th>
                  <th scope="col">Show / Hide</th>
               </tr>
            </thead>
            <tbody>
            
            <?php
            
            	foreach($businessArray as $business){
            		require(__DIR__ . "/partials/businessTr-partial-view.php");
            	}
            
            ?>
            
            <tr>
            	<td colspan="3"> <i id="btnAddBusiness" class="fas fa-plus-square"></i> </td>
            </tr>
            <tr>
            	<td colspan="3"> <iframe id="addIframe" name="iframe" src="?page=businessRegistration&iframe=true"></iframe> </td>
            </tr>
            </tbody>
         </table>
      </div>
   </div>
</div>
<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>