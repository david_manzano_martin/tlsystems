<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="breederRegistrationDiv" class="container">
   <div id="header" class="row text-center">
      <div class="col-12 ">
         <h1>Register as a VET</h1>
      </div>
   </div>
   <div id="body" class="container">
      <form method="POST">
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Contact VET name</span>
               </div>
               <input type="text" displayName="Contact VET name" name="contactVetName" disabled="true" value="<?= $newBusiness->getCompanyName() ?>" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Vet practice name</span>
               </div>
               <input type="text" displayName="Vet practice name" name="vetPracticeName" class="form-control" placeholder="Vet practice name">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Vet Reg No official</span>
               </div>
               <input type="number" step="1" displayName="Vet Reg No official" name="vetRegOfficial" class="form-control" placeholder="Vet Reg No official">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Date of practice registration</span>
               </div>
               <input type="date" displayName="Date of practice registration" name="datePracticeRegistration" class="form-control" placeholder="Date of practice registration">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Add 1</span>
               </div>
               <input type="text" displayName="Add 1" name="add1" class="form-control" placeholder="Add 1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Add 2</span>
               </div>
               <input type="text" displayName="Add 2" name="add2" class="form-control" placeholder="Add 2">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Town</span>
               </div>
               <input type="text" displayName="Town" name="town" class="form-control" placeholder="Town">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">County 1</span>
               </div>
               <input type="text" displayName="County 1" name="county" class="form-control" placeholder="County">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Postcode</span>
               </div>
               <input type="number" step="1" displayName="Postcode" name="postcode" class="form-control" placeholder="Postcode">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Practice email</span>
               </div>
               <input type="email" displayName="Practice email" name="practiceEmail" class="form-control" placeholder="Practice email">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Practice Landline No</span>
               </div>
               <input type="email" displayName="Practice LandLine No" name="practiceLandline" class="form-control" placeholder="Practice Landline No">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Practice Mobile No</span>
               </div>
               <input type="number" displayName="Practice Mobile No" name="practiceMobile" class="form-control" placeholder="Practice Mobile No">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Practice WebPage</span>
               </div>
               <input type="text" displayName="Practice WebPage" name="practiceWebPage" class="form-control" placeholder="Practice WebPage">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Practice Facebook Page</span>
               </div>
               <input type="text" displayName="Practice Facebook Page" name="practiceFacebook" class="form-control" placeholder="Practice Facebook">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Practice Twitter Page</span>
               </div>
               <input type="text" displayName="Practice Twitter page" name="practiceTwitter" class="form-control" placeholder="Practice Twitter">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Area covered</span>
               </div>
               <input type="text" displayName="Area covered" name="areaCovered" class="form-control" placeholder="Area covered">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">About this practice</span>
               </div>
               <input type="text" displayName="About this practice" name="aboutPractice" class="form-control" placeholder="About this practice">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">About this Vet</span>
               </div>
               <input type="text" displayName="About this Vet" name="aboutVet" class="form-control" placeholder="About this Vet">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Display info</label>
               </div>
               <select class="custom-select" displayName="Display info" name="displayInfo" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>
         <input type="hidden" name="businessType" value="<?= $_POST["businessType"] ?>" >
         <div class="col-12  text-center">
            <a id="sendButton" class="btn btn-primary">Save</a>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>