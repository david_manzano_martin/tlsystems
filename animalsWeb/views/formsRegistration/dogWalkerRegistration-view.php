<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="breederRegistrationDiv" class="container">
   <div id="header" class="row text-center">
      <div class="col-12">
         <h1>Register as a dog walker</h1>
      </div>
   </div>
   <div id="body" class="container">
      <form method="POST">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business name</span>
               </div>
               <input type="text"displayName="Business name" name="name" disabled value="<?= $newBusiness->getCompanyName() ?>" class="form-control" placeholder="Username">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <!--<div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">CBR Register Number</span>
               </div>-->
               <input type="hidden" name="cbrRegNo" displayName="CBR Register Number" class="form-control" placeholder="CBR Register Number">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Valid dbs check</label>
               </div>
               <select class="custom-select" name="validDbs" displayName="Valid dbs check" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Valid dbs: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="validDbs" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="validDbs" value="no">No
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Business insurance</label>
               </div>
               <select class="custom-select" name="businessInsurance" displayName="Business insurance" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Business insurance: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="businessInsurance" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="businessInsurance" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Insurance provider</label>
               </div>
              	<input type="upload" name="insuranceProvider" class="form-control" displayName="Insurance provider" placeholder="Insurance provider">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Canine first aid qualification</label>
               </div>
              	<input type="text" name="canineAidQualification" displayName="Canine first aid qualification" placeholder="Canine first aid qualification">
            </div>
         </div>-->
         <div class="col-12">
            <label>Canine first aid qualification: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="canineAidQualification" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="canineAidQualification" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Max number of dogs walked together</label>
               </div>
              	<input type="number" step="1" max="99" name="maxDogsTogheter" displayName="Max number of dogs walked together" placeholder="Max dogs walked together">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Areas covered</span>
               </div>
               <textarea displayName="Areas covered" name="areasCovered" class="form-control" placeholder="Write the areas that you cover"></textarea>
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Air condition</label>
               </div>
               <select class="custom-select" name="airCondition" displayName="Air condition" id="inputGroupSelect01">
                  <option value="withoutVan">Without VAN</option>
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Air condition: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="airCondition" value="car with air">Car with air condition
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="airCondition" value="van with air">Van with air condition
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="airCondition" value="car without air">Car without air condition
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="airCondition" value="van without air">Van without air condition
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Pick up Drop Service</label>
               </div>
               <select class="custom-select" name="pickUpDropService" displayName="Pick up Drop Service" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Product range and price</span>
               </div>
               <textarea displayName="Product range and price" name="productRangeAndPrice" class="form-control" placeholder="Write the product range and price"></textarea>
            </div>
         </div>
         <input type="hidden" name="businessType" value="<?= $_POST["businessType"] ?>" >
         <div class="col-12 text-center">
         	<a id="sendButton" class="btn btn-primary">Save</a>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>