

<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="breederRegistrationDiv" class="container">
   <div id="header" class="row text-center">
      <div class="col-12">
         <h1>Register as a food provider</h1>
      </div>
   </div>
   <div id="body" class="container">
      <form method="POST">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business name</span>
               </div>
               <input type="text"displayName="Business name" name="name" disabled value="<?= $newBusiness->getCompanyName() ?>" class="form-control" placeholder="Username">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">CBR Register Number</span>
               </div>
               <input type="text" name="cbrRegNo" displayName="CBR Register Number" class="form-control" placeholder="CBR Register Number">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Type food</span>
               </div>
               <textarea displayName="Area covered" name="typeFood" class="form-control" placeholder="Write the area that you cover"></textarea>
            </div>
         </div>-->
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Type food</label>
               </div>
               <select class="custom-select" name="typeFood" id="inputGroupSelect01">
                  <option value="yes">Ex1</option>
                  <option value="no">Ex2</option>
                  <option value="yes">Ex1</option>
                  <option value="no">Ex2</option>
                  <option value="yes">Ex1</option>
                  <option value="no">Ex2</option>
               </select>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Area covered</span>
               </div>
               <textarea displayName="Area covered" name="areaCovered" class="form-control" placeholder="Write the area that you cover"></textarea>
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Home delivery</label>
               </div>
               <select class="custom-select" name="homeDelivery" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Home delivery: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="homeDelivery" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="homeDelivery" value="no">No
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Shipping</label>
               </div>
               <select class="custom-select" name="shipping" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Shipping: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="shipping" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="shipping" value="no">No
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Personal consultation</label>
               </div>
               <select class="custom-select" name="personalConsultation" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Personal consultation: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="personalConsultation" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="personalConsultation" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Product Range and Price</span>
               </div>
               <textarea displayName="Product Range and Price" class="form-control" name="productRangePrice" placeholder="Write the price and range of your products"></textarea>
            </div>
         </div>
         <input type="hidden" name="businessType" value="<?= $_POST["businessType"] ?>" >
         <div class="col-12 text-center">
            <a id="sendButton" class="btn btn-primary">Save</a>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>

