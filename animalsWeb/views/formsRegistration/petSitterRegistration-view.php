<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="breederRegistrationDiv" class="container">
   <div id="header" class="row text-center">
      <div class="col-12">
         <h1>Register as a pet sitter</h1>
      </div>
   </div>
   <div id="body" class="container">
      <form method="POST">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business name</span>
               </div>
               <input type="text"displayName="Business name" name="name" disabled value="<?= $newBusiness->getCompanyName() ?>" class="form-control" placeholder="Username">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <!--<div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">CBR Register Number</span>
               </div>-->
               <input type="hidden" name="cbrRegNo" displayName="CBR Register Number" class="form-control" placeholder="CBR Register Number">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Business insurance</label>
               </div>
               <select class="custom-select" name="businessInsurance" displayName="Business insurance" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Business insurance: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="businessInsurance" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="businessInsurance" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Insurance provider</label>
               </div>
              	<input type="upload" name="insuranceProvider" class="form-control" displayName="Insurance provider" placeholder="Insurance provider">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Experience years</label>
               </div>
              	<input type="number" step="1" max="99" name="experienceYears" displayName="Experience years" placeholder="How many years">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Prefered dogs</label>
               </div>
              	<input type="text" name="preferedDogs" displayName="Prefered dogs" placeholder="Insurance provider">
            </div>
         </div>-->
         <div class="col-12">
            <label>Business insurance: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="preferedDogs" value="small">Small
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="preferedDogs" value="medium">Medium
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="preferedDogs" value="large">Large
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Areas covered</span>
               </div>
               <textarea displayName="Areas covered" name="areasCovered" class="form-control" placeholder="Write the areas that you cover"></textarea>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Product Range and Price</span>
               </div>
               <textarea displayName="Product Range and Price" class="form-control" name="productRangePrice" placeholder="Write the price and range of your products"></textarea>
            </div>
         </div>
         <input type="hidden" name="businessType" value="<?= $_POST["businessType"] ?>" >
         <div class="col-12 text-center">
         	<a id="sendButton" class="btn btn-primary">Save</a>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>