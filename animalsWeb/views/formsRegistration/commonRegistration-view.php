<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="businessRegistrationDiv" class="container">
   <div id="businessHeader" class="row">
      <div class="col-12 text-center">
         <h1>Business registration</h1>
      </div>
   </div>
   <hr>
   <div id="businessBody" class="justify-content-start container">
      <form autocomplete="off" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>?page=businessRegistration&secondPart=true" enctype="multipart/form-data">
         <div class="col-12">
            <div class="input-group mb-3">
               <!--<div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Name</span>
                  </div>-->
               <input required type="hidden" name="company" value="0" class="form-control" placeholder="Name / 1st / last">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business name</span>
               </div>
               <input required type="text" name="name" class="form-control" placeholder="Business name">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Contact</span>
               </div>
               <input required type="text" name="contact" class="form-control" placeholder="Contact ">
            </div>
         </div>
         <!-- <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">VAT</label>
               </div>
               <select class="custom-select" name="vat" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
            </div>-->
         <div class="col-12" style="margin:15px; margin-left: 0px;">
            <label>VAT: </label>
            <input type="checkbox" checked class="toggleElement" name="vat" dataToggle="null" value="yes"> Yes
            <input type="checkbox" class="toggleElement" name="vat" dataToggle="null" value="no"> No
         </div>
         <div class="col-12" style="margin:15px; margin-left: 0px;">
            <label>Has Company Registration Number: </label>
            <input type="checkbox" class="toggleElement" name="coNumber" dataToggle="test1" value="yes"> Yes
            <input type="checkbox" class="toggleElement" name="coNumber" dataToggle="test1" value="no"> No
            <input type="text" disabled="" name="coNumber" value="0" class="test1">
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">CO number</span>
               </div>
               <input required type="number" step="1" name="coNumber" class="form-control" placeholder="Co number">
            </div>
            </div>-->
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="custom-file">
                  <input style="width: 100%" type="file" name="logo" class="custom-file-input" id="logoField">
                  <label required class="custom-file-label" id="" for="inputGroupFile01">Logo</label>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Landline Phone & mobile number</span>
               </div>
               <input required type="number" step="1" name="phone" class="form-control" placeholder="Landline Phone & mobile number">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Email</span>
               </div>
               <input required type="email" name="email" class="form-control" placeholder="Email">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Web Page</span>
               </div>
               <input required type="text" name="webPage" class="form-control" placeholder="Web page address">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Business type</label>
               </div>
               <select class="custom-select" name="businessType" id="inputGroupSelect01">
               <?php
                  for($k=0;$k<=count($businessTypesArray)-1;$k++) {
                  	require(__DIR__ . "/formsPartials/businessTypeOptions-partial-view.php");
                  }
                  
                  ?>
               </select>
            </div>
            <div class="col-12">
               <small>You will be able to add more types later</small>
            </div>
         </div>
         <div class="col-12 text-center">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Facebook page</span>
               </div>
               <input type="text" class="form-control" name="facebookPage" placeholder="Facebook page">
            </div>
         </div>
         <div class="col-12 text-center">
            <a id="sendButton" class="btn btn-primary">Send</a>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>