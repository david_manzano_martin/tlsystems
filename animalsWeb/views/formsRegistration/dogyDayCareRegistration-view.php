<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="breederRegistrationDiv" class="container">
   <div id="header" class="row text-center">
      <div class="col-12">
         <h1>Register as a doggy day care</h1>
      </div>
   </div>
   <div id="body" class="container">
      <form method="POST">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business name</span>
               </div>
               <input type="text"displayName="Business name" name="name" disabled value="<?= $newBusiness->getCompanyName() ?>" class="form-control" placeholder="Username">
            </div>
         </div>
         <!-- <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">CBR Register Number</span>
               </div>
               <input type="number" step="1" name="cbrRegNo" displayName="CBR Register Number" class="form-control" placeholder="CBR Register Number">
            </div>
            </div> -->
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Local authority license</span>
               </div>
               <input type="text" step="1" name="localAutorithyLicense" displayName="Local authority license" class="form-control" placeholder="Local authority license">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business insurance</span>
               </div>
               <input type="text" name="businessINsurance" displayName="Business insurance" class="form-control">
            </div>
            </div>-->
         <div class="col-12">
            <label>Business insurance: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="businessINsurance" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="businessINsurance" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Insurance provider</span>
               </div>
               <input type="text" name="insuranceProvider" displayName="Insurance provider" class="form-control">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Canine first aid qualification</span>
               </div>
               <input type="number" step="1" name="canineFirstAidQualification" displayName="Canine first aid qualification" class="form-control">
            </div>
            </div>-->
         <div class="col-12">
            <label>Canine first aid qualification: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="canineFirstAidQualification" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="canineFirstAidQualification" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Number of staff</span>
               </div>
               <input type="number" step="1" name="noStaff" displayName="Number of staff" class="form-control">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">No of dogs licensed to board</span>
               </div>
               <input type="number" step="1" name="noDogLicenseBoard" displayName="Canine first aid qualification" class="form-control">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Prefered dogs</label>
               </div>
               <select class="custom-select" name="preferedDogs" displayName="Breeds 6" id="inputGroupSelect01">
                  <option value="small">Small</option>
                  <option value="medium">Medium</option>
                  <option value="large">Large</option>
                  <option value="any">Any</option>
               </select>
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Accept neutered males</label>
               </div>
               <select class="custom-select" name="acceptNeuteredMales" displayName="Accept neutered males" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
            </div>-->
         <div class="col-12">
            <label>Accept neutered males: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="acceptNeuteredMales" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="acceptNeuteredMales" value="no">No
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Accept neutered females</label>
               </div>
               <select class="custom-select" name="acceptNeuteredfemales" displayName="Accept neutered females" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
            </div>-->
         <div class="col-12">
            <label>Accept neutered females: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="acceptNeuteredfemales" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="acceptNeuteredfemales" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">No of years licence held for</span>
               </div>
               <input type="number" step="1" name="yearsExperience" displayName="Years experience" class="form-control">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Method of exercise</span>
               </div>
               <input type="text" name="methodOfExercise" displayName="Method of exercise" class="form-control">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Other facilities</span>
               </div>
               <input type="text" name="otherFacilities" displayName="Other facilities" class="form-control">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Area covered</span>
               </div>
               <input type="text" name="areaCovered" displayName="Area covered" class="form-control">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Pick up drop service</label>
               </div>
               <select class="custom-select" name="pickUpDropService" displayName="Pick up drop service" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
            </div>-->
         <div class="col-12">
            <label>Pick up/Drop off service: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="pickUpDropService" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="pickUpDropService" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Opening hours</span>
               </div>
               <input type="text" name="openingHours" displayName="Opening hours" class="form-control">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Product range price</label>
               </div>
               <select class="custom-select" name="productRangePrice" id="inputGroupSelect01">
                  <option value="Doggy Day Care 1 dog">Doggy Day Care 1 dog</option>
                  <option value="Doggy Day Care 2 dogs">Doggy Day Care 2 dogs</option>
                  <option value="Doggy Day Care 3 dogs">Doggy Day Care 3 dogs</option>
               </select>
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Product range price</span>
               </div>
               <input type="text" name="productRangePrice" displayName="productRangePrice" class="form-control">
            </div>
            </div>-->
         <input type="hidden" name="businessType" value="<?= $_POST["businessType"] ?>" >
         <div class="col-12 text-center">
            <a id="sendButton" class="btn btn-primary">Save</a>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>