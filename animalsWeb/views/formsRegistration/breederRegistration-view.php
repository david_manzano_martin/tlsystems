<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="breederRegistrationDiv" class="container">
   <div id="header" class="row text-center">
      <div class="col-12">
         <h1>Register as a breeder</h1>
      </div>
   </div>
   <div id="body" class="container">
      <form method="POST">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business name</span>
               </div>
               <input type="text"displayName="Business name" name="name" disabled value="<?= $newBusiness->getCompanyName() ?>" class="form-control" placeholder="Username">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <!--<div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">ECBR Register Number</span>
               </div>-->
               <input type="hidden" name="cbrRegNo" displayName="CBR Register Number" class="form-control" placeholder="CBR Register Number">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">KC Breeding name</span>
               </div>
               <input type="text" name="kcBreedingName" displayName="KC Breeding name" class="form-control" placeholder="Kc Breeding name">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Vet name</span>
               </div>
               <input type="text" name="vetName" displayName="Vet name" class="form-control" placeholder="Vet name">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Vet reference</span>
               </div>
               <input type="text" name="vetReference" displayName="Vet reference" class="form-control" placeholder="Vet reference">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Number of registered dogs</span>
               </div>
               <input type="number" step="1" name="numberDogs" displayName="Number f=if registered dogs" class="form-control" placeholder="Number of dogs">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Number of registered dams</span>
               </div>
               <input type="number" step="1" name="numberDams" displayName="Number of registered dams" class="form-control" placeholder="Number of dams">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">About you</span>
               </div>
               <textarea displayName="About you" name="aboutYou" name="aboutYou" class="form-control" placeholder="Write about you"></textarea>
            </div>
         </div>
         <br>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
               </div>
               <div class="custom-file">
                  <input type="file" displayName="Upload" name="logo" class="custom-file-input" multiple id="inputGroupFile01">
                  <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
               </div>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">CBR Accredited Breeder</label>
               </div>
               <select class="custom-select" name="cbrAccredited" displayName="CBR Accredited Breeder" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Breeds 1</label>
               </div>
               <select class="custom-select" name="breed1" displayName="Breeds 1" id="inputGroupSelect01">
               	<?php
               	
               		foreach($breedsArray as $element){

							?>
							
								<option value="<?= $element->getBreedName() ?>"><?= $element->getBreedName() ?></option>
							
							<?php
               		
               		}
               	
               	?>
               </select>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Breeds 2</label>
               </div>
               <select class="custom-select" name="breed2" displayName="Breeds 2" id="inputGroupSelect01">
                  <?php
               	
               		foreach($breedsArray as $element){

							?>
							
								<option value="<?= $element->getBreedName() ?>"><?= $element->getBreedName() ?></option>
							
							<?php
               		
               		}
               	
               	?>
               </select>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Breeds 3</label>
               </div>
               <select class="custom-select" name="breed3" displayName="Breeds 3" id="inputGroupSelect01">
                  <?php
               	
               		foreach($breedsArray as $element){

							?>
							
								<option value="<?= $element->getBreedName() ?>"><?= $element->getBreedName() ?></option>
							
							<?php
               		
               		}
               	
               	?>
               </select>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Breeds 4</label>
               </div>
               <select class="custom-select" name="breed4" displayName="Breeds 4" id="inputGroupSelect01">
                  <?php
               	
               		foreach($breedsArray as $element){

							?>
							
								<option value="<?= $element->getBreedName() ?>"><?= $element->getBreedName() ?></option>
							
							<?php
               		
               		}
               	
               	?>
               </select>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Breeds 5</label>
               </div>
               <select class="custom-select" name="breed5" displayName="Breeds 5" id="inputGroupSelect01">
                  <?php
               	
               		foreach($breedsArray as $element){

							?>
							
								<option value="<?= $element->getBreedName() ?>"><?= $element->getBreedName() ?></option>
							
							<?php
               		
               		}
               	
               	?>
               </select>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Breeds 6</label>
               </div>
               <select class="custom-select" name="breed6" displayName="Breeds 6" id="inputGroupSelect01">
                  <?php
               	
               		foreach($breedsArray as $element){

							?>
							
								<option value="<?= $element->getBreedName() ?>"><?= $element->getBreedName() ?></option>
							
							<?php
               		
               		}
               	
               	?>
               </select>
            </div>
         </div>
         <input type="hidden" name="businessType" value="<?= $_POST["businessType"] ?>" >
         <div class="col-12 text-center">
         	<a id="sendButton" class="btn btn-primary">Save</a>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>