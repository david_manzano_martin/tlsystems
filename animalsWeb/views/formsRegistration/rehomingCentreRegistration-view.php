<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>

<div id="rehomingCentreView" class="container">
   <div id="header" class="row text-center">
      <div class="col-12 ">
         <h1>Register as a Rehoming Centre</h1>
      </div>
   </div>
   <div id="body" class="container">
      <form method="POST">
      <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business name</span>
               </div>
               <input type="text"displayName="Business name" name="name" disabled value="<?= $newBusiness->getCompanyName() ?>" class="form-control" placeholder="Username">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Contact Name 1</span>
               </div>
               <input type="text" displayName="Contact Name 1" name="Contact Name 1"  class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Contact Name 2</span>
               </div>
               <input type="text" displayName="Contact Name 2" name="Contact Name 2"  class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Rehoming Centre Name</span>
               </div>
               <input type="text" displayName="Rehoming Centre Name" name="rehomingCentreName" class="form-control" placeholder="Rehoming Centre Name">
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupFileAddon01">Logo</span>
               </div>
               <div class="custom-file">
                  <input type="file" displayName="Upload" name="logo" class="custom-file-input" multiple id="inputGroupFile01">
                  <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
               </div>
            </div>
         </div>
         
        <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Charity Number</span>
               </div>
               <input type="number" displayName="Charity Number" name="charityNumber" class="form-control" placeholder="Charity Number">
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Landline Number</span>
               </div>
               <input type="text" displayName="Landline Number" name="landlineNumber" class="form-control" placeholder="Landline Number">
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Mobile No Contact 1</span>
               </div>
               <input type="text" displayName="Mobile No Contact 1" name="mobileNoContact1" class="form-control" placeholder="Mobile No Contact 1">
            </div>
         </div>
         
          <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Mobile No Contact 2</span>
               </div>
               <input type="text" displayName="Mobile No Contact 2" name="mobileNoContact2" class="form-control" placeholder="Mobile No Contact 2">
            </div>
         </div>
         
          <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">email address Contact 1</span>
               </div>
               <input type="text" class="form-control" placeholder="email address Contact 1" aria-label="emailAddressContact1" aria-describedby="basic-addon1">
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">email address Contact 2</span>
               </div>
               <input type="text" displayName="email address Contact 2" name="emailAddressContact2" class="form-control" placeholder="email address Contact 2">
            </div>
         </div>
         
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Website</span>
               </div>
               <input type="email" displayName="Website" name="website" class="form-control" placeholder="Website">
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Facebook Page</span>
               </div>
               <input type="email" displayName="Facebook Page" name="facebookPage" class="form-control" placeholder="Facebook Page">
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Instagram Page</span>
               </div>
               <input type="email" displayName="Instagram Page" name="instagramPage" class="form-control" placeholder="Instagram Page">
            </div>
         </div>
         
          <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Twitter Page</span>
               </div>
               <input type="email" displayName="Twitter Page" name="twitterPage" class="form-control" placeholder="Twitter Page">
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Dogs</label>
               </div>
               <select class="custom-select" displayName="Dogs" name="dogs" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Cats</label>
               </div>
               <select class="custom-select" displayName="Cats" name="cats" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Rabbits</label>
               </div>
               <select class="custom-select" displayName="Rabbits" name="rabbits" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Ferrets</label>
               </div>
               <select class="custom-select" displayName="Ferrets" name="ferrets" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>
         
        <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Small caged animals</label>
               </div>
               <select class="custom-select" displayName="Small caged animals" name="smallCagedAnimals" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>
         
          <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">About You</span>
               </div>
               <input type="text" displayName="About You" name="aboutYou" class="form-control" placeholder="About You">
            </div>
         </div>
         
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupFileAddon01">Picture 1-10 Upload</span>
               </div>
               <div class="custom-file">
                  <input type="file" displayName="Upload" name="logo" class="custom-file-input" multiple id="inputGroupFile01">
                  <label class="custom-file-label" for="inputGroupFile01">Upload area (x10)</label>
               </div>
            </div>
         </div>
         
         
         
         <div class="col-12  text-center">
            <a id="sendButton" class="btn btn-primary">Save</a>
         </div>
      </form>
   </div>
</div>


<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>