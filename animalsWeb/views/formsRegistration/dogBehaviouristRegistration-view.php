

<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="breederRegistrationDiv" class="container">
   <div id="header" class="row text-center">
      <div class="col-12">
         <h1>Register as a dog behavourist</h1>
      </div>
   </div>
   <div id="body" class="container">
      <form method="POST">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business name</span>
               </div>
               <input type=""displayName="Business name" name="name" disabled value="<?= $newBusiness->getCompanyName() ?>" class="form-control" placeholder="Username">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <!--<div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">CBR Register Number</span>
               </div>-->
               <input type="hidden" name="cbrRegNo" displayName="CBR Register Number" class="form-control" placeholder="CBR Register Number">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Business insurance</label>
               </div>
               <select class="custom-select" name="businessInsurance" displayName="Business insurance" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Business insurance: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="businessInsurance" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="businessInsurance" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Insurance provider</label>
               </div>
               <input type="text" name="insuranceProvider" class="form-control" displayName="Insurance provider" placeholder="Insurance provider">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Canine first aid qualification</span>
               </div>
               <input type="number" step="1" name="canineFirstAidQualification" displayName="Canine first aid qualification" class="form-control" placeholder="">
            </div>
         </div>-->
         <div class="col-12">
            <label>Business insurance: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="canineFirstAidQualification" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="canineFirstAidQualification" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Degree</label>
               </div>
               <input type="text" name="degree" class="form-control">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Qualification</span>
               </div>
               <input type="text" step="1" name="qualification" displayName="Qualification" class="form-control" placeholder="">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Examing body</label>
               </div>
               <input type="text" step="1" name="examingBody" displayName="Qualification" class="form-control" placeholder="">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Years of experience</span>
               </div>
               <input type="number" step="1" name="experienceYears" displayName="years of experience" class="form-control" placeholder="">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Area covered</span>
               </div>
               <textarea displayName="Area covered" name="areaCovered" class="form-control" placeholder="Write the area that you cover"></textarea>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Product Range and Price</span>
               </div>
               <textarea displayName="Product Range and Price" name="productRangePrice" placeholder="Write the price and range of your products"></textarea>
            </div>
         </div>
         <input type="hidden" name="businessType" value="<?= $_POST["businessType"] ?>" >
         <div class="col-12 text-center">
            <a id="sendButton" class="btn btn-primary">Save</a>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>

