

<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="breederRegistrationDiv" class="container">
   <div id="header" class="row text-center">
      <div class="col-12">
         <h1>Register as a kennel</h1>
      </div>
   </div>
   <div id="body" class="container">
      <form method="POST">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business name</span>
               </div>
               <input type="text"displayName="Business name" name="name" disabled value="<?= $newBusiness->getCompanyName() ?>" class="form-control" placeholder="Username">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <!--<div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">CBR Register Number</span>
               </div>-->
               <input type="hidden" name="cbrRegNo" displayName="CBR Register Number" class="form-control" placeholder="CBR Register Number">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Business insurance</label>
               </div>
               <select class="custom-select" name="businessInsurance" displayName="Business insurance" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Business insurance: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="businessInsurance" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="businessInsurance" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Insurance provider</label>
               </div>
               <input type="file" name="insuranceProvider" displayName="Insurance provider" placeholder="Insurance provider">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Canine first aid qualifications</label>
               </div>
               <input type="number" step="1" name="canineQualifications" displayName="Canine first aid qualifications" class="form-control" placeholder="Insurance provider">
            </div>
         </div>-->
         <div class="col-12">
            <label>Canine first aid qualification: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="canineQualifications" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="canineQualifications" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">No. of staff</label>
               </div>
               <input type="number" step="1" name="noStuff" displayName="Stuff number" class="form-control" placeholder="Insurance provider">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Kennels number</label>
               </div>
               <input type="number" step="1" name="noKennels" displayName="Kennels number" class="form-control" placeholder="Insurance provider">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Opening hours</label>
               </div>
               <input type="text" name="openingHours" displayName="Opening Hours" class="form-control" placeholder="Insurance provider">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Exercise methods</label>
               </div>
               <input type="text" name="exerciseMethods" displayName="Exercise methods" class="form-control" placeholder="Insurance provider">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Years of experience</label>
               </div>
               <input type="number" step="1" max="99" name="noYears" displayName="Years of experience" class="form-control" placeholder="Years of experience">
            </div>
         </div>
         <div class="col-12">
            <div class="form-group">
               <label for="exampleFormControlTextarea1">Area covered</label>
               <textarea class="form-control" displayName="areaCovered" name="Area covered" placeholder="rite the area where you work"></textarea>
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Pick up Drop service</label>
               </div>
               <select class="custom-select" name="pickUpDropService" displayName="Pick up Drop service" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Pick up Drop service: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="pickUpDropService" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="pickUpDropService" value="no">No
         </div>
         <div class="col-12">
            <div class="form-group">
               <label for="exampleFormControlTextarea1">Product range and price</label>
               <textarea class="form-control" displayName="Product range and price" name="productRangePrice" placeholder="rite the area where you work"></textarea>
            </div>
         </div>
         <input type="hidden" name="businessType" value="<?= $_POST["businessType"] ?>" >
         <div class="col-12 text-center">
            <a id="sendButton" class="btn btn-primary">Save</a>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>

