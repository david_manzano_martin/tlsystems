<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/partials/navbar-partial-view.php") ?>
<div id="businessRegistrationDiv" class="container">
   <div id="businessHeader" class="row">
      <div class="col-12 text-center">
         <h1>Business registration</h1>
      </div>
   </div>
   <hr>
   <div id="businessBody" class="justify-content-start container">
      <form autocomplete="off" method="POST">
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Company Name</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Category</label>
               </div>
               <select class="custom-select" id="inputGroupSelect01">
                  <option selected>Choose...</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
               </select>
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Category 2</label>
               </div>
               <select class="custom-select" id="inputGroupSelect01">
                  <option selected>Choose...</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
               </select>
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Category 3</label>
               </div>
               <select class="custom-select" id="inputGroupSelect01">
                  <option selected>Choose...</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
               </select>
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Contact Name</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Address Line 1</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Address Line 2</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Address Line 3</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Town</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">County</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Country</label>
               </div>
               <select class="custom-select" id="inputGroupSelect01">
                  <option selected>Choose...</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
               </select>
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">PostCode</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Email</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Confirm Email</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <div class="input-group-text">
                     <input type="checkbox" aria-label="Checkbox for following text input">
                  </div>
               </div>
               <input type="text" class="form-control" aria-label="Text input with checkbox" placeholder="Contact by Email">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Phone</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Mobile</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Website</span>
               </div>
               <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 ">
            <div class="form-group">
               <label for="exampleFormControlTextarea1">Company Summary</label>
               <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <div class="input-group-text">
                     <input type="checkbox" aria-label="Checkbox for following text input">
                  </div>
               </div>
               <input type="text" class="form-control" aria-label="Text input with checkbox" placeholder="Publish">
            </div>
         </div>
         <div id="divTherms" class="col-12">
         	<p>
         		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
         	</p>
         </div>
         <div class="col-12 ">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <div class="input-group-text">
                     <input type="checkbox" aria-label="Checkbox for following text input">
                  </div>
               </div>
               <input type="text" class="form-control" aria-label="Text input with checkbox" placeholder="Accept therms and conditions">
            </div>
         </div>
         <div class="col-12 text-center">
         	<button class="btn btn-primary">Send</button>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/partials/footer-partial-view.php") ?>