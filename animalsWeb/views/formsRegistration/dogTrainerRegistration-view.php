<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="breederRegistrationDiv" class="container">
   <div id="header" class="row text-center">
      <div class="col-12">
         <h1>Register as a Dog Trainer</h1>
      </div>
   </div>
   <div id="body" class="container">
      <form method="POST">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Business name</span>
               </div>
               <input type="text"displayName="Business name" name="name" disabled value="<?= $newBusiness->getCompanyName() ?>" class="form-control" placeholder="Username">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <!--<div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">CBR Register Number</span>
               </div>-->
               <input type="hidden" name="cbrRegNo" displayName="CBR Register Number" class="form-control" placeholder="CBR Register Number">
            </div>
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Valid dbs Check</label>
               </div>
               <select class="custom-select" name="validDbs" displayName="Valid dbs Check" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Valid dbs Check: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="validDbs" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="validDbs" value="no">No
         </div>
         <!--<div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Business Insurance</label>
               </div>
               <select class="custom-select" name="businessInsurance" displayName="Business Insurance" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>-->
         <div class="col-12">
            <label>Business Insurance: </label>
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" checked name="businessInsurance" value="yes">Yes
            <input type="checkbox" onclick="chooseCheckbox" class="checkBox" name="businessInsurance" value="no">No
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Insurance provider</span>
               </div>
               <input type="file" name="insuranceProvider" displayName="Insurance provider" class="form-control" placeholder="Vet reference">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Canine First Aid Qualification</label>
               </div>
               <select class="custom-select" name="canineFirstAidQualification" displayName="Canine First Aid Qualification" id="inputGroupSelect01">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
               </select>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Qualifications</span>
               </div>
               <input type="number" class="form-control" step="1" name="qualifications" displayName="Qualifications">
            </div>
         </div>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">kg gods approved</span>
               </div>
               <textarea displayName="kgGcds Approved" name="kgGcdsApproved" class="form-control" placeholder="Write about you"></textarea>
            </div>
         </div>
         <br>
         <!--<div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Area covered</span>
               </div>
               <textarea displayName="Area covered" name="areaCovered" class="form-control" placeholder="Area covered"></textarea>
            </div>
         </div>-->
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Area covered</label>
               </div>
               <select class="custom-select" name="areaCovered" displayName="Canine First Aid Qualification" id="inputGroupSelect01">
                  <?php
			
							foreach($ukRegionsArray as $ukRegion){

								echo("<option value='" . $ukRegion->getRegionName() . "'>" . $ukRegion->getRegionName() . "</option>");
				
							}
			
						?>
               </select>
            </div>
         </div>
			

         <br>
         <div class="col-12">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">Product range and price</span>
               </div>
               <textarea displayName="Product range and price" name="productRangePrice" class="form-control" placeholder="Write your product range and price"></textarea>
            </div>
         </div>
         <input type="hidden" name="businessType" value="<?= $_POST["businessType"] ?>" >
         <div class="col-12 text-center">
         	<a id="sendButton" class="btn btn-primary">Save</a>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>