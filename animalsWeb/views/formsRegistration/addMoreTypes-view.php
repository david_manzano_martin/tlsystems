<?php require(__DIR__ . "/../partials/header-partial-view.php") ?>
<?php require(__DIR__ . "/../partials/navbar-partial-view.php") ?>
<div id="businessRegistrationDiv" class="container">
   <div id="businessHeader" class="row">
      <div class="col-12 text-center">
         <h1>Business registration</h1>
      </div>
   </div>
   <hr>
   <div id="businessBody" class="justify-content-start container">
      <form autocomplete="off" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>?page=businessRegistration&secondPart=true" enctype="multipart/form-data">
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Business type</label>
               </div>
               <select class="custom-select" name="businessType" id="inputGroupSelect01">
                  <?php
                  
                  	for($k=0;$k<=count($businessTypesArray)-1;$k++) {
                  		require(__DIR__ . "/formsPartials/businessTypeOptions-partial-view.php");
                  	}
                  
                  ?>
               </select>
            </div>
            <div class="col-12">
            	<small>You will be able to add more types later</small>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <input type="hidden" displayName="Business name" name="name" value="<?= $_GET["name"] ?>" class="form-control" placeholder="Username">
            </div>
         </div>
         <div class="col-12 text-center">
            <button id="sendButton" class="btn btn-primary">Continue</button>
         </div>
      </form>
   </div>
</div>
<?php require(__DIR__ . "/../partials/footer-partial-view.php") ?>