<?php

require(__DIR__ . "/../controllers/classes/Business.php");

$response = new stdClass();
$response->type = "default";
$response->title = "Default";
$response->text = "Default";


	checkResponse();
	
	if($response->type != "error"){

		$originalBusiness = Business::searchBusinessByAttr($db, "companyName", $_POST["name"]);
		$newBusiness = new Business($originalBusiness->getIdBusiness(), $_POST["name"], $_POST["contact"], $_POST["vat"], $_POST["company"], $_POST["coNumber"], $_POST["phone"], $_POST["email"], $_POST["webPage"], $_POST["status"]);
		
		if(Business::updateFullBusinesByAttr($db, $newBusiness, "companyName", $_POST["name"])){

			$response->type = "error";
			$response->title = "Error while updating data";
			$response->text = "There were some error while updating data";
		
		} else{

			$response->type = "success";
			$response->title = "Everything is OK";
			$response->text = "Data updated properly";
		
		}
	
	}
	
	echo(json_encode($response));
	
	/* FUNCTIONS */
	
	function checkResponse(){
		
		global $response;

		$data = $_POST;
		$arrayFields = array();
		
		for($k=0; $k<=count($data)-1; $k++) {

			if((trim($_POST[array_keys($_POST)[$k]]) == "") && (array_keys($_POST)[$k] !== "logo")){
				
				array_push($arrayFields, array_keys($_POST)[$k]);
			
			}
		
		}
		
		if(count($arrayFields) > 0){

			if(count($arrayFields) > 1){

				if(trim($_POST[array_keys($_POST)[$k]]) == ""){

					$response->type = "error";
					$response->title = "Error while sending data";
					$response->text = "The fields " . implode(", ", $arrayFields) . " can't be empty";
			
				}
			
			} else{

					if(trim($_POST[array_keys($_POST)[$k]]) == ""){

						$response->type = "error";
						$response->title = "Error while sending data";
						$response->text = "The field " . implode(", ", $arrayFields) . " can't be empty";
			
					}
				
				}
		
		}
	
	}

?>