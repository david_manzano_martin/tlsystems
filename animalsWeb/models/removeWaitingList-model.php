<?php

	require(__DIR__ . "/../controllers/classes/WaitingList.php");
	
	$response = new stdClass();
	$response->type = "default";
	$response->title = "Default";
	$response->text = "Default";
	
	if(WaitingList::removeWaitingListByNames($db, $user, $_POST["breedName"], $_POST["regionName"]) === 1){

		$response->type = "error";
		$response->title = "Error while removing";
		$response->text = "Was an error while removing your from the waiting list";
	
	} else{

		$response->type = "success";
		$response->title = "Everything was OK";
		$response->text = "You were removed from the waiting list";
	
	}
	
	echo(json_encode($response));
	
?>