<?php

	//echo("FileName: ". $_FILES["file"]["tmp_name"]);
	//print_r($_FILES);
	
	$response = new stdClass();
	$response->type = "default";
	$response->title = "Default";
	$response->text = "Default";
	
	$dir = __DIR__ . "/../imgs/uploads/users/" . $user->getUsername() . "/" . $_GET["businessName"];
	/*echo($dir);
	move_uploaded_file($_FILES["file"]["tmp_name"] . "/" . $_FILES["file"]["name"], ($dir . "test.txt"));*/
	
	foreach ($_FILES as $key) {
		//echo("Name: " . $key["name"]);
        $tmp_name = $key["tmp_name"];
        // basename() may prevent filesystem traversal attacks;
        // further validation/sanitation of the filename may be appropriate
        $name = $key["name"];
        $name = basename("logo" . substr($name, strrpos($name, "."), strlen($name)));
        if(!move_uploaded_file($tmp_name, "$dir/$name")){

			$response->type = "error";
			$response->title = "Error while updating data";
			$response->text = "There were some error while uploading logo";
        	
        } else{

				$response->type = "success";
				$response->title = "Image updated";
				$response->text = "Data updated properly";
        
        }
	}
	
	echo(json_encode($response));	

?>