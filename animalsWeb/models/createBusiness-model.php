<?php

require(__DIR__ . "/../controllers/classes/BusinessType.php");
require(__DIR__ . "/../controllers/classes/BusinessTypesRelations.php");
require(__DIR__ . "/../controllers/classes/UserBusinessRelation.php");
require(__DIR__ . "/../controllers/classes/Business.php");
require(__DIR__ . "/../controllers/links/regularExpressions.php");

$response = new stdClass();
$response->type = "default";
$response->title = "Default";
$response->text = "Default";

	//checkResponse();
	
	if($response->type != "error"){

		//TO CREATE THE FIRST PART (COMMON REGISTRATION)
		if(isset($_POST["coNumber"])){

			$businessType = BusinessType::getBusinessTypeByAttr($db, "businessId", $_POST["businessType"]);
			$newBusiness = new Business("null", $_POST["name"], $_POST["contact"], $_POST["vat"], $_POST["company"], $_POST["coNumber"], $_POST["phone"], $_POST["email"], $_POST["webPage"], "", $_POST["facebookPage"]);

			if(Business::countBusinessByAttr($db, "companyName", $newBusiness->getCompanyName())==0){

				if(Business::createBusiness($db, $newBusiness) == 1){		//IF WAS AN ERROR CREATING THE ACCOUNT

					$response->type = "error";
					$response->title = "Something was wrong";
					$response->text = "Error while creating the account";
		
				} else{					//IF ALL IS ALRIGHT

					$response->type = "success";
					$response->title = "Account created";
					$response->text = "You will be redirect in 5 seconds";

					$createdBusiness = Business::searchBusinessByAttr($db, "companyName", $newBusiness->getCompanyName());
					mkdir(__DIR__ . "/../imgs/uploads/users/" . $user->getUsername() . "/" . $newBusiness->getCompanyName());
					//uploadLogo();
					$_SESSION["lastBusinessId"] = $createdBusiness->getIdBusiness();
					UserBusinessRelation::createRelation($db, $user->getIdUser(), $createdBusiness->getIdBusiness());
		
				}

			} else{		//IF THE NAME ALREADY EXIST

				$response->type = "error";
				$response->title = "Error while creating the account";
				$response->text = "This name already exist";
	
			}
		
		} else{ 		//TO CREATE THE SECOND PART (SWITCH TO EACH TYPE OF BUSINESS)

			$flag = true;
			
			$selectedBusiness = Business::searchBusinessByAttr($db, "companyName", $_POST["name"]);
			$businessType = BusinessType::getBusinessTypeByAttr($db, "businessId", $_POST["businessType"]);
			BusinessTypesRelations::createRelation($db, $selectedBusiness->getIdBusiness(), $businessType->getBusinessId());
			
			
			switch(BusinessTypesRelations::getBusinessTypeByBusinessName($db, $_POST["name"])) {

				case "Pet Shop":{

					require(__DIR__ . "/../controllers/classes/PetShop.php");
					$localPetShop = new PetShop("null", $_POST["areaCovered"], $_POST["aboutShop"]);
					
					if(PetShop::addPetShop($db, $localPetShop) === 1){
						$flag = false;
					}
					
					break;
				
				}
				
				case "Breeder":{

					require(__DIR__ . "/../controllers/classes/Breeder.php");
					$localBreeder = new Breeder("null", "null", $selectedBusiness->getIdBusiness(), $_POST["cbrRegNo"], $_POST["kcBreedingName"], $_POST["vetName"], $_POST["vetReference"], $_POST["numberDogs"], $_POST["numberDams"], $_POST["aboutYou"], $_POST["cbrAccredited"], $_POST["breed1"]);
					
					if(Breeder::addBreeder($db, $localBreeder) === 1){
						$flag = false;
					}
					
					break;
				
				}
				
				case "Dog Trainer":{
					
					require(__DIR__ . "/../controllers/classes/DogTrainer.php");
					$localDogTrainer = new DogTrainer("null", "null", $selectedBusiness->getIdBusiness(), $_POST["validDbs"], $_POST["businessInsurance"], $_POST["insuranceProvider"], $_POST["canineFirstAidQualification"], $_POST["qualifications"], $_POST["kgGcdsApproved"], $_POST["areaCovered"], $_POST["productRangePrice"]);
									
					
					if(DogTrainer::addDogTrainer($db, $localDogTrainer) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
				
				case "Pet Craft":{
					
					require(__DIR__ . "/../controllers/classes/DogCraft.php");
					$localDogCraft = new DogCraft("null", "null", $selectedBusiness->getIdBusiness(), $_POST["productOverview"], $_POST["productNamePrice"], $_POST["LinkShop"]);
									
					if(DogCraft::addDogCraft($db, $localDogCraft) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
				
				case "Dog Walker":{
					
					require(__DIR__ . "/../controllers/classes/DogWalker.php");
					$localDogWalker = new DogWalker("null", "null", $selectedBusiness->getIdBusiness(), $_POST["validDbs"], $_POST["businessInsurance"], $_POST["insuranceProvider"], $_POST["canineAidQualification"], 
					$_POST["maxDogsTogheter"], $_POST["areasCovered"], $_POST["airCondition"], $_POST["pickUpDropService"], $_POST["productRangeAndPrice"]);
					
					if(DogWalker::addDogWalker($db, $localDogWalker) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
				
				case "Pet Sitter":{
					
					require(__DIR__ . "/../controllers/classes/PetSitting.php");
					$localPetSitter = new PetSitting("null", "null", $selectedBusiness->getIdBusiness(), $_POST["businessInsurance"], $_POST["insuranceProvider"],
					 $_POST["experienceYears"], $_POST["preferedDogs"], $_POST["areasCovered"], $_POST["productRangePrice"]);
					
					if(PetSitting::addPetSitting($db, $localPetSitter) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
				
				case "Dog Medical insurance Provider":{
					
					require(__DIR__ . "/../controllers/classes/DogMedicalInsuranceProvider.php");
					$localDogMedicalInsuranceProvider = new DogMedicalInsuranceProvider("null", "null", $selectedBusiness->getIdBusiness(),$_POST["specialistArea"], $_POST["productRangePrice"]);
			
					
					if(DogMedicalInsuranceProvider::addDogMedicalInsuranceProvider($db, $localDogMedicalInsuranceProvider) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
				
				case "Dog Behaviourist":{
					
					require(__DIR__ . "/../controllers/classes/DogBehaviourist.php");
					$localDogBehaviourist = new DogBehaviourist("null", "null", $selectedBusiness->getIdBusiness(), $_POST["businessInsurance"], $_POST["insuranceProvider"], $_POST["canineFirstAidQualification"],
					 $_POST["degree"], $_POST["qualification"], $_POST["examingBody"], $_POST["experienceYears"], $_POST["areaCovered"], $_POST["productRangePrice"]);
			
					
					if(DogBehaviourist::addDogBehaviourist($db, $localDogBehaviourist) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
				
				case "Dog Grommer":{
					
					require(__DIR__ . "/../controllers/classes/DogGrommer.php");
					$localDogGrommer = new DogGrommer("null", "null", $selectedBusiness->getIdBusiness(), $_POST["businessInsurance"], $_POST["insuranceProvider"], $_POST["canineFirstAidQualification"], 
					 $_POST["qualification"], $_POST["examingBody"], $_POST["experienceYears"], $_POST["areaCovered"], $_POST["pickUpDropService"], $_POST["productRangePrice"]);
			
					
					if(DogGrommer::addDogGrommer($db, $localDogGrommer) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
				
				case "Kennel":{
					
					require(__DIR__ . "/../controllers/classes/Kennel.php");
					
					$localKennel = new Kennel("null", "null", $selectedBusiness->getIdBusiness(), $_POST["businessInsurance"], $_POST["insuranceProvider"], $_POST["canineQualifications"], $_POST["noStuff"], 
					$_POST["noKennels"], $_POST["openingHours"], $_POST["exerciseMethods"], $_POST["noYears"], $_POST["areaCovered"], $_POST["pickUpDropService"], $_POST["productRangePrice"]);
			
					
					if(Kennel::addKennel($db, $localKennel) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
				
				case "Dog Food Provider":{
					
					require(__DIR__ . "/../controllers/classes/DogFoodProvider.php");
					
					$localDogFoodProvider = new DogFoodProvider("null", "null", $selectedBusiness->getIdBusiness(), $_POST["typeFood"], $_POST["areaCovered"], $_POST["homeDelivery"], $_POST["shipping"], 
					$_POST["personalConsultation"], $_POST["productRangePrice"]);
			
					
					if(DogFoodProvider::addDogFoodProvider($db, $localDogFoodProvider) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
				
				case "Pet Photographer":{
					
					require(__DIR__ . "/../controllers/classes/PetPhotographer.php");
					
					$localPetPhotographer = new PetPhotographer("null", "null", $selectedBusiness->getIdBusiness(), $_POST["businessINsurance"], $_POST["insuranceProvider"], $_POST["areaCovered"], $_POST["productRangePrice"], $_POST["animalsPhotographed"]);
					
					if(PetPhotographer::addPetPhotographer($db, $localPetPhotographer) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
				
				case "Home Boarder":{
					
					require(__DIR__ . "/../controllers/classes/HomeBoarder.php");
					
					
					$localHomeBoarder = new HomeBoarder("null", "null", $selectedBusiness->getIdBusiness(), $_POST["localAutorithyLicense"], $_POST["businessINsurance"], $_POST["insuranceProvider"], 
					$_POST["canineFirstAidQualification"], $_POST["noStaff"], $_POST["noDogLicenseBoard"], $_POST["preferedDogs"], $_POST["acceptNeuteredMales"], $_POST["acceptNeuteredfemales"], 
					$_POST["yearsExperience"], $_POST["methodOfExercise"], $_POST["otherFacilities"], $_POST["areaCovered"], $_POST["pickUpDropService"], $_POST["openingHours"], $_POST["productRangePrice"]);
					
					if(HomeBoarder::addHomeBoarder($db, $localHomeBoarder) === 1){
						$flag = false;
					}
					
					
					break;
				
				}
			
			}
			
			if(!$flag){

				$response->type = "error";
				$response->title = "Error while linking your profile";
				$response->text = "Error while linking your profile with your profession";
					
			} else{

				$response->type = "success";
				$response->title = "Profile linked properly";
				$response->text = "We will contact you when revise your profile";
					
			}
		
			
		}
		
	
	}
	
	echo(json_encode($response));
	
	/* FUNCTIONS */
	
	function uploadLogo(){

		print_r($_FILES);
		die("Finished");
		//$extension = substr($fileName, strrpos($fileName, ".") + 1, strlen($fileName));
		//print_r($array);

	}
	
	function checkResponse(){
		
		global $response;

		$data = $_POST;
		$arrayFields = array();
		//print_r($data);
		
		for($k=0; $k<=count($data)-1; $k++) {

			if(trim($_POST[array_keys($_POST)[$k]]) == ""){

				array_push($arrayFields, array_keys($_POST)[$k]);
			
			}
		
		}
		
		if(count($arrayFields) > 0){

			if(count($arrayFields) > 1){

				if(trim($_POST[array_keys($_POST)[$k]]) == ""){

					$response->type = "error";
					$response->title = "Error while sending data";
					$response->text = "The fields " . implode(", ", $arrayFields) . " can't be empty";
			
				}
			
			} else{

					if(trim($_POST[array_keys($_POST)[$k]]) == ""){

						$response->type = "error";
						$response->title = "Error while sending data";
						$response->text = "The field " . implode(", ", $arrayFields) . " can't be empty";
			
					}
				
				}
		}
		
		$arrayDoesntMatch = checkArrayToRegexp($data);
		
		if(count($arrayDoesntMatch)>0){
			$response->type = "error";
			$response->title = "Error while sending data";
			$response->text = "The fields " . implode(", ", $arrayDoesntMatch) . " doesn't match with the requeriments";
		}
	
	}

?>