<?php

	$response = new stdClass();
	$response->type = "default";
	$response->title = "Default";
	$response->text = "Default";
	
	$dir = __DIR__ . "/../imgs/uploads/users/" . $user->getUsername();
	/*echo($dir);
	move_uploaded_file($_FILES["file"]["tmp_name"] . "/" . $_FILES["file"]["name"], ($dir . "test.txt"));*/
	
	$arrayLogos = glob("imgs/uploads/users/" . $user->getUsername() . "/profileImg.*");
	
	foreach($arrayLogos as $file){
		unlink($file);
	}
	
	foreach ($_FILES as $key) {
		//echo("Name: " . $key["name"]);
        $tmp_name = $key["tmp_name"];
        // basename() may prevent filesystem traversal attacks;
        // further validation/sanitation of the filename may be appropriate
        $name = $key["name"];
        $name = basename("profileImg" . substr($name, strrpos($name, "."), strlen($name)));
        if(!move_uploaded_file($tmp_name, "$dir/$name")){

			$response->type = "error";
			$response->title = "Error while updating data";
			$response->text = "There were some error while uploading logo";
        	
        } else{

				$response->type = "success";
				$response->title = "Everything is OK";
				$response->text = "Refresh the page to see the changes";
        
        }
	}
	
	echo(json_encode($response));	

?>