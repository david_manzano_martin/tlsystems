<?php

/* USER CLASS IMPORTED ON index.php FILE */

$response = new stdClass();

checkResponse();

if($response->type != "error"){

	if(User::logIn($db, $_POST["username"], $_POST["password"]) === 1){

		$response->type = "error";
		$response->title = "Error while log in";
		$response->text = "The username or password is incorrect";

	} else{

		$response->type = "success";
		$response->title = "Welcome " . $_POST["username"];
		$response->text = "You will be redirect in 3 seconds";

	}

}



echo(json_encode($response));

/* FUNCTIONS */
	
	function checkResponse(){
		
		global $response;

		$data = $_POST;
		$arrayFields = array();
		
		/*for($k=0; $k<=count($data)-1; $k++) {

			if(trim($_POST[array_keys($_POST)[$k]]) == ""){

				$response->type = "error";
				$response->title = "Error while sending data";
				$response->text = "The field " . array_keys($_POST)[$k] . " can't be empty";
			
			}
		
		}*/
		for($k=0; $k<=count($data)-1; $k++) {

			if(trim($_POST[array_keys($_POST)[$k]]) == ""){

				array_push($arrayFields, array_keys($_POST)[$k]);
			
			}
		
		}
		
		if(count($arrayFields) > 0){

			if(count($arrayFields) > 1){

				if(trim($_POST[array_keys($_POST)[$k]]) == ""){

					$response->type = "error";
					$response->title = "Error while sending data";
					$response->text = "The fields " . implode(", ", $arrayFields) . " can't be empty";
			
				}
			
			} else{

					if(trim($_POST[array_keys($_POST)[$k]]) == ""){

						$response->type = "error";
						$response->title = "Error while sending data";
						$response->text = "The field " . implode(", ", $arrayFields) . " can't be empty";
			
					}
				
				}
		
		}
	
	}

?>