<?php

	/* USER CLASS IMPORTED ON index.php FILE */
	require(__DIR__ . "/../controllers/links/regularExpressions.php");
	
	$response = new stdClass();
	$response->type = "default";
	$response->title = "Default";
	$response->text = "Default";
	
	checkResponse();
	
	
	if($response->type != "error"){

		$localUser = new User("null", "null", $_POST["username"], $_POST["password"], 1);		

		switch(User::createUser($db, $localUser)){
			
			case 0:{

				$response->type = "success";
				$response->title = "User created correctly";
				$response->text = "You will be redirect automatically";

				User::logIn($db, $_POST["username"], $_POST["password"]);
				
				mkdir(__DIR__ . "/../imgs/uploads/users/" . $_POST["username"]);
				
				break;
			
			}
			
			case 1:{

				$response->type = "error";
				$response->title = "Error while creating user";
				$response->text = "Error while creating user";
				break;
			
			}
			
			case 2:{

				$response->type = "error";
				$response->title = "Error while creating user";
				$response->text = "This username is already in use";
				break;
			
			}
		
		}
		
	}
	
	echo json_encode($response);
	
	
	/* FUNCTIONS */
	
	function checkResponse(){
		
		global $response;

		$data = $_POST;
		$arrayFields = array();
		
		for($k=0; $k<=count($data)-1; $k++) {

			if(trim($_POST[array_keys($_POST)[$k]]) == ""){

				array_push($arrayFields, array_keys($_POST)[$k]);
			
			}
		
		}
		
		if(count($arrayFields) > 0){

			if(count($arrayFields) > 1){

				if(trim($_POST[array_keys($_POST)[$k]]) == ""){

					$response->type = "error";
					$response->title = "Error while sending data";
					$response->text = "The fields " . implode(", ", $arrayFields) . " can't be empty";
			
				}
			
			} else{

					if(trim($_POST[array_keys($_POST)[$k]]) == ""){

						$response->type = "error";
						$response->title = "Error while sending data";
						$response->text = "The field " . implode(", ", $arrayFields) . " can't be empty";
			
					}
				
				}
		
		}
		
		$arrayDoesntMatch = checkArrayToRegexp($data);
		
		if(count($arrayDoesntMatch)>0){
			$response->type = "error";
			$response->title = "Error while sending data";
			$response->text = "The fields " . implode(", ", $arrayDoesntMatch) . " doesn't match with the requeriments";
		}
	
	}

?>