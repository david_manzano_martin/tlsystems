<?php

require(__DIR__ . "/../../controllers/classes/HealthTest.php");

if(!User::checkPermissionRequired(3, $user)){
	die("You don't have access");
}

	$response = new stdClass();
	$response->type = "default";
	$response->title = "Default";
	$response->text = "Default";

$temporalHealthTest = new HealthTest($_POST["idHealthTest"], $_POST["healthTestName"], $_POST["description"]);

//echo($temporalHealthTest = HealthTest::getHealthTestByAttr($db, "idHealthTest", $_POST["idHealthTest"]));

//if(HealthTest::getHealthTestByAttr($db, "idHealthTest", $temporalHealthTest->getIdHealthTest()) === 1){

	if($temporalHealthTest->updateHealthTest($db) === 1){

		$response->type = "error";
		$response->title = "Error while updating";
		$response->text = "Error while updating the health test";

	} else{

		$response->type = "success";
		$response->title = "Everything was OK";
		$response->text = "The health test was updated successfully";

	}

/*} else{

	$response->type = "error";
	$response->title = "Error while adding";
	$response->text = "The test doesn't exist";
	
}*/

echo(json_encode($response));

?>