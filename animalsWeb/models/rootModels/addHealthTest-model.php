<?php

require(__DIR__ . "/../../controllers/classes/HealthTest.php");

if(!User::checkPermissionRequired(3, $user)){
	die("You don't have access");
}

	$response = new stdClass();
	$response->type = "default";
	$response->title = "Default";
	$response->text = "Default";

$temporalHealthTest = HealthTest::getHealthTestByAttr($db, "idHealthTest", $_POST["idHealthTest"]);

if($temporalHealthTest !== 1){
	
	$temporalHealthTest->setHealthTestName($_POST["healthTestName"]);
	$temporalHealthTest->setHealthTestDescription($_POST["description"]);

	if($temporalHealthTest->addHealthTest($db) === 1){

		$response->type = "error";
		$response->title = "Error while adding";
		$response->text = "Error while adding the health test";

	} else{

		$response->type = "success";
		$response->title = "Everything was OK";
		$response->text = "The health test was created successfully";

	}

} else{

	$response->type = "error";
	$response->title = "Error while adding";
	$response->text = "The test already exist";
	
}

echo(json_encode($response));

?>