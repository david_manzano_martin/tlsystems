<?php

require(__DIR__ . "/../controllers/classes/WaitingList.php");
require(__DIR__ . "/../controllers/classes/Breed.php");
require(__DIR__ . "/../controllers/classes/UkRegion.php");

	$response = new stdClass();
	$response->type = "default";
	$response->title = "Default";
	$response->text = "Default";


/*echo($_POST["idBreed"]);
echo($_POST["zoneId"]);*/

$localWaitingList = new WaitingList("null", $user->getIdUser(), $_POST["idBreed"], $_POST["zoneId"]);

if(WaitingList::checkIfExist($db, $localWaitingList) > 0){

	$response->type = "error";
	$response->title = "Error while adding";
	$response->text = "You are already on this waiting list";

} else{

	if(WaitingList::addWaitingList($db, $localWaitingList) === 1){

	
		$response->type = "error";
		$response->title = "Error while adding";
		$response->text = "Was an error while adding you to the waiting list";
	
	} else{

		$response->type = "success";
		$response->title = "Everything was OK";
		$response->text = "You were added to the waiting list";
	
	}

}
	
	echo(json_encode($response))
	

?>