<?php

require(__DIR__ . "/classes/Breed.php");


if($_SERVER["REQUEST_METHOD"] == "POST"){
	
	$breed1 = new Breed("null",strtoupper($_POST["breedName"]) ,$_POST["firstDescription"] ,$_POST["secondDescription"]);
	
	addBreed($db, $breed1);
	uploadPictures(strtoupper($_POST["breedName"]));
	
}

require(__DIR__ . "/../views/breedRegistration-view.php");

/* FUNCTIONS */

function addBreed($db, $breedElement){

	$query = "INSERT INTO breeds (idBreed, breedName, firstDescription, secondDescription) VALUES(" . $breedElement->getIdBreed() . ", '" . $breedElement->getBreedName() . "' , '" . $breedElement->getFirstDescription() . "', '" . $breedElement->getSecondDescription() . "')";
	if(!$db->query($query)){

		echo("Error while adding the product" . $db->error());
	
	}
	
}

function uploadPictures($name){

	$arrayPermisions = [];
	
	$dir = __DIR__ . "/../imgs/uploads/breeds/";
	$cont = 0;
	foreach($_FILES as $file){
		$cont++;
		if (move_uploaded_file($file['tmp_name'], $dir . $name . $cont . ".jpg")) {
        //print "Received {$file['name']} - its size is {$file['size']}";
        array_push($arrayPermisions, $dir . $file['name']);
    } else {
        //print "Upload failed!";
    }
	
	}

}

?>