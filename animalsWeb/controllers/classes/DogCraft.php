
<?php
class DogCraft {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idDogCraft;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $ecbrNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productOverview;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productRangeAndPrice;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $linkToShop;
    
    public function __construct($idDogCraft = "null", $ecbrNo = "null", $idBusiness = "null", $productOverview = "null", $productRangeAndPrice = "null", $linkToShop = "null"){

		$this->idDogCraft = $idDogCraft;
		$this->ecbrNo = $ecbrNo;
		$this->idBusiness = $idBusiness;
		$this->productOverview = $productOverview;
		$this->productRangeAndPrice = $productRangeAndPrice;
		$this->linkToShop = $linkToShop;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdDogCraft() {
        return $this->idDogCraft;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idDogCraft ARGDESCRIPTION
     */
    public function setIdDogCraft($idDogCraft) {
        $this->idDogCraft = $idDogCraft;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getEcbrNo() {
        return $this->ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $ecbrNo ARGDESCRIPTION
     */
    public function setEcbrNo($ecbrNo) {
        $this->ecbrNo = $ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductOverview() {
        return $this->productOverview;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productOverview ARGDESCRIPTION
     */
    public function setProductOverview($productOverview) {
        $this->productOverview = $productOverview;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductRangeAndPrice() {
        return $this->productRangeAndPrice;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productRangeAndPrice ARGDESCRIPTION
     */
    public function setProductRangeAndPrice($productRangeAndPrice) {
        $this->productRangeAndPrice = $productRangeAndPrice;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getLinkToShop() {
        return $this->linkToShop;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $linkToShop ARGDESCRIPTION
     */
    public function setLinkToShop($linkToShop) {
        $this->linkToShop = $linkToShop;
    }
    
    /* OTHER FUNCTIONS */
    
    /* STATIC FUNCTIONS */
    public static function addDogCraft($db, $dogCraft, $tableName = "dogCraftFields"){
		
		$query = "INSERT INTO " . $tableName . " (idDogCraft, ecbrNo, idBusiness, productOverview, productRangeAndPrice, linkToShop) VALUES (
		" . $dogCraft->getIdDogCraft() . ", 
		'" . $dogCraft->getEcbrNo() . "', 
		'" . $dogCraft->getIdBusiness() . "', 
		'" . $dogCraft->getProductOverview() . "', 
		'" . $dogCraft->getProductRangeAndPrice() . "', 
		'" . $dogCraft->getLinkToShop() . "'
		)";
		
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
}

