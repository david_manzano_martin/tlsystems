<?php
class DogGrommer {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idDogGrommer;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $ecbrNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $businessInsurance;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $insuranceProvider;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $canineFirstAidQualification;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $qualification;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $examingBody;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $experienceYears;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $areaCovered;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $pickUpDropService;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productRangeAndPrice;
    
    public function __construct($idDogGrommer = "null", $ecbrNo = "null", $idBusiness = "null", $businessInsurance = "null", $insuranceProvider = "null", $canineFirstAidQualification = "null",
     $qualification = "null", $examingBody = "null", $experienceYears = "null", $areaCovered = "null", $pickUpDropService = "null", $productRangeAndPrice = "null"){

		$this->idDogGrommer = $idDogGrommer;
		$this->ecbrNo = $ecbrNo;
		$this->idBusiness = $idBusiness;
		$this->businessInsurance = $businessInsurance;
		$this->insuranceProvider = $insuranceProvider;
		$this->canineFirstAidQualification = $canineFirstAidQualification;
		$this->qualification = $qualification;
		$this->examingBody = $examingBody;
		$this->experienceYears = $experienceYears;
		$this->areaCovered = $areaCovered;
		$this->pickUpDropService = $pickUpDropService;
		$this->productRangeAndPrice = $productRangeAndPrice;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdDogGrommer() {
        return $this->idDogGrommer;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idDogGrommer ARGDESCRIPTION
     */
    public function setIdDogGrommer($idDogGrommer) {
        $this->idDogGrommer = $idDogGrommer;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getEcbrNo() {
        return $this->ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $ecbrNo ARGDESCRIPTION
     */
    public function setEcbrNo($ecbrNo) {
        $this->ecbrNo = $ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBusinessInsurance() {
        return $this->businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessInsurance ARGDESCRIPTION
     */
    public function setBusinessInsurance($businessInsurance) {
        $this->businessInsurance = $businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getInsuranceProvider() {
        return $this->insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $insuranceProvider ARGDESCRIPTION
     */
    public function setInsuranceProvider($insuranceProvider) {
        $this->insuranceProvider = $insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getCanineFirstAidQualification() {
        return $this->canineFirstAidQualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $canineFirstAidQualification ARGDESCRIPTION
     */
    public function setCanineFirstAidQualification($canineFirstAidQualification) {
        $this->canineFirstAidQualification = $canineFirstAidQualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getQualification() {
        return $this->qualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $qualification ARGDESCRIPTION
     */
    public function setQualification($qualification) {
        $this->qualification = $qualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getExamingBody() {
        return $this->examingBody;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $examingBody ARGDESCRIPTION
     */
    public function setExamingBody($examingBody) {
        $this->examingBody = $examingBody;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getExperienceYears() {
        return $this->experienceYears;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $experienceYears ARGDESCRIPTION
     */
    public function setExperienceYears($experienceYears) {
        $this->experienceYears = $experienceYears;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAreaCovered() {
        return $this->areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $areaCovered ARGDESCRIPTION
     */
    public function setAreaCovered($areaCovered) {
        $this->areaCovered = $areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getPickUpDropService() {
        return $this->pickUpDropService;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $pickUpDropService ARGDESCRIPTION
     */
    public function setPickUpDropService($pickUpDropService) {
        $this->pickUpDropService = $pickUpDropService;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductRangeAndPrice() {
        return $this->productRangeAndPrice;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productRangeAndPrice ARGDESCRIPTION
     */
    public function setProductRangeAndPrice($productRangeAndPrice) {
        $this->productRangeAndPrice = $productRangeAndPrice;
    }
    
    /* OTHER FUNCTIONS */
    
    //STATIC FUNCTIONS
    public static function addDogGrommer($db, $dogGrommer, $tableName = "dogGrommerFields"){
    	
		$query = "INSERT INTO " . $tableName . " (idDogGrommer, ecbrNo, idBusiness, businessInsurance, insuranceProvider, canineFirstAidQualification, qualification, examingBody, experienceYears, 
		areaCovered, pickUpDropService, productRangeAndPrice) VALUES (
		" . $dogGrommer->getIdDogGrommer() . ", 
		'" . $dogGrommer->getEcbrNo() . "',
		'" . $dogGrommer->getIdBusiness() . "',
		'" . $dogGrommer->getBusinessInsurance() . "',
		'" . $dogGrommer->getInsuranceProvider() . "',
		'" . $dogGrommer->getCanineFirstAidQualification() . "',
		'" . $dogGrommer->getQualification() . "',
		'" . $dogGrommer->getExamingBody() . "',
		'" . $dogGrommer->getExperienceYears() . "',
		'" . $dogGrommer->getAreaCovered() . "',
		'" . $dogGrommer->getPickUpDropService() . "',
		'" . $dogGrommer->getProductRangeAndPrice() . "'
		)";
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
}

?>