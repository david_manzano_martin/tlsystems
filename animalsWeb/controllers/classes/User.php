<?php
class User {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idUser;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $username;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $password;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $permissionLevel;
    
    public $imagePath;
    
    public function __construct($idUser = "null", $idBusiness = "null", $username = "anonymous", $password = "0000", $permissionLevel = 0){
    	
		$this->idUser = $idUser;
		$this->idBusiness = $idBusiness;
		$this->username = $username;
		$this->password = $password;
		$this->permissionLevel = $permissionLevel;
		
		$imagePathPng = "imgs/uploads/" . $this->username . "/profileImg.png";
		$imagePathJpg = "imgs/uploads/users/" . $this->username . "/profileImg.jpg";
		
		$arrayLogos = glob("imgs/uploads/users/" . $this->username . "/profileImg.*");
		
		if(count($arrayLogos)>0){

			$this->imagePath = $arrayLogos[0];
		
		} else {

			$this->imagePath = "imgs/profileImage.png";
		
		}
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdUser() {
        return $this->idUser;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idUser ARGDESCRIPTION
     */
    public function setIdUser($idUser) {
        $this->idUser = $idUser;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $username ARGDESCRIPTION
     */
    public function setUsername($username) {
        $this->username = $username;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getpassword() {
        return $this->password;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $password ARGDESCRIPTION
     */
    public function setpassword($password) {
        $this->password = $password;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getPermissionLevel() {
        return $this->permissionLevel;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $permissionLevel ARGDESCRIPTION
     */
    public function setPermissionLevel($permissionLevel) {
        $this->permissionLevel = $permissionLevel;
    }
    
    public function getImagePath() {
        return $this->imagePath;
    }
    
    public function setimagePath($imagePath) {
        $this->imagePath = $imagePath;
    }

	/* OTHER FUNCTIONS */

/* STATIC FUNCTIONS */

public static function checkPermissionRequired($permissionRequired, $user){

	if($user->getPermissionLevel() >= $permissionRequired){
		return true;
	} else{
		return false;
	}

}

/* IF USER EXIST RETURN  2, IF THERE IS ANY ERROR CREATING RETURN 1 OR 0 IF EVERITHING IS OK*/
public static function createUser($db, $userTemporal, $tableName = "users"){
	
	$result = 0;
	if(User::getUserByAttr($db, "username", $userTemporal->getUsername()) !== 1){

		$result = 2;
		
	} else{

		$userTemporal->setpassword(md5($userTemporal->getpassword()));
	
		$query = "INSERT INTO " . $tableName . " (idUser, username, password, permissionLevel) VALUES (
	'" . $userTemporal->getIdUser() . "', 
	'" . $userTemporal->getUsername() . "', 
	'" . $userTemporal->getPassword() . "', 
	'" . $userTemporal->getPermissionLevel() . "'	
		)";
	
		if(!$db->query($query)){

			$result = 1;
	
		}
	
	}
	
	
	
	
	return $result;

}

public static function checkUserLogged($db){

	global $user;
	if((User::getUserByAttr($db, "username", json_decode($_SESSION["user"])->username) !== 1) && (User::getUserByAttr($db, "password", json_decode($_SESSION["user"])->password) !== 1)){

		$localUser = json_decode($_SESSION["user"]);
		$user = new User($localUser->idUser, $localUser->idBusiness, $localUser->username, $localUser->password, $localUser->permissionLevel);
	
	} else{

		$user = new User();
	
	}

}

public static function logOut(){

	$localUser = new User();
	$_SESSION["user"] = json_encode($localUser);

}

public static function logIn($db, $username, $password, $tableName = "users"){
	
	$localUser = 1;
	$password = md5($password);
	
	$query = "SELECT* FROM " . $tableName . " WHERE username = '" . $username . "' AND password = '" . $password . "'";
	$result = $db->query($query);
	
	while($row = $result->fetch_assoc()){

		$localUser = new User($row["idUser"], $row["idBusiness"], $row["username"], $row["password"], $row["permissionLevel"]);
		$_SESSION["user"] = json_encode($localUser);
	
	}
	
	return $localUser;

}

public static function getUserByAttr($db, $attrName, $attrVal, $tableName = "users"){

	$localUser = 1;
	$query = "SELECT* FROM " . $tableName . " WHERE " . $attrName . " = '" . $attrVal . "'";
	$result = $db->query($query);
	
	while($row = $result->fetch_assoc()){

		$localUser = new User($row["idUser"], $row["idBusiness"], $row["username"], $row["password"], $row["permissionLevel"]);
	
	}
	
	return $localUser;

}    
    
}


?>