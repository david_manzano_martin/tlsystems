<?php
class BreedHealthTestRelation {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBreedHealthTest;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBreed;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idHealthTest;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $importance;
    
    public function __construct($idBreedHealthTest = "null", $idBreed = "null", $idHealthTest = "null", $importance = "null"){

		$this->idBreedHealthTest = $idBreedHealthTest;
		$this->idBreed = $idBreed;
		$this->idHealthTest = $idHealthTest;
		$this->importance = $importance;

    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBreedHealthTest() {
        return $this->idBreedHealthTest;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBreedHealthTest ARGDESCRIPTION
     */
    public function setIdBreedHealthTest($idBreedHealthTest) {
        $this->idBreedHealthTest = $idBreedHealthTest;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBreed() {
        return $this->idBreed;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBreed ARGDESCRIPTION
     */
    public function setIdBreed($idBreed) {
        $this->idBreed = $idBreed;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdHealthTest() {
        return $this->idHealthTest;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idHealthTest ARGDESCRIPTION
     */
    public function setIdHealthTest($idHealthTest) {
        $this->idHealthTest = $idHealthTest;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getImportance() {
        return $this->importance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $importance ARGDESCRIPTION
     */
    public function setImportance($importance) {
        $this->importance = $importance;
    }
    
    /* OTHER FUNCTIONS */
    
    /* STATIC FUNCTIONS */
    public static function getHealthTestsByBreedByImportance($db, $breed, $importance, $tableName = "breedHealthTestRelations"){

			$arrayResults = array();
			
			$query = "SELECT * FROM 
				(breedHealthTestRelations INNER JOIN breeds on breedHealthTestRelations.idBreed = breeds.idBreed) 
				INNER JOIN healthTest on breedHealthTestRelations.idHealthTest = healthTest.idHealthTest
				WHERE breeds.idBreed = '" . $breed->getIdBreed() . "' AND importance = '" . $importance . "'";
				
			$result = $db->query($query);
		
			while($row = $result->fetch_assoc()){
				$localHealthTest = new HealthTest($row["idHealthTest"], $row["healthTestName"], $row["healthTestDescription"]);
				array_push($arrayResults, $localHealthTest);
			}
			
			return $arrayResults;
		
		}
    
		public static function getHealthTestsByBreed($db, $breed, $tableName = "breedHealthTestRelations"){

			$arrayResults = array();
			
			$query = "SELECT * FROM 
				(breedHealthTestRelations INNER JOIN breeds on breedHealthTestRelations.idBreed = breeds.idBreed) 
				INNER JOIN healthTest on breedHealthTestRelations.idHealthTest = healthTest.idHealthTest
				WHERE breeds.idBreed = '" . $breed->getIdBreed() . "'";
				
				
			$result = $db->query($query);
		
			while($row = $result->fetch_assoc()){
				$localHealthTest = new HealthTest($row["idHealthTest"], $row["healthTestName"], $row["healthTestDescription"]);
				array_push($arrayResults, $localHealthTest);
			}
			
			return $arrayResults;
		
		}
    
    
    	public static function getAllHealthTestRelations($db, $tableName = "breedHealthTestRelations"){

		$arrayResults = array();
		
		$query = "SELECT * FROM " . $tableName;
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()){
			$localHealthTestRelation = new BreedHealthTestRelation($row["idHealthTestRelation"], $row["idBreed"], $row["idHealthTest"], $row["importance"]);
			array_push($arrayResults, $localHealthTestRelation);
		}
		
		return $arrayResults;
	
	}
    
    
}

?>