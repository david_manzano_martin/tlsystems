<?php
class PetSitting {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idPetSitting;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $ecbrNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $businessInsurance;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $insuranceProvider;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $experienceYears;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $preferedDogs;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $areaCovered;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productRangeAndPrice;
    
    public function __construct($idPetSitting = "null", $ecbrNom = "null", $idBusiness = "null", $businessInsurance = "null", $insuranceProvider = "null", $experienceYears = "null", 
    $preferedDogs = "null", $areaCovered = "null", $productRangeAndPrice = "null"){

		$this->idPetSitting = $idPetSitting;
		$this->ecbrNo = $ecbrNo;
		$this->idBusiness = $idBusiness;
		$this->businessInsurance = $businessInsurance;
		$this->insuranceProvider = $insuranceProvider;
		$this->experienceYears = $experienceYears;
		$this->preferedDogs = $preferedDogs;
		$this->areaCovered = $areaCovered;
		$this->productRangeAndPrice = $productRangeAndPrice;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdPetSitting() {
        return $this->idPetSitting;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idPetSitting ARGDESCRIPTION
     */
    public function setIdPetSitting($idPetSitting) {
        $this->idPetSitting = $idPetSitting;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getEcbrNo() {
        return $this->ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $ecbrNo ARGDESCRIPTION
     */
    public function setEcbrNo($ecbrNo) {
        $this->ecbrNo = $ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBusinessInsurance() {
        return $this->businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessInsurance ARGDESCRIPTION
     */
    public function setBusinessInsurance($businessInsurance) {
        $this->businessInsurance = $businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getInsuranceProvider() {
        return $this->insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $insuranceProvider ARGDESCRIPTION
     */
    public function setInsuranceProvider($insuranceProvider) {
        $this->insuranceProvider = $insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getExperienceYears() {
        return $this->experienceYears;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $experienceYears ARGDESCRIPTION
     */
    public function setExperienceYears($experienceYears) {
        $this->experienceYears = $experienceYears;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getPreferedDogs() {
        return $this->preferedDogs;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $preferedDogs ARGDESCRIPTION
     */
    public function setPreferedDogs($preferedDogs) {
        $this->preferedDogs = $preferedDogs;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAreaCovered() {
        return $this->areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $areaCovered ARGDESCRIPTION
     */
    public function setAreaCovered($areaCovered) {
        $this->areaCovered = $areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductRangeAndPrice() {
        return $this->productRangeAndPrice;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productRangeAndPrice ARGDESCRIPTION
     */
    public function setProductRangeAndPrice($productRangeAndPrice) {
        $this->productRangeAndPrice = $productRangeAndPrice;
    }

	/* OTHER FUNCTIONS */
	
	//STATIC FUNCTIONS
    public static function addPetSitting($db, $petSitting, $tableName = "petSittingFields"){
    	
		$query = "INSERT INTO " . $tableName . " (idPetSitting, ecbrNo, idBusiness, businessInsurance, insuranceProvider, experienceYears, preferedDogs, areaCovered, productRangeAndPrice) VALUES (
		" . $petSitting->getIdPetSitting() . ", 
		'" . $petSitting->getEcbrNo() . "',
		'" . $petSitting->getIdBusiness() . "',
		'" . $petSitting->getBusinessInsurance() . "',
		'" . $petSitting->getInsuranceProvider() . "',
		'" . $petSitting->getExperienceYears() . "',
		'" . $petSitting->getPreferedDogs() . "',
		'" . $petSitting->getAreaCovered() . "',
		'" . $petSitting->getProductRangeAndPrice() . "' 
		)";
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
    
}

?>