
<?php
class BusinessType {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $businessId;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $businessName;
    
    public function __construct($businessId, $businessName){

		$this->businessId = $businessId;
		$this->businessName = $businessName;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBusinessId() {
        return $this->businessId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessId ARGDESCRIPTION
     */
    public function setBusinessId($businessId) {
        $this->businessId = $businessId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBusinessName() {
        return $this->businessName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessName ARGDESCRIPTION
     */
    public function setBusinessName($businessName) {
        $this->businessName = $businessName;
    }

		/* STATIC FUNCTIONS */

		public static function getBusinessTypeByAttr($db, $attrName, $attrVal){

			$localBusiness = 1;

			$query = "SELECT * FROM businessTypes WHERE " . $attrName . " = '" . $attrVal . "'";
			
			$result = $db->query($query);
			
			while($row = $result->fetch_assoc()){

				$localBusiness = new BusinessType($row["businessId"], $row["businessName"]);
			
			}
			
			return $localBusiness;
		
		}		
		
		public static function getAllBusiness($db){

			$arrayResults = array();
			
			$query = "SELECT * FROM businessTypes ORDER BY businessName ASC";
			$result = $db->query($query);
			
			while($row = $result->fetch_assoc()){

				$localBusiness = new BusinessType($row["businessId"], $row["businessName"]);
				array_push($arrayResults, $localBusiness);
			
			}
			
			return $arrayResults;
		
		}  
    
}

?>