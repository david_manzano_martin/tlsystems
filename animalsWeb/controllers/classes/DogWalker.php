<?php
class DogWalker {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idDogWalker;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $ecbrNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $validDbsCheck;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $businessInsurance;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $insuranceProvider;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $canineFirstAidQualification;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $maxNoOfDogsWalkedTogether;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $areasCovered;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $airCondition;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $pickUpDropService;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productRangeAndPrice;
    
    public function __construct($idDogWalker = "null", $ecbrNo = "null", $idBusiness = "null", $validDbsCheck = "null", $businessInsurance = "null", $insuranceProvider = "null", 
    $canineFirstAidQualification = "null", $maxNoOfDogsWalkedTogether = "null", $areasCovered = "null", $airCondition = "null", $pickUpDropService = "null", $productRangeAndPrice = "null"){

		$this->idDogWalker = $idDogWalker;
		$this->ecbrNo = $ecbrNo;
		$this->idBusiness = $idBusiness;
		$this->validDbsCheck = $validDbsCheck;
		$this->businessInsurance = $businessInsurance;
		$this->insuranceProvider = $insuranceProvider;
		$this->canineFirstAidQualification = $canineFirstAidQualification;
		$this->maxNoOfDogsWalkedTogethe = $maxNoOfDogsWalkedTogether;
		$this->areasCovered = $areasCovered;
		$this->airCondition = $airCondition;
		$this->pickUpDropService = $pickUpDropService;
		$this->productRangeAndPrice = $productRangeAndPrice;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdDogWalker() {
        return $this->idDogWalker;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idDogWalker ARGDESCRIPTION
     */
    public function setIdDogWalker($idDogWalker) {
        $this->idDogWalker = $idDogWalker;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getEcbrNo() {
        return $this->ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $ecbrNo ARGDESCRIPTION
     */
    public function setEcbrNo($ecbrNo) {
        $this->ecbrNo = $ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getValidDbsCheck() {
        return $this->validDbsCheck;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $validDbsCheck ARGDESCRIPTION
     */
    public function setValidDbsCheck($validDbsCheck) {
        $this->validDbsCheck = $validDbsCheck;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBusinessInsurance() {
        return $this->businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessInsurance ARGDESCRIPTION
     */
    public function setBusinessInsurance($businessInsurance) {
        $this->businessInsurance = $businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getInsuranceProvider() {
        return $this->insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $insuranceProvider ARGDESCRIPTION
     */
    public function setInsuranceProvider($insuranceProvider) {
        $this->insuranceProvider = $insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getCanineFirstAidQualification() {
        return $this->canineFirstAidQualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $canineFirstAidQualification ARGDESCRIPTION
     */
    public function setCanineFirstAidQualification($canineFirstAidQualification) {
        $this->canineFirstAidQualification = $canineFirstAidQualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getMaxNoOfDogsWalkedTogether() {
        return $this->maxNoOfDogsWalkedTogether;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $maxNoOfDogsWalkedTogether ARGDESCRIPTION
     */
    public function setMaxNoOfDogsWalkedTogether($maxNoOfDogsWalkedTogether) {
        $this->maxNoOfDogsWalkedTogether = $maxNoOfDogsWalkedTogether;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAreasCovered() {
        return $this->areasCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $areasCovered ARGDESCRIPTION
     */
    public function setAreasCovered($areasCovered) {
        $this->areasCovered = $areasCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAirCondition() {
        return $this->airCondition;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $airCondition ARGDESCRIPTION
     */
    public function setAirCondition($airCondition) {
        $this->airCondition = $airCondition;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getPickUpDropService() {
        return $this->pickUpDropService;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $pickUpDropService ARGDESCRIPTION
     */
    public function setPickUpDropService($pickUpDropService) {
        $this->pickUpDropService = $pickUpDropService;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductRangeAndPrice() {
        return $this->productRangeAndPrice;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productRangeAndPrice ARGDESCRIPTION
     */
    public function setProductRangeAndPrice($productRangeAndPrice) {
        $this->productRangeAndPrice = $productRangeAndPrice;
    }

	/* OTHER FUNCTIONS */
	
	//STATIC FUNCTIONS
	public static function addDogWalker($db, $dogWalker, $tableName = "dogWalkerFields"){
		
		$query = "INSERT INTO " . $tableName . " (idDogWalker, ecbrNo, idBusiness, validDbsCheck, businessInsurance, insuranceProvider, canineFirstAidQualification, maxNoOfDogsWalkedTogether, 
		areasCovered, airCondition, pickUpDropService, productRangeAndPrice) VALUES (
		" . $dogWalker->getIdDogWalker() . ", 
		'" . $dogWalker->getEcbrNo() . "', 
		'" . $dogWalker->getIdBusiness() . "',
		'" . $dogWalker->getValidDbsCheck() . "',
		'" . $dogWalker->getBusinessInsurance() . "',
		'" . $dogWalker->getInsuranceProvider() . "',
		'" . $dogWalker->getCanineFirstAidQualification() . "',
		'" . $dogWalker->getMaxNoOfDogsWalkedTogether() . "',
		'" . $dogWalker->getAreasCovered() . "',
		'" . $dogWalker->getAirCondition() . "',
		'" . $dogWalker->getPickUpDropService() . "',
		'" . $dogWalker->getProductRangeAndPrice() . "'
		)";
		
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
    
}

?>