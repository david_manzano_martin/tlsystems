
<?php
class PetPhotographer {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idPetPhotographer;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $ecbrNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $businessINsurance;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $insuranceProvider;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $areaCovered;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productRangeAndPrice;
    public $animalsPhotographed;
    
    public function __construct($idPetPhotographer = "null", $ecbrNo = "null", $idBusiness = "null", $businessInsurance = "null", $insuranceProvider = "null", $areaCovered = "null", $productRangeAndPrice = "null", 
    $animalsPhotographed = "null"){

		$this->idPetPhotographer = $idPetPhotographer;
		$this->ecbrNo = $ecbrNo;
		$this->idBusiness = $idBusiness;
		$this->businessINsurance = $businessINsurance;
		$this->insuranceProvider = $insuranceProvider;
		$this->areaCovered = $areaCovered;
		$this->productRangeAndPrice = $productRangeAndPrice;
		$this->animalsPhotographed =$animalsPhotographed;
    
    }
    
    public function getAnimalsPhotographed() {
        return $this->animalsPhotographed;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdPetPhotographer() {
        return $this->idPetPhotographer;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idPetPhotographer ARGDESCRIPTION
     */
    public function setIdPetPhotographer($idPetPhotographer) {
        $this->idPetPhotographer = $idPetPhotographer;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getEcbrNo() {
        return $this->ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $ecbrNo ARGDESCRIPTION
     */
    public function setEcbrNo($ecbrNo) {
        $this->ecbrNo = $ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessId ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBusinessINsurance() {
        return $this->businessINsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessINsurance ARGDESCRIPTION
     */
    public function setBusinessINsurance($businessINsurance) {
        $this->businessINsurance = $businessINsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getInsuranceProvider() {
        return $this->insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $insuranceProvider ARGDESCRIPTION
     */
    public function setInsuranceProvider($insuranceProvider) {
        $this->insuranceProvider = $insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAreaCovered() {
        return $this->areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $areaCovered ARGDESCRIPTION
     */
    public function setAreaCovered($areaCovered) {
        $this->areaCovered = $areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductRangeAndPrice() {
        return $this->productRangeAndPrice;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productRangeAndPrice ARGDESCRIPTION
     */
    public function setProductRangeAndPrice($productRangeAndPrice) {
        $this->productRangeAndPrice = $productRangeAndPrice;
    }

    /* OTHER FUNCTIONS */
	
	//STATIC FUNCTIONS
    public static function addPetPhotographer($db, $petPhotographer, $tableName = "petPhotographerFields"){
    	
		$query = "INSERT INTO " . $tableName . " VALUES (
		" . $petPhotographer->getIdPetPhotographer() . ", 
		'" . $petPhotographer->getEcbrNo() . "',
		'" . $petPhotographer->getIdBusiness() . "',
		'" . $petPhotographer->getBusinessINsurance() . "',
		'" . $petPhotographer->getInsuranceProvider() . "',
		'" . $petPhotographer->getAreaCovered() . "',
		'" . $petPhotographer->getProductRangeAndPrice() . "', 
		'" . $petPhotographer->getAnimalsPhotographed() . "'
		)";
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
    
}

