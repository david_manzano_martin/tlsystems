
<?php
class Business {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $companyName;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $contact;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $VAT;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $company;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $coNumber;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $phone;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $email;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $webPage;
    public $status;
    public $facebookPage;
    
    /*


			query
			SELECT Orders.OrderID, Customers.CustomerName, Shippers.ShipperName
FROM (Orders
INNER JOIN Customers ON Customers.CustomerID = Orders.CustomerID)
INNER JOIN Shippers ON Shippers.ShipperID = Orders.ShipperID;

    
    */
    
    public function __construct($idBusiness, $companyName, $contact, $VAT, $company, $coNumber, $phone, $email, $webPage, $status = "", $facebookPage = ""){

		$this->idBusiness = $idBusiness;
		$this->companyName = $companyName;
		$this->contact = $contact;
		$this->VAT = $VAT;
		$this->company = $company;
		$this->coNumber = $coNumber;
		$this->phone = $phone;
		$this->email = $email;
		$this->webPage = $webPage;
		$this->status = $status;
    	$this->facebookPage = $facebookPage;
    }
    
    public function getFacebookPage() {
        return $this->facebookPage;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getcompanyName() {
        return $this->companyName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $companyName ARGDESCRIPTION
     */
    public function setcompanyName($companyName) {
        $this->companyName = $companyName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getContact() {
        return $this->contact;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $contact ARGDESCRIPTION
     */
    public function setContact($contact) {
        $this->contact = $contact;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getVAT() {
        return $this->VAT;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $VAT ARGDESCRIPTION
     */
    public function setVAT($VAT) {
        $this->VAT = $VAT;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getCompany() {
        return $this->company;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $company ARGDESCRIPTION
     */
    public function setCompany($company) {
        $this->company = $company;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getCoNumber() {
        return $this->coNumber;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $coNumber ARGDESCRIPTION
     */
    public function setCoNumber($coNumber) {
        $this->coNumber = $coNumber;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $phone ARGDESCRIPTION
     */
    public function setPhone($phone) {
        $this->phone = $phone;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $email ARGDESCRIPTION
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getWebPage() {
        return $this->webPage;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $webPage ARGDESCRIPTION
     */
    public function setWebPage($webPage) {
        $this->webPage = $webPage;
    }
    
    public function getStatus() {
        return $this->status;
    }
    
    public function setStatus($status) {
        $this->status = $status;
    }

    /* OTHER FUNCTIONS */
    
    public function getRoute(){

		global $db;
		
		$query = "SELECT * FROM userBusinessRelations INNER JOIN users ON userBusinessRelations.userId = users.idUser WHERE businessId = " . $this->getIdBusiness();
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()){

			$localUser = new User($row["idUser"], $row["idBusiness"], $row["username"], $row["password"], $row["permissionLevel"]);
		
		}
		
		$arrayNames = glob("imgs/uploads/users/" . $localUser->getUsername() . "/" . $this->getCompanyName() . "/logo.*");
		
		if(count($arrayNames)>0){

			$imgRoute = $arrayNames[0];
		
		} else{

			$imgRoute = "imgs/logoImage.png";
		
		}

			/*if(file_exists("imgs/uploads/users/" . $localUser->getUsername() . "/" . $this->getCompanyName() . "/logo.jpg")) {

				$imgRoute = "imgs/uploads/users/" . $localUser->getUsername() . "/" . $this->getCompanyName() . "/logo.jpg";
			
			} else{

				$imgRoute = "imgs/logoImage.png";
			
			}*/
			
			
		//$imgRoute = "imgs/uploads/users/" . $localUser->getUsername() . "/" . $this->getCompanyName() . "/logo.jpg";

			//$imgRoute = "imgs/logoImage.png";
		
		
		
		return $imgRoute;
    
    }
    
    /* STATIC FUNCTIONS */
    
    public static function getAllBusinessByAttr($db, $attrName, $attrVal, $tableName = "business"){
		$array_results = array();
		
		$query = "SELECT * FROM " . $tableName . " WHERE " . $attrName . " LIKE '" . $attrVal . "'";
		
		$result = $db->query($query);
		while($row = $result->fetch_assoc()){

			$localBusiness = new Business($row["idBusiness"], $row["companyName"], $row["contact"], $row["VAT"], $row["company"], $row["coNumber"], $row["phone"], $row["email"], $row["webPage"], $row["status"]);
			array_push($array_results, $localBusiness);
		
		}
		
		return $array_results;
    
    }
    
    public static function getAllBusiness($db, $tableName = "business"){

		$array_results = array();
		
		$query = "SELECT * FROM " . $tableName;
		$result = $db->query($query);
		while($row = $result->fetch_assoc()){

			$localBusiness = new Business($row["idBusiness"], $row["companyName"], $row["contact"], $row["VAT"], $row["company"], $row["coNumber"], $row["phone"], $row["email"], $row["webPage"], $row["status"]);
			array_push($array_results, $localBusiness);
		
		}
		
		return $array_results;
    
    }

	public static function countBusinessByAttr($db, $attrName, $attrVal){

		$query = "SELECT * FROM business WHERE " . $attrName . " = '" . $attrVal . "'";
		
		$result = $db->query($query);
		
		return $result->num_rows;
	
	}
	
	public static function searchBusinessByAttr($db, $attrName, $attrVal){

		$localBusiness = 1;

		$query = "SELECT * FROM business WHERE " . $attrName . " = '" . $attrVal . "'";
		
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()){

			$localBusiness = new Business($row["idBusiness"], $row["companyName"], $row["contact"],$row["VAT"],$row["company"],$row["coNumber"],$row["phone"],$row["email"],$row["webPage"], $row["status"]);
			
		}
		
		return $localBusiness;
	
	}
	
	public static function getArrayBusinessByStatus($db, $status, $tableName = "business"){

		$array_results = array();
		
		$query = "SELECT * FROM " . $tableName . " WHERE status = '" . $status . "'";
		$result = $db->query($query);
		while($row = $result->fetch_assoc()){

			$localBusiness = new Business($row["idBusiness"], $row["companyName"], $row["contact"], $row["VAT"], $row["company"], $row["coNumber"], $row["phone"], $row["email"], $row["webPage"], $row["status"]);
			array_push($array_results, $localBusiness);
		
		}
		
		return $array_results;
	
	}
	
	public static function updateUserBusinesByAttr($db, $business, $attrName, $attrVal, $tableName = "business"){

		$query = "UPDATE business SET 
		idBusiness = " . $business->getIdBusiness() . ", 
		companyName = '" . $business->getCompanyName() . "', 
		contact = '" . $business->getContact() . "', 
		VAT = '" . $business->getVAT() . "', 
		company = '" . $business->getCompany() . "', 
		coNumber = '" . $business->getCoNumber() . "', 
		phone = '" . $business->getPhone() . "', 
		email = '" . $business->getEmail() . "', 
		webPage = '" . $business->getWebPage() . "' WHERE " . $attrName . " = '" . $attrVal . "'";
		
		if(!$db->query($query)){

			return 1;
		
		}
		
	}
	
	public static function updateFullBusinesByAttr($db, $business, $attrName, $attrVal, $tableName = "business"){

		$query = "UPDATE business SET 
		idBusiness = " . $business->getIdBusiness() . ", 
		companyName = '" . $business->getCompanyName() . "', 
		contact = '" . $business->getContact() . "', 
		VAT = '" . $business->getVAT() . "', 
		company = '" . $business->getCompany() . "', 
		coNumber = '" . $business->getCoNumber() . "', 
		phone = '" . $business->getPhone() . "', 
		email = '" . $business->getEmail() . "', 
		webPage = '" . $business->getWebPage() . "', 
		status = '" . $business->getStatus() . "' WHERE " . $attrName . " = '" . $attrVal . "'";
		
		if(!$db->query($query)){

			return 1;
		
		}
		
	}
	
	/*public static function updateFullBusinesByAttr($db, $business, $attrName, $attrVal, $tableName = "business"){

		$query = "UPDATE business SET 
		idBusiness = " . $business->getIdBusiness() . ", 
		companyName = '" . $business->getCompanyName() . "', 
		contact = '" . $business->getContact() . "', 
		VAT = '" . $business->getVAT() . "', 
		company = '" . $business->getCompany() . "', 
		coNumber = '" . $business->getCoNumber() . "', 
		phone = '" . $business->getPhone() . "', 
		email = '" . $business->getEmail() . "', 
		webPage = '" . $business->getWebPage() . "', 
		status = '" . $business->getStatus() . "' WHERE " . $attrName . " = '" . $attrVal . "'";
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}*/
    
	public static function createBusiness($db, $business){

		$query = "INSERT INTO business (idBusiness, companyName, contact, VAT, company, coNumber, phone, email, webpage, status, facebookPage) VALUES (
		'" . $business->getIdBusiness() . "', 
		'" . $business->getCompanyName() . "', 
		'" . $business->getContact() . "', 
		'" . $business->getVAT() . "', 
		'" . $business->getCompany() . "', 
		'" . $business->getCoNumber() . "', 
		'" . $business->getPhone() . "', 
		'" . $business->getEmail() . "', 
		'" . $business->getWebPage() . "', 
		'pending',
		'" . $business->getFacebookPage() . "'
		)";
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
    
}

?>