
<?php
class DogFoodProvider {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idDogFoodProvider;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $ecbNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $typeFood;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $areaCovered;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $homeDelivery;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $shipping;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $personalConsultation;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productRangeAndPrice;
    
    public function __construct($idDogFoodProvider = "null", $ecbNo = "null", $idBusiness = "null", $typeFood = "null", $areaCovered = "null", $homeDelivery = "null", $shipping = "null", 
     $personalConsultation= "null", $productRangeAndPrice = "null"){

		$this->idDogFoodProvider = $idDogFoodProvider;
		$this->ecbNo = $ecbNo;
		$this->idBusiness = $idBusiness;
		$this->typeFood = $typeFood;
		$this->areaCovered = $areaCovered;
		$this->homeDelivery = $homeDelivery;
		$this->shipping = $shipping;
		$this->personalConsultation = $personalConsultation;
		$this->productRangeAndPrice = $productRangeAndPrice;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdDogFoodProvider() {
        return $this->idDogFoodProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idDogFoodProvider ARGDESCRIPTION
     */
    public function setIdDogFoodProvider($idDogFoodProvider) {
        $this->idDogFoodProvider = $idDogFoodProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getEcbNo() {
        return $this->ecbNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $ecbNo ARGDESCRIPTION
     */
    public function setEcbNo($ecbNo) {
        $this->ecbNo = $ecbNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getTypeFood() {
        return $this->typeFood;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $typeFood ARGDESCRIPTION
     */
    public function setTypeFood($typeFood) {
        $this->typeFood = $typeFood;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAreaCovered() {
        return $this->areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $areaCovered ARGDESCRIPTION
     */
    public function setAreaCovered($areaCovered) {
        $this->areaCovered = $areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getHomeDelivery() {
        return $this->homeDelivery;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $homeDelivery ARGDESCRIPTION
     */
    public function setHomeDelivery($homeDelivery) {
        $this->homeDelivery = $homeDelivery;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getShipping() {
        return $this->shipping;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $shipping ARGDESCRIPTION
     */
    public function setShipping($shipping) {
        $this->shipping = $shipping;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getPersonalConsultation() {
        return $this->personalConsultation;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $personalConsultation ARGDESCRIPTION
     */
    public function setPersonalConsultation($personalConsultation) {
        $this->personalConsultation = $personalConsultation;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductRangeAndPrice() {
        return $this->productRangeAndPrice;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productRangeAndPrice ARGDESCRIPTION
     */
    public function setProductRangeAndPrice($productRangeAndPrice) {
        $this->productRangeAndPrice = $productRangeAndPrice;
    }

    /* OTHER FUNCTIONS */
    
    /* STATIC FUNCTIONS */
    public static function addDogFoodProvider($db, $dogFoodProvider, $tableName = "dogFoodProviderFields"){
		
		$query = "INSERT INTO " . $tableName . " (idDogFoodProvider, ecbrNo, idBusiness, typeFood, areaCovered, homeDelivery, shipping, personalConsultation, productRangeAndPrice) VALUES (
		" . $dogFoodProvider->getIdDogFoodProvider() . ", 
		'" . $dogFoodProvider->getEcbNo() . "', 
		'" . $dogFoodProvider->getIdBusiness() . "',
		'" . $dogFoodProvider->getTypeFood() . "',
		'" . $dogFoodProvider->getAreaCovered() . "',
		'" . $dogFoodProvider->getHomeDelivery() . "',
		'" . $dogFoodProvider->getShipping() . "',
		'" . $dogFoodProvider->getPersonalConsultation() . "',
		'" . $dogFoodProvider->getProductRangeAndPrice() . "'
		)";
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
    
}

?>