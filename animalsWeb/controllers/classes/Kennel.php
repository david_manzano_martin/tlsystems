
<?php
class Kennel {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idKennel;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $ecbrNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $businessInsurance;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $insuranceProvider;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $canineFirstAidQualification;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $noStaff;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $noKennels;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $openingHours;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $exerciseMethod;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $noYears;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $pickUpDropService;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productRangeAndPrice;
    
    public function __construct($idKennel = "null", $ecbrNo = "null", $idBusiness = "null", $businessInsurance = "null", $canineFirstAidQualification = "null", $noStaff = "null", $noKennels = "null", 
    $openingHours = "null", $exerciseMethod = "null", $noYears = "null", $areaCovered = "null", $pickUpDropService = "null", $productRangeAndPrice = "null"){

		$this->idKennel = $idKennel;
		$this->ecbrNo = $ecbrNo;
		$this->idBusiness = $idBusiness;
		$this->businessInsurance = $businessInsurance;
		$this->insuranceProvider = $insuranceProvider;
		$this->canineFirstAidQualification = $canineFirstAidQualification;
		$this->noStaff = $noStaff;
		$this->noKennels = $noKennels;
		$this->openingHours = $openingHours;
		$this->exerciseMethod = $exerciseMethod;
		$this->noYears = $noYears;
		$this->areaCovered = $areaCovered;
		$this->pickUpDropService = $pickUpDropService;
		$this->productRangeAndPrice = $productRangeAndPrice;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdKennel() {
        return $this->idKennel;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idKennel ARGDESCRIPTION
     */
    public function setIdKennel($idKennel) {
        $this->idKennel = $idKennel;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getEcbrNo() {
        return $this->ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $ecbrNo ARGDESCRIPTION
     */
    public function setEcbrNo($ecbrNo) {
        $this->ecbrNo = $ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBusinessInsurance() {
        return $this->businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessInsurance ARGDESCRIPTION
     */
    public function setBusinessInsurance($businessInsurance) {
        $this->businessInsurance = $businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getInsuranceProvider() {
        return $this->insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $insuranceProvider ARGDESCRIPTION
     */
    public function setInsuranceProvider($insuranceProvider) {
        $this->insuranceProvider = $insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getCanineFirstAidQualification() {
        return $this->canineFirstAidQualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $canineFirstAidQualification ARGDESCRIPTION
     */
    public function setCanineFirstAidQualification($canineFirstAidQualification) {
        $this->canineFirstAidQualification = $canineFirstAidQualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getNoStaff() {
        return $this->noStaff;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $noStaff ARGDESCRIPTION
     */
    public function setNoStaff($noStaff) {
        $this->noStaff = $noStaff;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getNoKennels() {
        return $this->noKennels;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $noKennels ARGDESCRIPTION
     */
    public function setNoKennels($noKennels) {
        $this->noKennels = $noKennels;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getOpeningHours() {
        return $this->openingHours;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $openingHours ARGDESCRIPTION
     */
    public function setOpeningHours($openingHours) {
        $this->openingHours = $openingHours;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getExerciseMethod() {
        return $this->exerciseMethod;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $exerciseMethod ARGDESCRIPTION
     */
    public function setExerciseMethod($exerciseMethod) {
        $this->exerciseMethod = $exerciseMethod;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getNoYears() {
        return $this->noYears;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $noYears ARGDESCRIPTION
     */
    public function setNoYears($noYears) {
        $this->noYears = $noYears;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getPickUpDropService() {
        return $this->pickUpDropService;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $pickUpDropService ARGDESCRIPTION
     */
    public function setPickUpDropService($pickUpDropService) {
        $this->pickUpDropService = $pickUpDropService;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductRangeAndPrice() {
        return $this->productRangeAndPrice;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productRangeAndPrice ARGDESCRIPTION
     */
    public function setProductRangeAndPrice($productRangeAndPrice) {
        $this->productRangeAndPrice = $productRangeAndPrice;
    }
    
    public function getAreaCovered() {
        return $this->areaCovered;
    }
    
    public function setAreaCovered($areaCovered) {
        $this->areaCovered = $areaCovered;
    }

	/* OTHER FUNCTIONS */
    
    //STATIC FUNCTIONS
    public static function addKennel($db, $kennel, $tableName = "kennelFields"){
    	
		$query = "INSERT INTO " . $tableName . " (idKennel, ecbrNo, idBusiness, businessInsurance, insuranceProvider, canineFirstAidQualification, noStaff, noKennels, openingHours, exerciseMethod, 
		noYears, areaCovered, pickUpDropService, productRangeAndPrice) VALUES (
		" . $kennel->getIdKennel() . ", 
		'" . $kennel->getEcbrNo() . "',
		'" . $kennel->getIdBusiness() . "',
		'" . $kennel->getBusinessInsurance() . "',
		'" . $kennel->getInsuranceProvider() . "',
		'" . $kennel->getCanineFirstAidQualification() . "',
		'" . $kennel->getNoStaff() . "',
		'" . $kennel->getNoKennels() . "',
		'" . $kennel->getOpeningHours() . "',
		'" . $kennel->getExerciseMethod() . "',
		'" . $kennel->getNoYears() . "',
		'" . $kennel->getAreaCovered() . "',
		'" . $kennel->getPickUpDropService() . "',
		'" . $kennel->getProductRangeAndPrice() . "'
		)";
		
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}    
    
}

?>