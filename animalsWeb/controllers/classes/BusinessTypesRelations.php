<?php
class BusinessTypesRelations {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusinessTypesRelations;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusinessType;
    
    public function __construct($idBusinessTypesRelations = "null", $idBusiness = "null", $idBusinessType = "null"){

		$this->idBusinessTypesRelations = $idBusinessTypesRelations;
		$this->idBusiness = $idBusiness;
		$this->idBusinessType = $idBusinessType;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusinessTypesRelations() {
        return $this->idBusinessTypesRelations;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusinessTypesRelations ARGDESCRIPTION
     */
    public function setIdBusinessTypesRelations($idBusinessTypesRelations) {
        $this->idBusinessTypesRelations = $idBusinessTypesRelations;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusinessType() {
        return $this->idBusinessType;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusinessType ARGDESCRIPTION
     */
    public function setIdBusinessType($idBusinessType) {
        $this->idBusinessType = $idBusinessType;
    }

	/* OTHER FUNCTIONS */
	
	//STATIC FUNCTIONS
	
	public static function getBusinessTypeByBusinessName($db, $name, $tableName = "businessTypesRelations"){

		//require(__DIR__ . "BusinessType.php");
		$query = "SELECT businessTypes.businessName as 'businessTypeName' FROM (businessTypes INNER JOIN businessTypesRelations on businessTypes.businessId = businessTypesRelations.idBusinessType) INNER JOIN business ON business.idBusiness = businessTypesRelations.idBusiness WHERE business.companyName = '" . $name . "'";
		$result = $db->query($query);
		
		while($row =$result->fetch_assoc()){

			return $row["businessTypeName"];
		
		}
		
		return false;
	
	}

	public static function createRelation($db, $businessId, $businessTypeId, $tableName = "businessTypesRelations"){
		
		$query = "INSERT INTO " . $tableName . " (idBusinessTypesRelations, idBusiness, idBusinessType) VALUES (null, " . $businessId . ", " . $businessTypeId . ")" ;
		if(!$db->query($query)){

			return 1;
		
		}

	}
	
}

?>