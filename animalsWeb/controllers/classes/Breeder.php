
<?php
class Breeder {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBreeder;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $typeId;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $cbrRegNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $kcBreedingName;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $vetName;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $vetReference;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $numberDogs;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $numberDams;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $aboutYou;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $cbrAccreditedBreeder;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $breeds;
    
    public function __construct($idBreeder = "null", $typeId = "null", $idBusiness = "null", $cbrRegNo = "null", $kcBreedingName = "null", $vetName = "null", $vetReference = "null", $numberDogs = "null"
    , $numberDams = "null", $aboutYou = "null", $cbrAccreditedBreeder = "null", $breeds = "null"){

		$this->idBreeder = $idBreeder;
		$this->typeId = $typeId;
		$this->idBusiness = $idBusiness;
		$this->cbrRegNo = $cbrRegNo;
		$this->kcBreedingName = $kcBreedingName;
		$this->vetName = $vetName;
		$this->vetReference = $vetReference;
		$this->numberDogs = $numberDogs;
		$this->numberDams = $numberDams;
		$this->aboutYou = $aboutYou;
		$this->cbrAccreditedBreeder = $cbrAccreditedBreeder;
		$this->breeds = $breeds;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBreeder() {
        return $this->idBreeder;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBreeder ARGDESCRIPTION
     */
    public function setIdBreeder($idBreeder) {
        $this->idBreeder = $idBreeder;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getTypeId() {
        return $this->typeId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $typeId ARGDESCRIPTION
     */
    public function setTypeId($typeId) {
        $this->typeId = $typeId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getCbrRegNo() {
        return $this->cbrRegNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $cbrRegNo ARGDESCRIPTION
     */
    public function setCbrRegNo($cbrRegNo) {
        $this->cbrRegNo = $cbrRegNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getKcBreedingName() {
        return $this->kcBreedingName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $kcBreedingName ARGDESCRIPTION
     */
    public function setKcBreedingName($kcBreedingName) {
        $this->kcBreedingName = $kcBreedingName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getVetName() {
        return $this->vetName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $vetName ARGDESCRIPTION
     */
    public function setVetName($vetName) {
        $this->vetName = $vetName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getVetReference() {
        return $this->vetReference;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $vetReference ARGDESCRIPTION
     */
    public function setVetReference($vetReference) {
        $this->vetReference = $vetReference;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getNumberDogs() {
        return $this->numberDogs;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $numberDogs ARGDESCRIPTION
     */
    public function setNumberDogs($numberDogs) {
        $this->numberDogs = $numberDogs;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getNumberDams() {
        return $this->numberDams;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $numberDams ARGDESCRIPTION
     */
    public function setNumberDams($numberDams) {
        $this->numberDams = $numberDams;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAboutYou() {
        return $this->aboutYou;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $aboutYou ARGDESCRIPTION
     */
    public function setAboutYou($aboutYou) {
        $this->aboutYou = $aboutYou;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getCbrAccreditedBreeder() {
        return $this->cbrAccreditedBreeder;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $cbrAccreditedBreeder ARGDESCRIPTION
     */
    public function setCbrAccreditedBreeder($cbrAccreditedBreeder) {
        $this->cbrAccreditedBreeder = $cbrAccreditedBreeder;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBreeds() {
        return $this->breeds;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $breeds ARGDESCRIPTION
     */
    public function setBreeds($breeds) {
        $this->breeds = $breeds;
    }

	/* OTHER FUNCTIONS */
	
	//STATIC FUNCTIONS
	
	public static function addBreeder($db, $breeder, $tableName = "breederFields"){
		
		$query = "INSERT INTO " . $tableName . " (idBreeder, typeId, idBusiness, cbrRegNo, kcBreedingName, vetName, vetReference, numberDogs, numberDams, aboutYou, cbrAccreditedBreeder, breeds) VALUES (
'" . $breeder->getIdBreeder() . "', 
(SELECT businessId FROM businessTypes WHERE businessName = 'Breeder'), 
'" . $breeder->getIdBusiness() . "', 
'" . $breeder->getCbrRegNo() . "', 
'" . $breeder->getKcBreedingName() . "', 
'" . $breeder->getVetName() . "', 
'" . $breeder->getVetReference() . "', 
'" . $breeder->getNumberDogs() . "', 
'" . $breeder->getNumberDams() . "', 
'" . $breeder->getAboutYou() . "', 
'" . $breeder->getCbrAccreditedBreeder() . "', 
'" . $breeder->getBreeds() . "'		
		)";
		
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
    
    
}

?>