<?php
class HomeBoarder {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idHomeBoarder;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $ecbrNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $localAutorithyLicense;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $businessInsurance;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $insuranceProvider;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $canineFirstAidQualification;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $noStaff;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $noDogLicenseBoard;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $preferedDogs;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $acceptNeuteredMales;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $acceptNeuteredFemales;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $yearsExperience;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $methodOfExercise;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $otherFacilities;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $areaCovered;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $pickUpDropService;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $openingHours;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productRangePrice;
    
    public function __construct($idHomeBoarder = "null", $ecbrNo = "null", $idBusiness = "null", $localAutorithyLicense = "null", $businessInsurance = "null", $insuranceProvider = "null", 
    $canineFirstAidQualification = "null", $noStaff = "null", $noDogLicenseBoard = "null", $preferedDogs = "null", $acceptNeuteredMales = "null", $acceptNeuteredFemales = "null", $yearsExperience = "null", 
    $methodOfExercise = "null", $otherFacilities = "null", $areaCovered = "null", $pickUpDropService = "null", $openingHours = "null", $productRangePrice = "null"){

		$this->idHomeBoarder = $idHomeBoarder;
		$this->ecbrNo = $ecbrNo;
		$this->idBusiness = $idBusiness;
		$this->localAutorithyLicense = $localAutorithyLicense;
		$this->businessInsurance = $businessInsurance;
		$this->insuranceProvider = $insuranceProvider;
		$this->canineFirstAidQualification = $canineFirstAidQualification;
		$this->noStaff = $noStaff;
		$this->noDogLicenseBoard = $noDogLicenseBoard;
		$this->preferedDogs = $preferedDogs;
		$this->acceptNeuteredMales = $acceptNeuteredMales;
		$this->acceptNeuteredFemales = $acceptNeuteredFemales;
		$this->yearsExperience = $yearsExperience;
		$this->methodOfExercise = $methodOfExercise;
		$this->otherFacilities = $otherFacilities;
		$this->areaCovered = $areaCovered;
		$this->pickUpDropService = $pickUpDropService;
		$this->openingHours = $openingHours;
		$this->productRangePrice = $productRangePrice;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdHomeBoarder() {
        return $this->idHomeBoarder;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idHomeBoarder ARGDESCRIPTION
     */
    public function setIdHomeBoarder($idHomeBoarder) {
        $this->idHomeBoarder = $idHomeBoarder;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getEcbrNo() {
        return $this->ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $ecbrNo ARGDESCRIPTION
     */
    public function setEcbrNo($ecbrNo) {
        $this->ecbrNo = $ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getLocalAutorithyLicense() {
        return $this->localAutorithyLicense;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $localAutorithyLicense ARGDESCRIPTION
     */
    public function setLocalAutorithyLicense($localAutorithyLicense) {
        $this->localAutorithyLicense = $localAutorithyLicense;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBusinessInsurance() {
        return $this->businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessInsurance ARGDESCRIPTION
     */
    public function setBusinessInsurance($businessInsurance) {
        $this->businessInsurance = $businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getInsuranceProvider() {
        return $this->insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $insuranceProvider ARGDESCRIPTION
     */
    public function setInsuranceProvider($insuranceProvider) {
        $this->insuranceProvider = $insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getCanineFirstAidQualification() {
        return $this->canineFirstAidQualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $canineFirstAidQualification ARGDESCRIPTION
     */
    public function setCanineFirstAidQualification($canineFirstAidQualification) {
        $this->canineFirstAidQualification = $canineFirstAidQualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getNoStaff() {
        return $this->noStaff;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $noStaff ARGDESCRIPTION
     */
    public function setNoStaff($noStaff) {
        $this->noStaff = $noStaff;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getNoDogLicenseBoard() {
        return $this->noDogLicenseBoard;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $noDogLicenseBoard ARGDESCRIPTION
     */
    public function setNoDogLicenseBoard($noDogLicenseBoard) {
        $this->noDogLicenseBoard = $noDogLicenseBoard;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getPreferedDogs() {
        return $this->preferedDogs;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $preferedDogs ARGDESCRIPTION
     */
    public function setPreferedDogs($preferedDogs) {
        $this->preferedDogs = $preferedDogs;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAcceptNeuteredMales() {
        return $this->acceptNeuteredMales;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $acceptNeuteredMales ARGDESCRIPTION
     */
    public function setAcceptNeuteredMales($acceptNeuteredMales) {
        $this->acceptNeuteredMales = $acceptNeuteredMales;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAcceptNeuteredFemales() {
        return $this->acceptNeuteredFemales;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $acceptNeuteredFemales ARGDESCRIPTION
     */
    public function setAcceptNeuteredFemales($acceptNeuteredFemales) {
        $this->acceptNeuteredFemales = $acceptNeuteredFemales;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getYearsExperience() {
        return $this->yearsExperience;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $yearsExperience ARGDESCRIPTION
     */
    public function setYearsExperience($yearsExperience) {
        $this->yearsExperience = $yearsExperience;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getMethodOfExercise() {
        return $this->methodOfExercise;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $methodOfExercise ARGDESCRIPTION
     */
    public function setMethodOfExercise($methodOfExercise) {
        $this->methodOfExercise = $methodOfExercise;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getOtherFacilities() {
        return $this->otherFacilities;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $otherFacilities ARGDESCRIPTION
     */
    public function setOtherFacilities($otherFacilities) {
        $this->otherFacilities = $otherFacilities;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAreaCovered() {
        return $this->areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $areaCovered ARGDESCRIPTION
     */
    public function setAreaCovered($areaCovered) {
        $this->areaCovered = $areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getPickUpDropService() {
        return $this->pickUpDropService;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $pickUpDropService ARGDESCRIPTION
     */
    public function setPickUpDropService($pickUpDropService) {
        $this->pickUpDropService = $pickUpDropService;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getOpeningHours() {
        return $this->openingHours;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $openingHours ARGDESCRIPTION
     */
    public function setOpeningHours($openingHours) {
        $this->openingHours = $openingHours;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductRangePrice() {
        return $this->productRangePrice;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productRangePrice ARGDESCRIPTION
     */
    public function setProductRangePrice($productRangePrice) {
        $this->productRangePrice = $productRangePrice;
    }

    /* OTHER FUNCTIONS */
    
    //STATIC FUNCTIONS
    public static function addHomeBoarder($db, $homeBoarder, $tableName = "homeBoarderFields"){

    	
		$query = "INSERT INTO " . $tableName . " VALUES (
		" . $homeBoarder->getIdHomeBoarder() . ", 
		'" . $homeBoarder->getEcbrNo() . "',
		'" . $homeBoarder->getIdBusiness() . "',
		'" . $homeBoarder->getLocalAutorithyLicense() . "',
		'" . $homeBoarder->getBusinessInsurance() . "',
		'" . $homeBoarder->getInsuranceProvider() . "',
		'" . $homeBoarder->getCanineFirstAidQualification() . "',
		'" . $homeBoarder->getNoStaff() . "',
		'" . $homeBoarder->getNoDogLicenseBoard() . "',
		'" . $homeBoarder->getPreferedDogs() . "',
		'" . $homeBoarder->getAcceptNeuteredMales() . "',
		'" . $homeBoarder->getAcceptNeuteredFemales() . "',
		'" . $homeBoarder->getYearsExperience() . "',
		'" . $homeBoarder->getMethodOfExercise() . "',
		'" . $homeBoarder->getOtherFacilities() . "',
		'" . $homeBoarder->getAreaCovered() . "',
		'" . $homeBoarder->getPickUpDropService() . "',
		'" . $homeBoarder->getOpeningHours() . "',
		'" . $homeBoarder->getProductRangePrice() . "'
		)";
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
    
}

?>