<?php

class Breed {

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBreed;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $breedName;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $firstDescription;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $secondDescription;
    
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $firstImage;
    
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $secondImage;
    
    
    
    public function __construct($idBreed = "default", $breedName = "test", $firstDescription = "first", $secondDescription = "second"){
    	$dir = __dir__ . "/../../imgs/uploads/breeds/";

		if($idBreed == null){
			$this->idBreed = "null";
		} else{
			$this->idBreed = $idBreed;
		}
		
		if($breedName == null){
			$breedName = "test";
		} else{
			$this->breedName = $breedName;
		}
		
		if($firstDescription == null){
			$firstDescription = "first";
		} else{
			$this->firstDescription = $firstDescription;
		}
		
		if($secondDescription == null){
			$secondDescription = "second";
		} else{
			$this->secondDescription = $secondDescription;
		}
		
		if(file_exists($dir . "/" . $this->breedName . "1.jpg")){

			$this->firstImage = "imgs/uploads/breeds/" . $this->breedName . "1.jpg";
		
		} else{

			$this->firstImage = "imgs/uploads/breeds/common1.jpg";
		
		}
		
		if(file_exists($dir . "/" . $this->breedName . "2.jpg")){

			$this->secondImage = "imgs/uploads/breeds/" . $this->breedName . "2.jpg";
		
		} else{

			$this->secondImage = "imgs/uploads/breeds/common2.jpg";
		
		}
		
		$this->formatDb();
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBreed() {
        return $this->idBreed;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBreed ARGDESCRIPTION
     */
    public function setIdBreed($idBreed) {
        $this->idBreed = $idBreed;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBreedName() {
        return $this->breedName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $breedName ARGDESCRIPTION
     */
    public function setBreedName($breedName) {
        $this->breedName = $breedName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getFirstDescription() {
        return $this->firstDescription;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $firstDescription ARGDESCRIPTION
     */
    public function setFirstDescription($firstDescription) {
        $this->firstDescription = $firstDescription;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getSecondDescription() {
        return $this->secondDescription;
    }
    
    public function getFirstImage() {
        return $this->firstImage;
    }
    
    public function setFirstImage($firstImage){
    	$this->firstImage = $firstImage;
    }
    
    public function getSecondImage() {
        return $this->secondImage;
    }
    
    public function setSecondImage($secondImage){
    	$this->secondImage = $secondImage;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $secondDescription ARGDESCRIPTION
     */
    public function setSecondDescription($secondDescription) {
        $this->secondDescription = $secondDescription;
    }

		/* MY FUNCTIONS */
		
		public static function getAllBreeds($db, $tableName = "breeds"){

			$arrayResults = array();
			$query = "SELECT * FROM " . $tableName;
			$result = $db->query($query);
			
			while($row = $result->fetch_assoc()){

				$localBreed = new Breed($row["idBreed"], $row["breedName"], $row["firstDescription"], $row["secondDescription"]);
				array_push($arrayResults, $localBreed);
			
			}
			
			return $arrayResults;
		
		}
		
		public static function getBreedByAttr($db, $attrName, $attrVal, $tableName = "breeds"){

			//$query = "SELECT * FROM " . $tableName . " WHERE breedName = '" . $name . "'";
			$query = "SELECT * FROM " . $tableName . " WHERE " . $attrName . " = '" . $attrVal . "'";
			$result = $db->query($query);
			
			while($row = $result->fetch_assoc()){

				$localBreed = new Breed($row["idBreed"], $row["breedName"], $row["firstDescription"], $row["secondDescription"]);
				return $localBreed;
			
			}
			
			return false;
		
		}
		
		public function formatDb(){

			$this->firstDescription = str_replace("'", "\'", $this->firstDescription);
			$this->secondDescription = str_replace("'", "\'", $this->secondDescription);
		
		}
		
		public function formatWeb(){

			$this->firstDescription = str_replace("\'", "'", $this->firstDescription);
			$this->secondDescription = str_replace("\'", "'", $this->secondDescription);
		
		}
    
}

?>