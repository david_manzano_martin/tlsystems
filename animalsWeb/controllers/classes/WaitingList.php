
<?php
class WaitingList {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idWaitingList;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idUser;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBreed;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idUkRegion;
    
    public function __construct($idWaitingList = "null", $idUser = "null", $idBreed = "null", $idUkRegion = "null"){

		$this->idWaitingList = $idWaitingList;
		$this->idUser = $idUser;
		$this->idBreed = $idBreed;
		$this->idUkRegion = $idUkRegion;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdWaitingList() {
        return $this->idWaitingList;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idWaitingList ARGDESCRIPTION
     */
    public function setIdWaitingList($idWaitingList) {
        $this->idWaitingList = $idWaitingList;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdUser() {
        return $this->idUser;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idUser ARGDESCRIPTION
     */
    public function setIdUser($idUser) {
        $this->idUser = $idUser;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBreed() {
        return $this->idBreed;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBreed ARGDESCRIPTION
     */
    public function setIdBreed($idBreed) {
        $this->idBreed = $idBreed;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdUkRegion() {
        return $this->idUkRegion;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idUkRegion ARGDESCRIPTION
     */
    public function setIdUkRegion($idUkRegion) {
        $this->idUkRegion = $idUkRegion;
    }

	/* OTHER FUNCTIONS */
	
	public function getBreedName(){

		global $db;

		return Breed::getBreedByAttr($db, "idBreed", $this->getIdBreed())->getBreedName();
	
	}
	
	public function getRegionName(){

		global $db;
		
		return UkRegion::getUkRegionByAttr($db, "idUkRegion", $this->getIdUkRegion())->getRegionName();
	
	}
	
	/* STATIC FUNCTIONS */
	
	public static function checkIfExist($db, $waitingList, $tableName = "waitingList"){

		$result = 0;
		$query = "SELECT * FROM " . $tableName . " WHERE idUser ='" . $waitingList->getIdUser() . "' AND idBreed = '" . $waitingList->getIdBreed() . "' 
		AND idUkRegion = '" . $waitingList->getIdUkRegion() . "'";
		
		$result = $db->query($query);
		
		return $result->num_rows;
	
	}
	
	public static function removeWaitingListByNames($db, $user, $breedName, $regionName){

		$arrayIdsToRemove = array();
		$result = 0;
		
		$query = 'SELECT waitingList.idWaitingList from waitingList 
			inner join users on waitingList.idUser = users.idUser
			INNER join breeds on waitingList.idBreed = breeds.idBreed
			inner join ukRegions on waitingList.idUkRegion = ukRegions.idUkRegion 
			WHERE users.idUser = "' . $user->getIdUser() . '" and breeds.breedName = "' . $breedName . '" AND ukRegions.regionName = "' . $regionName . '"';
		
		$result = $db->query($query);	
		while($row = $result->fetch_assoc()){

			$localId = $row["idWaitingList"];
			array_push($arrayIdsToRemove, $localId);
		
		}

		foreach($arrayIdsToRemove as $id){

			$query = "DELETE FROM waitingList WHERE idWaitingList = $id";
			if(!$db->query($query)){
				$result = 1;
			}
		
		}
		
		return $result;

	}

	public static function getWaitingListByUser($db, $user, $tableName = "waitingList"){

		$arrayResults = array();

		$query = "SELECT * FROM " . $tableName . " WHERE idUser = " . $user->getIdUser();
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()){

			$localWaitingList = new WaitingList($row["idWaitingList"], $row["idUser"], $row["idBreed"], $row["idUkRegion"]);
			array_push($arrayResults, $localWaitingList);
		
		}
		
		return $arrayResults;		
	
	}
	
	public static function addWaitingList($db, $waitingList, $tableName = "waitingList"){

		$query = "INSERT INTO " . $tableName . " VALUES(
			'" . $waitingList->getIdWaitingList() . "', 
			'" . $waitingList->getIdUser() . "', 
			'" . $waitingList->getIdBreed() . "', 
			'" . $waitingList->getIdUkRegion() . "'		
		)";
		
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
    
}

?>