
<?php
class UkRegion {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idUkRegion;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $regionName;
    
    public function __construct($idUkRegion = "null", $regionName = "null"){

		$this->idUkRegion = $idUkRegion;
		$this->regionName = $regionName;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdUkRegion() {
        return $this->idUkRegion;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idUkRegion ARGDESCRIPTION
     */
    public function setIdUkRegion($idUkRegion) {
        $this->idUkRegion = $idUkRegion;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getRegionName() {
        return $this->regionName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $regionRegionName ARGDESCRIPTION
     */
    public function setRegionName($regionRegionName) {
        $this->regionRegionName = $regionRegionName;
    }
    
    /* OTHER FUNCTIONS */
    
    /* STATIC FUNCTIONS */
    
    public static function getUkRegionByAttr($db, $attrName, $attrVal, $tableName = "ukRegions"){

		$query = "SELECT * FROM " . $tableName . " WHERE " . $attrName . " = '" . $attrVal . "'";
		$result = $db->query($query);
		while($row = $result->fetch_assoc()){

			$localUkRegion = new UkRegion($row["idUkRegion"], $row["regionName"]);
			return $localUkRegion;
			
		}
		
		return 1;
    
    }

	public static function getAllUkRegions($db, $tableName = "ukRegions"){

		$arrayResults = array();
		$query = "SELECT * FROM " . $tableName;
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()){

			$localUkRegion = new UkRegion($row["idUkRegion"], $row["regionName"]);
			array_push($arrayResults, $localUkRegion);
			
		}
			
		return $arrayResults;
	
	}
    
}