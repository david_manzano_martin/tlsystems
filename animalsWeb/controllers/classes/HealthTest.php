
<?php
class HealthTest {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idHealthTest;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $healthTestName;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $healthTestDescription;
    
    public function __construct($idHealthTest = "null", $healthTestName = "null", $healthTestDescription = "null"){

		$this->idHealthTest = $idHealthTest;
		$this->healthTestName = $healthTestName;
		$this->healthTestDescription = $healthTestDescription;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdHealthTest() {
        return $this->idHealthTest;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idHealthTest ARGDESCRIPTION
     */
    public function setIdHealthTest($idHealthTest) {
        $this->idHealthTest = $idHealthTest;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getHealthTestName() {
        return $this->healthTestName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $healthTestName ARGDESCRIPTION
     */
    public function setHealthTestName($healthTestName) {
        $this->healthTestName = $healthTestName;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getHealthTestDescription() {
        return $this->healthTestDescription;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $healthTestDescription ARGDESCRIPTION
     */
    public function setHealthTestDescription($healthTestDescription) {
        $this->healthTestDescription = $healthTestDescription;
    }

	/* OTHER CLASSES */	
	public function updateHealthTest($db, $tableName = "healthTest"){

		$query = "UPDATE " . $tableName . " SET healthTestName = '" . $this->getHealthTestName() . "', healthTestDescription = '" . $this->getHealthTestDescription() . "' WHERE idHealthTest = " . $this->getIdHealthTest();
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
	
	public function addHealthTest($db, $tableName = "healthTest"){

		$query = "INSERT INTO " . $tableName . " VALUES(" . $this->getIdHealthTest() . ", '" . $this->getHealthTestName() . "', '" . $this->getHealthTestDescription() . "')";
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
	
	/* STATIC FUNCTIONS */

	public static function getHealthTestByAttr($db, $attrName, $attrVal, $tableName = "healthTest"){

		$query = "SELECT * FROM " . $tableName . " WHERE " . $attrName . " = '" . $attrVal . "'";
		
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()){
			$localHealthTest = new HealthTest($row["idHealthTest"], $row["healthTestName"], $row["healthTestDescription"]);
			return $localHealthTest;
		}
		
		return 1;
	
	}
	
	public static function getAllHealthTests($db, $tableName = "healthTest"){

		$arrayResults = array();
		
		$query = "SELECT * FROM " . $tableName;
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()){
			$localHealthTest = new HealthTest($row["idHealthTest"], $row["healthTestName"], $row["healthTestDescription"]);
			//echo($row["idHealthTest"]);
			array_push($arrayResults, $localHealthTest);
		}
		
		return $arrayResults;
	
	}
    
}
?>