<?php
class DogTrainer {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idDogTrainer;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $ECBRNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $validDbsCheck;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $businessInsurance;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $insuranceProvider;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $canineFirstAidQualification;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $qualifications;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $kgGcdsApproved;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $areaCovered;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productRange;
    
    public $businessId;
    
    public function __construct($idDogTrainer = "null", $ECBRNo= "null", $businessId = "null", $validDbsCheck= "null", $businessInsurance= "null", $insuranceProvider= "null", $canineFirstAidQualification= "null",
     $qualifications= "null", $kgGcdsApproved= "null", $areaCovered= "null", $productRange= "null"){
     	
		$this->idDogTrainer = $idDogTrainer;
		$this->ECBRNo = $ECBRNo;
		$this->businessId = $businessId;
		$this->validDbsCheck = $validDbsCheck;
		$this->businessInsurance = $businessInsurance;
		$this->insuranceProvider = $insuranceProvider;
		$this->canineFirstAidQualification = $canineFirstAidQualification;
		$this->qualifications = $qualifications;
		$this->kgGcdsApproved = $kgGcdsApproved;
		$this->areaCovered = $areaCovered;
		$this->productRange = $productRange;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdDogTrainer() {
        return $this->idDogTrainer;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idDogTrainer ARGDESCRIPTION
     */
    public function setIdDogTrainer($idDogTrainer) {
        $this->idDogTrainer = $idDogTrainer;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getECBRNo() {
        return $this->ECBRNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $ECBRNo ARGDESCRIPTION
     */
    public function setECBRNo($ECBRNo) {
        $this->ECBRNo = $ECBRNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getValidDbsCheck() {
        return $this->validDbsCheck;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $validDbsCheck ARGDESCRIPTION
     */
    public function setValidDbsCheck($validDbsCheck) {
        $this->validDbsCheck = $validDbsCheck;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBusinessInsurance() {
        return $this->businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessInsurance ARGDESCRIPTION
     */
    public function setBusinessInsurance($businessInsurance) {
        $this->businessInsurance = $businessInsurance;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getInsuranceProvider() {
        return $this->insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $insuranceProvider ARGDESCRIPTION
     */
    public function setInsuranceProvider($insuranceProvider) {
        $this->insuranceProvider = $insuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getCanineFirstAidQualification() {
        return $this->canineFirstAidQualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $canineFirstAidQualification ARGDESCRIPTION
     */
    public function setCanineFirstAidQualification($canineFirstAidQualification) {
        $this->canineFirstAidQualification = $canineFirstAidQualification;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getQualifications() {
        return $this->qualifications;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $qualifications ARGDESCRIPTION
     */
    public function setQualifications($qualifications) {
        $this->qualifications = $qualifications;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getKgGcdsApproved() {
        return $this->kgGcdsApproved;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $kgGcdsApproved ARGDESCRIPTION
     */
    public function setKgGcdsApproved($kgGcdsApproved) {
        $this->kgGcdsApproved = $kgGcdsApproved;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAreaCovered() {
        return $this->areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $areaCovered ARGDESCRIPTION
     */
    public function setAreaCovered($areaCovered) {
        $this->areaCovered = $areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductRange() {
        return $this->productRange;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productRange ARGDESCRIPTION
     */
    public function setProductRange($productRange) {
        $this->productRange = $productRange;
    }
    
    public function getBusinessId() {
        return $this->businessId;
    }
    
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }
    
    /* OTHER FUNCTIONS */
    
    /* STATIC FUNCTIONS */
  	 public static function addDogTrainer($db, $dogTrainer, $tableName = "dogTrainerFields"){
  	 	
  	 	$query = "INSERT INTO " . $tableName . " (idDogTrainer, ECBRNo, idBusiness, validDbsCheck, businessInsurance, insuranceProvider, canineFirstAidQualification, qualifications, kgGcdsApproved, areaCovered, productRangeAndPrice) VALUES(
		" . $dogTrainer->getIdDogTrainer() . ", 
		'" . $dogTrainer->getECBRNo() . "', 
		'" . $dogTrainer->getBusinessId() . "', 
		'" . $dogTrainer->getValidDbsCheck() . "', 
		'" . $dogTrainer->getBusinessInsurance() . "', 
		'" . $dogTrainer->getInsuranceProvider() . "', 
		'" . $dogTrainer->getCanineFirstAidQualification() . "', 
		'" . $dogTrainer->getQualifications() . "', 
		'" . $dogTrainer->getKgGcdsApproved() . "', 
		'" . $dogTrainer->getAreaCovered() . "', 
		'" . $dogTrainer->getProductRange() . "'
  	 	)";
  	 	
  	 	
  	 	
  	 	if(!$db->query($query)){

			return 1;
		
		}
	
	}
    
    
    
}

?>