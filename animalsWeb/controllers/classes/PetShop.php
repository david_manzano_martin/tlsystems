<?php
class PetShop {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idPetShop;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $areaCovered;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $aboutShop;
    
    public function __construct($idPetShop = "null", $areaCovered = "null", $aboutShop){

		$this->idPetShop = $idPetShop;
		$this->areaCovered = $areaCovered;
		$this->aboutShop = $aboutShop;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdPetShop() {
        return $this->idPetShop;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idPetShop ARGDESCRIPTION
     */
    public function setIdPetShop($idPetShop) {
        $this->idPetShop = $idPetShop;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAreaCovered() {
        return $this->areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $areaCovered ARGDESCRIPTION
     */
    public function setAreaCovered($areaCovered) {
        $this->areaCovered = $areaCovered;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getAboutShop() {
        return $this->aboutShop;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $aboutShop ARGDESCRIPTION
     */
    public function setAboutShop($aboutShop) {
        $this->aboutShop = $aboutShop;
    }

    /* OTHER FUNCTIONS */
    
    //STATIC FUNCTIONS
    public static function addPetShop($db, $petShop, $tableName = "petShopFields"){
    	
		$query = "INSERT INTO " . $tableName . " VALUES (
		" . $petShop->getIdPetShop() . ", 
		'" . $petShop->getAreaCovered() . "',
		'" . $petShop->getAboutShop() . "' 
		)";
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
}

