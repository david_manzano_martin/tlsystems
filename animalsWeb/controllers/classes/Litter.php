<?php
class Litter {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $litterId;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $breederId;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $dateOfBirth;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $numberOfBitches;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $numberOfDogs;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $sireId;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $damId;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $dataAvailable;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $area;
    
    public function __construct($litterId = "null", $breederId = "null", $dateOfBirth = "null", $numberOfBitches = "null", $numberOfDogs = "null", $sireId = "null", $damId = "null", $dataAvailable = "null", $area = "null"){

		$this->litterId = $litterId;
		$this->breederId = $breederId;
		$this->dateOfBirth = $dateOfBirth;
		$this->numberOfBitches = $numberOfBitches;
		$this->numberOfDogs = $numberOfDogs;
		$this->sireId = $sireId;
		$this->damId = $damId;
		$this->dataAvailable = $dataAvailable;
		$this->area = $area;    
		
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getLitterId() {
        return $this->litterId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $litterId ARGDESCRIPTION
     */
    public function setLitterId($litterId) {
        $this->litterId = $litterId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBreederId() {
        return $this->breederId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $breederId ARGDESCRIPTION
     */
    public function setBreederId($breederId) {
        $this->breederId = $breederId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getDateOfBirth() {
        return $this->dateOfBirth;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $dateOfBirth ARGDESCRIPTION
     */
    public function setDateOfBirth($dateOfBirth) {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getNumberOfBitches() {
        return $this->numberOfBitches;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $numberOfBitches ARGDESCRIPTION
     */
    public function setNumberOfBitches($numberOfBitches) {
        $this->numberOfBitches = $numberOfBitches;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getNumberOfDogs() {
        return $this->numberOfDogs;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $numberOfDogs ARGDESCRIPTION
     */
    public function setNumberOfDogs($numberOfDogs) {
        $this->numberOfDogs = $numberOfDogs;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getSireId() {
        return $this->sireId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $sireId ARGDESCRIPTION
     */
    public function setSireId($sireId) {
        $this->sireId = $sireId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getDamId() {
        return $this->damId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $damId ARGDESCRIPTION
     */
    public function setDamId($damId) {
        $this->damId = $damId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getDataAvailable() {
        return $this->dataAvailable;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $dataAvailable ARGDESCRIPTION
     */
    public function setDataAvailable($dataAvailable) {
        $this->dataAvailable = $dataAvailable;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getArea() {
        return $this->area;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $area ARGDESCRIPTION
     */
    public function setArea($area) {
        $this->area = $area;
    }
}

/* MY FUNCTIONS */



?>