<?php
class UserBusinessRelation {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $relationId;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $userId;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $businessId;

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getRelationId() {
        return $this->relationId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $relationId ARGDESCRIPTION
     */
    public function setRelationId($relationId) {
        $this->relationId = $relationId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $userId ARGDESCRIPTION
     */
    public function setUserId($userId) {
        $this->userId = $userId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getBusinessId() {
        return $this->businessId;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $businessId ARGDESCRIPTION
     */
    public function setBusinessId($businessId) {
        $this->businessId = $businessId;
    }

	/* OTHER FUNCTIONS */
	
	/* STATIC FUNCTIONS */
	
	public static function getBusinessByUserId($db, $userId, $tableName = "userBusinessRelations"){

		$array_results = array();
		$query = "SELECT * FROM (business
					INNER JOIN userBusinessRelations ON userBusinessRelations.businessId = business.idBusiness)
					INNER JOIN users ON users.idUser = userBusinessRelations.userId where users.idUser = " . $userId;
		$result = $db->query($query);
		
		while($row =$result->fetch_assoc()){

			$localBusiness = new Business($row["idBusiness"], $row["companyName"], $row["contact"], $row["VAT"], $row["company"], $row["coNumber"], $row["phone"], $row["email"], $row["webPage"], $row["status"]);
			array_push($array_results, $localBusiness);
		
		}
		
		return $array_results;
	
	}
	
	public static function createRelation($db, $userId, $businessId, $tableName = "userBusinessRelations"){
		
		$query = "INSERT INTO " . $tableName . " (relationId, userId, businessId) VALUES (null, " . $userId . ", " . $businessId . ")" ;
		if(!$db->query($query)){

			return 1;
		
		}

	}
    
}

?>