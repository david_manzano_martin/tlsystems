
<?php
class DogMedicalInsuranceProvider {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idDogMedicalInsuranceProvider;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $ecbrNo;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $profile;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $idBusiness;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $specialistArea;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $productRangeAndPrice;
    
    public function __construct($idDogMedicalInsuranceProvider = "null", $ecbrNo = "null", $idBusiness = "null", $profile = "null", $specialistArea = "null", $productRangeAndPrice = "null"){

		$this->idDogMedicalInsuranceProvider = $idDogMedicalInsuranceProvider;
		$this->ecbrNo = $ecbrNo;
		$this->idBusiness = $idBusiness;
		$this->profile = $profile;
		$this->specialistArea = $specialistArea;
		$this->productRangeAndPrice = $productRangeAndPrice;
    
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdDogMedicalInsuranceProvider() {
        return $this->idDogMedicalInsuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idDogMedicalInsuranceProvider ARGDESCRIPTION
     */
    public function setIdDogMedicalInsuranceProvider($idDogMedicalInsuranceProvider) {
        $this->idDogMedicalInsuranceProvider = $idDogMedicalInsuranceProvider;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getEcbrNo() {
        return $this->ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $ecbrNo ARGDESCRIPTION
     */
    public function setEcbrNo($ecbrNo) {
        $this->ecbrNo = $ecbrNo;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProfile() {
        return $this->profile;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $profile ARGDESCRIPTION
     */
    public function setProfile($profile) {
        $this->profile = $profile;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getIdBusiness() {
        return $this->idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $idBusiness ARGDESCRIPTION
     */
    public function setIdBusiness($idBusiness) {
        $this->idBusiness = $idBusiness;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getSpecialistArea() {
        return $this->specialistArea;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $specialistArea ARGDESCRIPTION
     */
    public function setSpecialistArea($specialistArea) {
        $this->specialistArea = $specialistArea;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getProductRangeAndPrice() {
        return $this->productRangeAndPrice;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $productRangeAndPrice ARGDESCRIPTION
     */
    public function setProductRangeAndPrice($productRangeAndPrice) {
        $this->productRangeAndPrice = $productRangeAndPrice;
    }

    /* OTHER FUNCTIONS */
    
    /* STATIC FUNCTIONS */
    public static function addDogMedicalInsuranceProvider($db, $dogMedicalInsuranceProvider, $tableName = "dogMedicalInsuranceProviderFields"){
		
		$query = "INSERT INTO " . $tableName . " (idDogMedicalInsuranceProvider, ecbrNo, idBusiness, specialistArea, productRangeAndPrice) VALUES (
		" . $dogMedicalInsuranceProvider->getIdDogMedicalInsuranceProvider() . ", 
		'" . $dogMedicalInsuranceProvider->getEcbrNo() . "', 
		'" . $dogMedicalInsuranceProvider->getIdBusiness() . "',
		'" . $dogMedicalInsuranceProvider->getSpecialistArea() . "',
		'" . $dogMedicalInsuranceProvider->getProductRangeAndPrice() . "'
		)";
		
		if(!$db->query($query)){

			return 1;
		
		}
	
	}
    
}

