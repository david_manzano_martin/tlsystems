<?php

require(__DIR__ . "/../classes/HealthTest.php");

if(!User::checkPermissionRequired(3, $user)){
	header("Location: " . $_SERVER["PHP_SELF"]);
}

$healthTestsArray = HealthTest::getAllHealthTests($db);

require(__DIR__ . "/../../views/rootViews/viewHealthTests-view.php");

?>