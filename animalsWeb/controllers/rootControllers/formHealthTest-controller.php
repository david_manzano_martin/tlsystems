<?php

require(__DIR__ . "/../classes/HealthTest.php");

if(!User::checkPermissionRequired(3, $user)){
	header("Location: " . $_SERVER["PHP_SELF"]);
}

switch($page) {

	case "addHealthTest":{
		$temporalHealthTest = new HealthTest("", "", "");
		$title = "Adding new health test";
		$buttonText = "Add";
		$idField = "";
		break;
	}
	case "modifyHealthTest":{
		$name = $_GET["name"];
		$title = "Modifying " . $name . " test";
		$temporalHealthTest = HealthTest::getHealthTestByAttr($db, "healthTestName", $name);
		$idField = '<input type="hidden" value="' . $temporalHealthTest->getIdHealthTest() . '" name="idHealthTest">';
		$buttonText = "Update";
		break;
	}
	
}

require(__DIR__ . "/../../views/rootViews/formHealthTest-view.php");

?>