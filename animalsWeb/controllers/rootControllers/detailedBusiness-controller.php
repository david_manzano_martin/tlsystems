<?php

require(__DIR__ . "/../classes/Business.php");

if(!User::checkPermissionRequired(3, $user)){
	header("Location: " . $_SERVER["PHP_SELF"]);
}

$business = Business::searchBusinessByAttr($db, "idBusiness", $_GET["id"]);

require(__DIR__ . "/../../views/rootViews/detailedBusiness-view.php");

/* FUNCTIONS */
function checkSelected($val1, $val2){

	if($val1 == $val2){

		echo "selected";
	
	}

}

?>