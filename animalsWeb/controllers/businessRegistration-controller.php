<?php

require(__DIR__ . "/classes/BusinessType.php");
require(__DIR__ . "/classes/Business.php");
require(__DIR__ . "/classes/Breeder.php");
require(__DIR__ . "/classes/Breed.php");
require(__DIR__ . "/classes/UkRegion.php");



if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["businessType"])){

	$businessType = BusinessType::getBusinessTypeByAttr($db, "businessId", $_POST["businessType"]);
	
	(isset($_POST["name"])) ? $name =  $_POST["name"]: $name =  $_GET["name"] ;

	if(Business::searchBusinessByAttr($db, "companyName", $name) === 1){

		header("Location: " . $_SERVER["PHP_SELF"] . "?page=businessRegistration");
	
	} else{

		$newBusiness = Business::searchBusinessByAttr($db, "companyName", $_POST["name"]);
	
	}
	
	$businessTypesArray = BusinessType::getAllBusiness($db);
	$ukRegionsArray = UkRegion::getAllUkRegions($db);

	switch($businessType->getBusinessName()) {

		case "Pet Shop":{

			require(__DIR__ . "/../views/formsRegistration/petShopRegistration-view.php");
			break;
		
		}
		
		case "Doggy Day Care":{

			require(__DIR__ . "/../views/formsRegistration/dogyDayCareRegistration-view.php");
			break;
		
		}

		case "Rehoming Centre":{

			require(__DIR__ . "/../views/formsRegistration/rehomingCentreRegistration-view.php");
			break;
		
		}
		
		case "Dog Trainer":{

			require(__DIR__ . "/../views/formsRegistration/dogTrainerRegistration-view.php");
			break;
		
		}
		
		case "Breeder":{

			$breedsArray = Breed::getAllBreeds($db);
			require(__DIR__ . "/../views/formsRegistration/breederRegistration-view.php");
			break;
		
		}
		
		case "Dog Medical insurance Provider":{

			require(__DIR__ . "/../views/formsRegistration/dogMedicalInsuranceProviderRegistration-view.php");
			break;
		
		}
		
		case "Dog Walker":{

			require(__DIR__ . "/../views/formsRegistration/dogWalkerRegistration-view.php");
			break;
		
		}
		
		case "Pet Sitter":{

			require(__DIR__ . "/../views/formsRegistration/petSitterRegistration-view.php");
			break;
		
		}
		
		case "Pet Craft":{

			require(__DIR__ . "/../views/formsRegistration/dogCraftRegistration-view.php");
			break;
		
		}
		
		case "Dog Behaviourist":{

			require(__DIR__ . "/../views/formsRegistration/dogBehaviouristRegistration-view.php");
			break;
		
		}
		
		case "Dog Grommer":{
			
			require(__DIR__ . "/../views/formsRegistration/dogGrommerRegistration-view.php");
			break;
		
		}
		
		case "Kennel":{
			
			require(__DIR__ . "/../views/formsRegistration/kennelRegistration-view.php");
			break;
		
		}
		
		case "Dog Food Provider":{
			
			require(__DIR__ . "/../views/formsRegistration/dogFoodProviderRegistration-view.php");
			break;
		
		}
		
		case "Home Boarder":{
			
			require(__DIR__ . "/../views/formsRegistration/homeBoarderRegistration-view.php");
			break;
		
		}
		
		case "Pet Photographer":{
			
			require(__DIR__ . "/../views/formsRegistration/petPhotographerRegistration-view.php");
			break;
		
		}
		
		case "Vet":{

			require(__DIR__ . "/../views/formsRegistration/vetRegistration-view.php");
			break;
		
		}
	
	}

} else{

	$businessTypesArray = BusinessType::getAllBusiness($db);

	require(__DIR__ . "/../views/formsRegistration/commonRegistration-view.php");

}

/* FUNCTIONS */
?>