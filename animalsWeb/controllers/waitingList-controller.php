<?php

require(__DIR__ . "/classes/WaitingList.php");
require(__DIR__ . "/classes/Breed.php");
require(__DIR__ . "/classes/UkRegion.php");

$waitingListArray = WaitingList::getWaitingListByUser($db, $user);
$arrayBreeds = Breed::getAllBreeds($db);
$arrayUkRegions = UkRegion::getAllUkRegions($db);

require(__DIR__ . "/../views/waitingList-view.php");

?>