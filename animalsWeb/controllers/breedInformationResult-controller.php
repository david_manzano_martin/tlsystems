<?php

require(__DIR__ . "/classes/Breed.php");
require(__DIR__ . "/classes/HealthTest.php");
require(__DIR__ . "/classes/BreedHealthTestRelation.php");

$breedName = $_GET["breedName"];

if(!$breedObject = Breed::getBreedByAttr($db, "breedName", $breedName)){
	
	header("Location: ?page=breedInformation");
	
}

$allTests = BreedHealthTestRelation::getHealthTestsByBreed($db, $breedObject);

$breedObject->formatWeb();

require(__DIR__ . "/../views/breedInformationResult-view.php");

?>