<?php

	$stringToCompare = "davidmanzanomartin@gmail.com";
	$indexArray = "email";

	$regexps = array(
		"empty"=>"/^$/",
		"email"=>"/^[a-zA-Z]{0,}[@]{1}[a-z]{0,}[.]{1}[a-z]{0,}$/",
		"surNames" => "/^[A-Z]{1}[a-z]{0,}[ ]{1}[A-Z]{1}[a-z]{0,}$/",
		"phone" => "/^[0-9]{11}$/",
		"empty/name" => "/(^$)|(^[A-Z]{1}[a-z]{1,}$)/",
		"username" => "/^[a-zA-Z1-9]{1,9}$/",
		"password" => "/^[a-zA-Z1-9]{1,9}$/",
		//BUSINESS COMMON REGISTRATION
		"name" => "/^[A-Za-z1-9 ]{1,}$/",
		"contact" => "/^[A-Z]{1}[a-z]{1,}$/",
		"vat" => "/(^yes$)|(^no$)/",
		"company" => "/^[a-zA-Z0-9. ]{1,20}$/",
		"coNumber" => "/^[0-9]{1,10}$/",
		//"logo" => "/^[a-zA-Z0-9 .\/\\]{1,}$/",
		"logo" => "/^[a-zA-Z0-9 .\/\\]{1,}((.jpg)|(.png)|(jpeg))$/",
		"webPage" => "/^(www.)[a-z\/0-9]{1,}(.)[a-z]{1,}$/",
		"businessType" => "/^[0-9]{1,2}$/",
		//BREEDER
		"cbrRegNo" => "/^[0-9]{1,8}$/", 
		"kcBreedingName" => "/^[a-z A-Z]{1,40}$/", 
		"vetName" => "/^[A-Za-z1-9 ]{1,}$/", 		
		"vetReference" => "/^[a-zA-Z0-9]{12}$/", 
		"numberDogs" => "/^[0-9]{1,4}$/",
		"numberDams" => "/^[0-9]{1,4}$/",
		"aboutYou" => "/^[A-Z()a-z0-9 .,?@]{1,255}$/",
		"cbrAccredited" => "/^[a-zA-Z0-9]{1,20}$/",
		//DOG BEAHOURIST
		"businessInsurance" => "/(^yes$)|(^no$)/",
		"insuranceProvider" => "/^[a-zA-Z0-9]{1,25}$/",
		"canineFirstAidQualification" => "/^[0-3]{1,2}$/",
		"degree" => "/(^yes$)|(^no$)/",
		"qualification" => "/^[0-3]{1,2}$/",
		"examingBody" => "/(^yes$)|(^no$)/",
		"experienceYears" => "/^[0-3]{1,2}$/",
		"areaCovered" => "/^[€a-z A-Z0-9.,\/\\()@]{1,255}$/",
		"productRangePrice" => "/^[€a-z A-Z0-9.,\/\\()@]{1,255}$/",
		//CRAFT
		"productOverview" => "/^[a-zA-Z, .0-9()]{1,255}$/",
		"productNamePrice" => "/^[€a-z A-Z0-9.,\/\\()@]{1,255}$/",
		"linkShop" => "/^(www.)[a-z\/0-9]{1,}(.)[a-z]{1,}$/",
		//DOG FOOD PROVIDER
		"typeFood" => "/^[a-zA-Z, ]{1,52}$/",
		"homeDelivery" => "/(^yes$)|(^no$)/",
		"shipping" => "/(^yes$)|(^no$)/",
		"personalConsultation" => "/(^yes$)|(^no$)/",
		//GROOMER
		"pickUpDropService" => "/(^yes$)|(^no$)/",
		//MEDICAL INSURANCE PROVIDER
		"specialistArea" => "/^[€a-z A-Z0-9.,\/\\()@]{1,255}$/",
		//DOG TRAINER
		"validDbs" => "/(^yes$)|(^no$)/",
		"kgGcdsApproved" => "/^[€a-z A-Z0-9.,\/\\()@]{1,255}$/",
		//DOG WALKER
		"canineAidQualification" => "/^[0-9]{1,2}$/",
		"maxDogsTogheter" => "/^[0-9]{1,3}$/",
		"areasCovered" => "/^[€a-z A-Z0-9.,\/\\()@]{1,255}$/",
		"airCondition" => "/(^yes$)|(^no$)|(^withoutVan$)/",
		"productRangeAndPrice" => "/^[€a-z A-Z0-9.,\/\\()@]{1,255}$/",
		//Home boarder
		"localAutorithyLicense" => "/^[a-zA-Z0-9]{1,10}$/",
		"businessINsurance" => "/(^yes$)|(^no$)/",
		"noStaff" => "/^[0-9]{1,10}$/",
		"noDogLicenseBoard" => "/^[0-9]{1,10}$/",
		"preferedDogs" => "/^[a-z]{1,12}$/",
		"acceptNeuteredMales" => "/(^yes$)|(^no$)/",
		"acceptNeuteredFemales" => "/(^yes$)|(^no$)/",
		"yearsExperience" => "/^[0-9]{1,3}$/",
		"methodOfExercise" => "/^[a-zA-Z1-9 ]{1,45}$/",
		"otherFacilities" => "/^[a-zA-Z1-9 ,]{1,45}$/",
		"openingHours" => "/^[a-zA-Z0-9- .:;]{1,25}$/",
		//KENNEL
		"canineQualifications" => "/^[0-9]{1,2}$/",
		"noStuff" => "/^[0-9]{1,12}$/",
		"noKennels" => "/^[0-9]{1,12}$/",
		"exerciseMethods" => "/^[a-zA-Z1-9 ]{1,45}$/",
		"noYears" => "/^[0-9]{1,3}$/",
		"Area_covered" => "/^[€a-z A-Z0-9.,\/\\()@]{1,255}$/",
		//PET PHOTOGRAPHER
		//PET SITTER
		//REHOMING CENTER
		"Contact_Name_1" => "/^[A-Za-z1-9 ]{1,}$/",
		"Contact_Name_2" => "/^[A-Za-z1-9 ]{1,}$/",
		"rehomingCentreName" => "/^[a-zA-Z ,.0-9]{1,25}$/",
		"charityNumber" => "/^[0-9]{1,12}&/",
		"landlineNumber" => "/^[0-9]{1,12}&/",
		"mobileNoContact1" => "/^[0-9]{11}$/",
		"mobileNoContact2" => "/^[0-9]{11}$/"
	);
	
	$testArray = array(
		"email" => "d@g.c",
		"empty/name" => "/(^yes$)|(^no$)/",
		"insuranceProvider" => "/^[a-zA-Z0-9]{1,25}$/"
	);
	
	//echo(preg_match($regexps[$indexArray], $stringToCompare));
	
	//checkArrayToRegexp($testArray);
	
	function checkArrayToRegexp($arrayToCompare){

		global $regexps;
		$flag = true;
		$arrayResults = array();
		
		//print_r($arrayToCompare);
		//echo("<br>");

		for($k=0; $k<=count($arrayToCompare)-1;$k++) {

			//$_POST[array_keys($_POST)[$k]])
			if(preg_match($regexps[array_keys($arrayToCompare)[$k]], $arrayToCompare[array_keys($arrayToCompare)[$k]]) === 0){
				array_push($arrayResults, array_keys($arrayToCompare)[$k]);
			}
		
		}
		
		return $arrayResults;
	
	}

?>