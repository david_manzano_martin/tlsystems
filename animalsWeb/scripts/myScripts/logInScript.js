var form = null;

window.addEventListener("load", ()=>{

	form = document.getElementById("registationForm");
	var btnSend = document.getElementById("btnSend");
	
	var arrayInputs = document.querySelectorAll("input");
	
	for (k=0; k<=arrayInputs.length-1; k++) {
		
		arrayInputs[k].addEventListener("keypress", (e)=>{
			if(e.code == "Enter" || e.code == "NumpadEnter"){
				send();	
			}
		});
		
	}
	
	btnSend.addEventListener("click",send);
	
	

});

function send(){

	let data = serializePostData(form);
	login(data);

}

function login(data){
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
   	if (this.readyState == 4 && this.status == 200) {
      	// Typical action to be performed when the document is ready:
      	console.log(this.responseText);
      	checkResponse(this.responseText);
      	redirect(this.responseText);
      	//console.log("received");
   	}
	};

	xhttp.open("POST", "?page=logInModel", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data); 

}
