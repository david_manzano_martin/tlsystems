function chooseCheckbox(element){
	console.log("Here");
	let name = element.getAttribute("name");
	let elements = document.querySelectorAll("form input[name='" + name + "']");

	for (let k = 0; k<=elements.length-1; k++) {

		elements[k].checked = false;

	}

	element.checked = true;	

}

function enableDisableInput(element){

	let targetClass = element.getAttribute("dataToggle");
	
	let textElement = document.querySelector("." + targetClass);
	console.log(textElement);
	
	if(element.value == "yes"){

		textElement.disabled = false;
	
	} else{

		textElement.disabled = true;
		textElement.value = 0;
	
	}

}

function checkResponse(response){
	
	let response1 = JSON.parse(response);

	Swal.fire({
		type: response1["type"],
		title: response1["title"],
		text: response1["text"]
	});

}

function serializePostData(form){

	let arrayResults = new Array();
	let result = "";
	let dataToAdd = form.querySelectorAll("input:not([type='checkbox']), select, textArea, input[type='checkbox']:checked");
	
	for (let k=0; k<=dataToAdd.length-1; k++) {

		if(dataToAdd[k].getAttribute("name") != "logo"){
			result += dataToAdd[k].getAttribute("name") + "=" + dataToAdd[k].value + "&";
		}
	
	}
	
	result = result.substring(0, result.length-1);
	
	return result;

}

function redirect(response){

	let responseFormatted = JSON.parse(response);
	
	if(responseFormatted["type"] == "success"){
		
		document.querySelector("button.swal2-confirm").addEventListener("click", ()=>{

				document.querySelector("form").submit();
		
		});

		setTimeout(()=>{

			document.querySelector("form").submit();
		
		}, 5000);
		
	
	}

}