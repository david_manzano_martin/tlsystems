var visibleRow = false;

window.addEventListener("load", ()=>{
	
	//updateProfileLogo

	/* btnShow / btnHide */
	
	let arraybtnDelete = document.querySelectorAll(".btnRemove");
	
	let btnAddBusiness = document.getElementById("btnAddBusiness");
	let addWaitingList = document.getElementById("addWaitingList").querySelector("form");
	let addBtn = document.getElementById("addBtn");
	let form = document.querySelector("form");
	console.log(form);
	
	addBtn.addEventListener("click", ()=>{

		
		addWaitingListFnt(serializePostData(form));

	});
	
	btnAddBusiness.addEventListener("click", ()=>{
		let addWaitingList = document.getElementById("addWaitingList");
		if(addWaitingList.classList.contains("invisible")){
			addWaitingList.classList.remove("invisible");
		} else{
			addWaitingList.classList.add("invisible");
		}
		/*let addWaitingList = document.getElementById("addWaitingList");
		
		console.log(addWaitingList);

		if(visibleRow){
			addWaitingList.style.visibility = "hidden";
			visibleRow = !visibleRow;
		} else{
			addWaitingList.style.visibility = "visible";
			visibleRow = !visibleRow
		}*/
		
	});
	
	
	
	for (let k=0; k<=arraybtnDelete.length-1; k++){
		arraybtnDelete[k].addEventListener("click", (e)=>{
			let parent = e.target.parentNode.parentNode;
			let data= getDataToRemove(parent);
			removeWaitingList(data);
		});
	}

});

function showContent(className){
	let element = document.querySelector("." + className);
	element.classList.remove("invisible");
}

function hideContent(className){
	let element = document.querySelector("." + className);
	element.classList.add("invisible");
}

function getDataToRemove(trElement){

	let result = "";
	
	let breedName = trElement.querySelector("td:nth-child(2)").innerHTML;
	let regionName = trElement.querySelector("td:nth-child(3)").innerHTML;
	
	result = "breedName=" + breedName + "&regionName=" + regionName;
	
	return result;
	
}

function removeWaitingList(data){

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		console.log(this.status);
   	if (this.readyState == 4 && this.status == 200) {
   		console.log(this.responseText);
      	checkResponse(this.responseText);
      	redirect(this.responseText);
   	}
	};

	xhttp.open("POST", "?page=removeWaitingModel", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data); 

}

function addWaitingListFnt(data){
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		console.log(this.status);
   	if (this.readyState == 4 && this.status == 200) {
   		console.log(this.responseText);
      	checkResponse(this.responseText);
      	redirect(this.responseText);
      	
   	}
	};

	xhttp.open("POST", "?page=addWaitingModel", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data); 

}