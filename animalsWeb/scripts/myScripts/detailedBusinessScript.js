window.addEventListener("load", ()=>{

	let sendButton = document.getElementById("sendButton");
	let form = document.querySelector("form");
	
	sendButton.addEventListener("click", ()=>{

		//getFormData();
		//console.log(serializePostData(form));
		updateBusiness(serializePostData(form));
	
	});

});

function updateBusiness(data){
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
   	if (this.readyState == 4 && this.status == 200) {
      	// Typical action to be performed when the document is ready:
      	console.log(this.responseText);
      	checkResponse(this.responseText);
      	redirect(this.responseText);
      	//console.log("received");
      	//console.log(this.responseText);
   	}
	};

	xhttp.open("POST", "?page=updateBusinessModel", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data); 

}