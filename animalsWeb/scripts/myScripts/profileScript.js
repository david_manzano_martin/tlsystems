var visibleIframe = false;

window.addEventListener("load", ()=>{
	
	//updateProfileLogo

	/* btnShow / btnHide */
	
	let arraybtnShow = document.querySelectorAll(".btnShow");
	let arraybtnHide = document.querySelectorAll(".btnHide");
	let btnAddBusiness = document.getElementById("btnAddBusiness");
	let addIframe = document.getElementById("addIframe");
	let imageProfile = document.getElementById("imageProfile");
	let inputImageProfile = document.getElementById("inputImageProfile");
	
	inputImageProfile.addEventListener("change", ()=>{

		uploadLogo();
	
	});
	
	imageProfile.addEventListener("click", ()=>{

		inputImageProfile.click();
	
	});
	
	addIframe.contentWindow.addEventListener("load", ()=>{
		alert("loaded");
	});
	
	btnAddBusiness.addEventListener("click", ()=>{
		let addIframe = document.getElementById("addIframe");
		let insideIframe = (addIframe.contentWindow || addIframe.contentDocument);

		if(visibleIframe){
			addIframe.style.display = "none";
			visibleIframe = !visibleIframe;
		} else{
			addIframe.style.display = "block";
			visibleIframe = !visibleIframe
		}
		
		
		insideIframe.document.querySelector("body form").action = insideIframe.document.querySelector("body form").action + "&iframe=true";
		console.log(insideIframe.document.querySelector("body form").action);
	});
	
	for (let k=0; k<=arraybtnShow.length-1; k++){
		arraybtnShow[k].addEventListener("click", (e)=>{
			changeShowHide(e.target.parentNode, "btnHide");
			let className = e.target.getAttribute("dataToggle");
			showContent(className);
		});
	}
	
	for (let k=0; k<=arraybtnHide.length-1; k++){
		arraybtnHide[k].addEventListener("click", (e)=>{
			changeShowHide(e.target.parentNode, "btnShow");
			let className = e.target.getAttribute("dataToggle");
			hideContent(className);
		});
	}
	
	var cssLink = document.createElement("link");
cssLink.href = "styles/myStyles/iframe.css"; 
cssLink.rel = "stylesheet"; 
cssLink.type = "text/css"; 
frames['iframe'].document.head.appendChild(cssLink);
//addIframe.document.head.appendChild(cssLink);

});

function showContent(className){
	let element = document.querySelector("." + className);
	element.classList.remove("invisible");
}

function hideContent(className){
	let element = document.querySelector("." + className);
	element.classList.add("invisible");
}

function uploadLogo() {
	var fileInput = document.getElementById("inputImageProfile");
	var file = fileInput.files[0];
   var url = "?page=updateProfileLogo";
   console.log(fileInput);
   //var url = "?page=uploadLogo";
   var xhr = new XMLHttpRequest();        	
   var fd = new FormData();
   xhr.open("POST", url, true);
   xhr.onreadystatechange = function() {
       if (xhr.readyState === 4 && xhr.status === 200) {
       	console.log(this.responseText);
       	checkResponse(this.responseText);
       }
   };
   fd.append("file", file);
   xhr.send(fd);
   }

function changeShowHide(parent, visibleElement){

	switch(visibleElement) {
		case "btnShow":{
			let btnShow = parent.querySelector(".btnShow");
			let btnHide = parent.querySelector(".btnHide");

			btnShow.classList.remove("invisible");
			btnShow.classList.add("visible");

			btnHide.classList.add("invisible");
			btnHide.classList.remove("visible");
			
			break;
		}
		case "btnHide":{
			let btnShow = parent.querySelector(".btnShow");
			let btnHide = parent.querySelector(".btnHide");
			
			btnShow.classList.add("invisible");
			btnShow.classList.remove("visible");

			btnHide.classList.remove("invisible");
			btnHide.classList.add("visible");
			
			break;
		}
	}

}