window.addEventListener("load", ()=>{

	 var inputElement = document.getElementById("searcherInput");
	 inputElement.addEventListener("keyup", ()=>{
	 	getNames(inputElement.value);
	 });
	 
	 getNames("");

});

function formatResult(data){

	let localArray = JSON.parse(data);
	let parent = document.getElementById("results");
	
	parent.innerHTML = "";
	
	for (let k=0; k<= localArray.length-1; k++) {

		let divElement = document.createElement("div");
		let aElement = document.createElement("a");
		
		divElement.classList.add("dogResult");
		divElement.addEventListener("click", (element)=>{
			sendForm(element.target);
		});
		
		aElement.appendChild(document.createTextNode(localArray[k]["name"]));
		
		divElement.appendChild(aElement);
		
		parent.appendChild(divElement);
		
	}
	
	/*<div onclick="sendForm(this)" class="dogResult">
                  	<a href="" >bread1</a>
                  </div>*/

}

function getNames(name){

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
      formatResult(this.responseText);
      return this.responseText;
    }
	}
	
	xhttp.open("GET", "?page=searchBreedByNameModel&name=" + name, true);
	xhttp.send();

}

function sendForm(element){
	
	console.log(element);
	let pageOption = "breedInformationResult";
	let breedName = element.querySelector("a").innerText;
	
	window.location.href = "?page=" + pageOption + "&breedName=" + breedName;
	
}