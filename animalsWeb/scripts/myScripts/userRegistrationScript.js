var form;

window.addEventListener("load", ()=>{

	let sendButton = document.getElementById("sendButton");
	form = document.querySelector("form");
	
	let locationPath = window.location.toString();
	
	let checkBoxElements = document.querySelectorAll(".checkBox");
	
	for(let k=0; k<=checkBoxElements.length-1; k++){

		checkBoxElements[k].addEventListener("click", (e)=>{

			chooseCheckbox(e.target);
		
		});
	
	}
	
	//chooseCheckbox(e.target);

	var offsetValue =locationPath.substring((locationPath.indexOf("offsetValue") + 12), locationPath.length);
	console.log();
	
	
	sendButton.addEventListener("click", ()=>{

		//getFormData();
		//console.log(serializePostData(form));
		registerUser(serializePostData(form));
	
	});

});

function checkTypeRegistration(){

	let checkBoxElements = document.querySelectorAll("input[type='checkbox']");
	let selected;
	
	for(let k=0; k<=checkBoxElements.length-1; k++){

		if(checkBoxElements[k].checked == true){

			selected = checkBoxElements[k];
		
		}
	
	}
	
	if(selected.value == "client"){
		let formAction = form.action;
		form.action = formAction.substring(0, (formAction.indexOf("index.php") + 15)) + "main"; 
	}

}

function registerUser(data){
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
   	if (this.readyState == 4 && this.status == 200) {
      	// Typical action to be performed when the document is ready:
      	console.log(this.responseText);
      	//console.log("registered");
      	checkTypeRegistration();
      	checkResponse(this.responseText);
      	redirect(this.responseText);
   	}
	};

	xhttp.open("POST", "?page=userRegistrationModel", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data); 

}

