var commonRegistration;
var secondPart = false;
var businessName ="";

window.addEventListener("load", () => {

    let sendButton = document.getElementById("sendButton");
    let form = document.querySelector("form");
    let checkBoxElements = document.querySelectorAll("input[type='checkbox']");

    //console.log(document.querySelector(("input[name='name']")).getAttribute("disabled"));
    //window.location.href = "?page=userRegistration";
    let locationPath = window.location.href;

    if (locationPath.includes("secondPart")) {
        secondPart = true;
        businessName = document.querySelector("form input[name='name']").value;
    }
    
    for (let k = 0; k<=checkBoxElements.length-1; k++) {

		checkBoxElements[k].addEventListener("click", (e)=>{

			chooseCheckbox(e.target);
			enableDisableInput(e.target);
		
		});
	
	}

    if (document.querySelector(("input[name='name']")).getAttribute("disabled") == null) {

        //console.log("No disabled");
        commonRegistration = true;

    } else {

        commonRegistration = false;
        //console.log("Disabled");

    }


    sendButton.addEventListener("click", () => {

        //getFormData();
        console.log(serializePostData(form));
        sendForm(serializePostData(form));

    });

});

function sendForm(data) {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            /*console.log(this.responseText);
            checkResponse(this.responseText);
            redirect(this.responseText);*/

            if (commonRegistration) {
                let responseFormatted = JSON.parse(this.responseText);

                if (responseFormatted["type"] == "success") {

                    uploadLogo();

                } else {

                    checkResponse(this.responseText);

                }
            } else {

                let responseFormatted = JSON.parse(this.responseText);

                if (responseFormatted["type"] == "success") {

                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false,
                    })

                    swalWithBootstrapButtons.fire({
                        title: 'Add more areas of your business (eg dog walking, dog grooming etc',
                        text: "Choose your answer!",
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                        reverseButtons: true
                    }).then((result) => {
                        if (result.value) {
                            window.location.href = "?page=addMoreTypes&name=" + businessName;
                        } else if (
                            // Read more about handling dismissals
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                        		checkResponse(this.responseText);
                					redirect(this.responseText);
                        }
                    })

                } else {

                    checkResponse(this.responseText);

                }

                //checkResponse(this.responseText);

                //redirect(this.responseText);

            }

        }
    };

    xhttp.open("POST", "?page=createBusinessModel", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(data);

}

function checkIsIframe() {

    let result;
    let element = document.querySelector("head");

    if (element.innerHTML.match(/(iframe.css)/) != null) {
        result = true;
    } else {
        result = false;
    }

    return result;

}

function uploadLogo() {
    var fileInput = document.getElementById("logoField");
    var file = fileInput.files[0];
    var url = "?page=uploadLogo&businessName=" + document.querySelector("input[name='name']").value;
    console.log(fileInput);
    //var url = "?page=uploadLogo";
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            checkResponse(this.responseText);
            redirect(this.responseText);
        }
    };
    fd.append("file", file);
    xhr.send(fd);
}

/* function serializePostData(form){

	let arrayResults = new Array();
	let result = "";
	let dataToAdd = form.querySelectorAll("input, select, textArea");
	
	for (let k=0; k<=dataToAdd.length-1; k++) {

		result += dataToAdd[k].getAttribute("name") + "=" + dataToAdd[k].value + "&";
	
	}
	
	result = result.substring(0, result.length-1);
	
	return result;

} */

function getClasses(element) {

    let result = "";
    let arrayClasses = element.classList

    for (let k = 0; k <= arrayClasses.length - 1; k++) {

        result += " " + arrayClasses[k];

    }

    return result;

}

function getFormData() {

    let arrayElements = new Array();
    let formInputsArray = document.querySelectorAll("form input, form select, form textArea");

    for (let k = 0; k <= formInputsArray.length - 1; k++) {

        //console.log(formInputsArray[k]);
        let localInput = {

            classes: getClasses(formInputsArray[k]),
            disabled: formInputsArray[k].getAttribute("disabled"),
            mainType: formInputsArray[k].nodeName,
            secondType: formInputsArray[k].getAttribute("type"),
            name: formInputsArray[k].getAttribute("name"),
            showInformation: formInputsArray[k].getAttribute("displayName"),
            placeholder: formInputsArray[k].getAttribute("placeholder"),
            value: formInputsArray[k].value

        };

        arrayElements.push(localInput);

    }

    console.log(arrayElements);

    console.log(JSON.stringify(arrayElements));

    return arrayElements;

}