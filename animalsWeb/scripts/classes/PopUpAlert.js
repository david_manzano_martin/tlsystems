class PopUpAlert{

	constructor(data){
		//this.data = JSON.parse(data);
		this.data = data;	
	}
	
	createContainer(type){

		let containerElement = document.createElement("div");
		containerElement.classList.add(type);
		
		return containerElement;
	
	}
	
	createRow(){

		let rowElement = document.createElement("div");
		rowElement.classList.add("row");
		
		return rowElement;
	
	}
	
	createButton(text, type){

		let localButton = document.createElement("button");
		
		localButton.classList.add(type);
		localButton.innerText = text;
		
		return localButton;
	
	}
	
	print(){

		let parentElement = document.createElement("div");
		let insideElement = document.createElement("div");
		let firstContainer = this.createContainer("container-fluid");
		let firstRow = this.createRow();
		
		parentElement.id = "messageBox";
		
		insideElement.id = "insideBox";
		
		if(this.data["option"] == "choose"){
			console.log(Object.keys(this.data["buttons"]).length);
			for (let k=0; k<=Object.keys(this.data["buttons"]).length-1; k++) {
				
				let localButtonAdd = this.createButton(this.data["buttons"][k]["text"], this.data["buttons"][k]["class"]);
				console.log(localButtonAdd);
				firstRow.appendChild(localButtonAdd);
			
			}
			
		}
		
		firstContainer.appendChild(firstRow);
		insideElement.appendChild(firstContainer);
		
		parentElement.appendChild(insideElement);
		
		document.querySelector("body").appendChild(parentElement);
		
		
	}
	
	test(){
		return this.data;
	}

}