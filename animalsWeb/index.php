<?php

session_start();

require(__DIR__ . "/database/connection.php");
require(__DIR__ . "/controllers/classes/User.php");
//require(__DIR__ . "/controllers/classes/testClass.php");

//BusinessTypesRelations::createRelation($db, 5, 1);
/*$fileName = "testfile.jpeg";
echo$fileName;
$extension = substr($fileName, strrpos($fileName, ".") + 1, strlen($fileName));
echo("Extension: " . $extension);*/

$user = null;

//$user = new User();
User::checkUserLogged($db);

echo($folderPath);


$pageName = "WELCOME";

(!isset($_GET["page"])) ? $page = "main" : $page = $_GET["page"];

switch($page) {

	case"breedInformation":{
		require(__DIR__ . "/controllers/breedInformation-controller.php");
		break;
	}
	
	case"breedInformationResult":{
		require(__DIR__ . "/controllers/breedInformationResult-controller.php");
		break;
	}
	
	case"userRegistration":{
		require(__DIR__ . "/controllers/userRegistration-controller.php");
		break;
	}
	
	case"userRegistrationModel":{
		require(__DIR__ . "/models/userRegistration-model.php");
		break;
	}
	
	case"businessRegistration":{
		require(__DIR__ . "/controllers/businessRegistration-controller.php");
		break;
	}
	
	case"updateBusinessModel":{
		require(__DIR__ . "/models/updateBusiness-model.php");
		break;
	}
	
	case"createBusinessModel":{
		require(__DIR__ . "/models/createBusiness-model.php");
		break;
	}
	
	case"breederRegistration":{
		require(__DIR__ . "/controllers/breederRegistration-controller.php");
		break;
	}

	case"dogRegistration":{
		require(__DIR__ . "/controllers/dogRegistration-controller.php");
		break;
	}
	
	case"litterRegistration":{
		require(__DIR__ . "/controllers/litterRegistration-controller.php");
		break;
	}
	
	case"publicSearch":{
		require(__DIR__ . "/controllers/publicSearch-controller.php");
		break;
	}
	
	case"publicSearchResult":{
		require(__DIR__ . "/controllers/publicSearchResult-controller.php");
		break;
	}
	
	
	case"breedRegistration":{
		require(__DIR__ . "/controllers/breedRegistration-controller.php");
		break;
	}
	
	case"logIn":{
		require(__DIR__ . "/controllers/logIn-controller.php");
		break;
	}
	
	case"logInModel":{
		require(__DIR__ . "/models/logIn-model.php");
		break;
	}
	
	case"logOut":{
		require(__DIR__ . "/models/logOut-model.php");
		break;
	}
	
	case "searchBreedByNameModel":{
		require(__DIR__ . "/models/searchBreedByName-model.php");
		break;
	}
	
	case"profile":{
		require(__DIR__ . "/controllers/profile-controller.php");
		break;
	}
	
	case "iframeBusiness":{
		require(__DIR__ . "/controllers/iframeBusiness-controller.php");
		break;
	}
	
	case "updateBusinessUser":{
		require(__DIR__ . "/models/updateBusinessUser-model.php");
		break;
	}
	
	case "uploadLogo":{
		require(__DIR__ . "/models/uploadLogo-model.php");
		break;
	}
	
	case "updateProfileLogo":{
		require(__DIR__ . "/models/updateProfileLogo-model.php");
		break;
	}
	
	case "waitingList":{
		require(__DIR__ . "/controllers/waitingList-controller.php");
		break;
	}
	
	case "addWaitingList":{
		require(__DIR__ . "/controllers/addWaitingList-controller.php");
		break;
	}
	
	case "addWaitingModel":{
		require(__DIR__ . "/models/addWaitingList-model.php");
		break;
	}
	
	case "removeWaitingModel":{
		require(__DIR__ . "/models/removeWaitingList-model.php");
		break;
	}
	
	case "addMoreTypes":{
		require(__DIR__ . "/controllers/addMoreTypes-controller.php");
		break;
	}
	
	case "viewHealthTest":{
		require(__DIR__ . "/controllers/viewHealthTest-controller.php");
		break;
	}
	
	case "businessSearch":{
		require(__DIR__ . "/controllers/businessSearch-controller.php");
		break;
	}
	
	case "viewPublicBusiness":{
		require(__DIR__ . "/controllers/viewPublicBusiness-controller.php");
		break;
	}
	
	
	
	/* ONLY ROOT OPTIONS*/

	case "viewHealthTests":{
		require(__DIR__ . "/controllers/rootControllers/viewHealthTests-controller.php");
		break;
	}
	
	case "addHealthTest": case "modifyHealthTest":{
		require(__DIR__ . "/controllers/rootControllers/formHealthTest-controller.php");
		break;
	}
	
	case "updateHealthTestModel":{
		require(__DIR__ . "/models/rootModels/updateHealthTest-model.php");
		break;
	}
	
	case "addHealthTestModel":{
		require(__DIR__ . "/models/rootModels/addHealthTest-model.php");
		break;
	}
	
	case "viewBusiness":{
		require(__DIR__ . "/controllers/rootControllers/viewBusiness-controller.php");
		break;
	}
	
	case "detailedBusiness":{
		require(__DIR__ . "/controllers/rootControllers/detailedBusiness-controller.php");
		break;
	}
	
	
	case "test":{
		require(__DIR__ . "/controllers/test-controller.php");
		break;
	}
	
	default:{
		require(__DIR__ . "/controllers/main-controller.php");
		header("Location: ?page=asd");
		break;
	}

}


?>