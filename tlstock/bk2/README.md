# TLShopStock

stock amagement to import suppliers digital files in to central database to combine all suppliers in 1 database, then to export each suppliers stock to a fomatted csv file for use with prestashop.

# Server side requirement

### PHP Configuration
```php.ini```
upload_max_filesize=20M
post_max_size=20M

- Also requires ZipArchive and ModRewrite to be enabled.


### MySQL Configuration
```my.ini```
max_allowed_packet=16M


### Database tables
- Create a database and, import the tables, ```.sql``` files, into it. Directory: ```_sql```
- Set the database information on the configuration file at ```core/__config__.php```.

