<?php
namespace Core\Utils;

/**
 * @class Slugfy
 * @version 2.7.4
 */
final class Slugfy
{
    /**
     * Converts provided string to URL friendly format.
     * @param String $text The value to be converted.
     * @return String The converted text.
     */
    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = @iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
          return 'untitled';
        }

        return $text;
    }
}