<?php
namespace Core\Utils;

/**
 * @class FileUpload
 * @type Static
 */
class FileUpload
{
    /**
     * Validate web image file-type.
     * @param String $mimeType
     * @return Boolean
     */
    public static function isWebImage($mimeType)
    {
        $result = true;

        switch ($mimeType) {
            case 'image/jpeg':
            case 'image/png':
            case 'image/gif':
                break;
            default:
                $result = false;
        }

        return $result;
    }

    /**
     * Get file extension. WARNING: file type is NOT validated!
     * @param String $fileName
     * @return String
     */
    public static function getFileExtension($fileName)
    {
        if (empty($fileName)) {
            return '';
        }

        $ext = explode('.', $fileName);

        if (count($ext) < 2) {
            return '';
        }

        return strtolower($ext[(count($ext)-1)]);
    }

    /**
     * Convert bytes to MB.
     * @param int $bytes
     * @param int $decimals
     * @return string
     */
    public static function bytesToSize($bytes, $decimals=2)
    {
        $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $factor = floor((strlen($bytes) - 1) / 3);

        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

    /**
     * Create image thumbnail
     * <br>NOTE: requires GDLib enabled.
     * @param string $pathToImages Absolute path to original image.
     * @param string $pathToThumbs Absolute path to thumbnail directory (place where you want to save the thumb).
     * @param string $thumbWidth The desired width size for the thumb. The height is calculated automatically to keep ratio.
     * @return bool.
     */
    public static function createThumbs($pathToImages, $pathToThumbs, $thumbWidth)
    {
        // parse path for the extension
        $ext = explode('.',$pathToImages);
        $ext = $ext[(count($ext)-1)];
        $ext = strtolower($ext);

        // create JPG / JPEG. if it fails at any method call, it will return false.
        if ($ext == 'jpg' || $ext == 'jpeg') {
            // load image and get image size
            if (!$img = @imagecreatefromjpeg($pathToImages)){
                return false;
            }

            $width = @imagesx( $img );   // get image width.
            $height = @imagesy( $img );  // get image height.

            // calculate thumbnail size
            $new_width = $thumbWidth;
            $new_height = floor($height * ($thumbWidth / $width));  // calculate image height based on width ratio.

            // create a new temporary image
            if (!$tmp_img = @imagecreatetruecolor($new_width, $new_height)) {
                return false;
            }

            // copy and resize old image into new image
            if (!@imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height)) {
                return false;
            }

            // save thumbnail into a file
            if (!@imagejpeg($tmp_img, $pathToThumbs)) {
                return false;
            }
        }

        // create GIF
        elseif ($ext == 'gif') {
            // load image and get image size.
            if (!$img = @imagecreatefromgif($pathToImages)) {
                return false;
            }

            $width = imagesx($img);
            $height = imagesy($img);

            // calculate thumbnail size.
            $new_width = $thumbWidth;
            $new_height = floor($height * ($thumbWidth / $width)); // calculate image height based on width ratio.

            // create a new temporary image.
            if (!$tmp_img = @imagecreatetruecolor($new_width, $new_height)) {
                return false;
            }

            // copy and resize old image into new image.
            if (!@imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height)) {
                return false;
            }

            // save thumbnail into a file.
            if (!@imagegif($tmp_img, $pathToThumbs)) {
                return false;
            }
        }

        // create PNG.
        elseif ($ext == 'png') {
            // load image and get image size
            if (!$img = @imagecreatefrompng($pathToImages)) {
                return false;
            }

            $width = @imagesx($img);   // get image width.
            $height = @imagesy($img);  // get image height.

            // calculate thumbnail size.
            $new_width = $thumbWidth;
            $new_height = floor($height * ($thumbWidth / $width)); // calculate image height based on width ratio.

            // create a new temporary image
            if (!$tmp_img = @imagecreatetruecolor($new_width, $new_height)) {
                return false;
            }

            // set transparency
            @imagealphablending($tmp_img, false); // define alpha transparency blending.
            @imagesavealpha($tmp_img, true);      // save alpha channel

            // copy and resize old image into new image
            if (!@imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height)) {
                return false;
            }

            // save thumbnail into a file.
            if (!@imagepng($tmp_img, $pathToThumbs)) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * Public static method: get maximum upload filesize
     * @return string Max filesize in MB
     */
    public static function getMaxUploadFilesize()
    {
        $max_upload = (int)(ini_get('upload_max_filesize'));
        $max_post = (int)(ini_get('post_max_size'));
        $memory_limit = (int)(ini_get('memory_limit'));

        return min($max_upload, $max_post, $memory_limit) . 'MB';
    }


    /**
     * Resize image with pre-fixed size.
     * <br>---------------------------------------------------------------------
     * @param $image_path The path to the image to be resized.
     * @param $width The desired with
     * @param $height The desired height
     * @return boolean
     */
    public static function resizeImageWithFixSize($image_path, $width, $height)
    {
        // validate
        if (!file_exists($image_path) || $width < 1 || $height < 1) {
            return false;
        }

        // get extension.
        $ext = explode('.',$image_path);
        $ext = $ext[(count($ext)-1)];
        $ext = strtolower($ext);

        switch ($ext) {
            case 'jpg':
                $img = imagecreatefromjpeg($image_path);
                break;
            case 'gif':
                $img = imagecreatefromgif($image_path);
                break;
            case 'png':
                $img = imagecreatefrompng($image_path);
                break;
            default:
                return false;
        }

        // define.
        $box_w = $width;
        $box_h = $height;

        //create the image, of the required size
        $new = imagecreatetruecolor($box_w, $box_h);
        if ($new === false) {
            //creation failed -- probably not enough memory
            return null;
        }


        //Fill the image with a light grey color
        //(this will be visible in the padding around the image,
        //if the aspect ratios of the image and the thumbnail do not match)
        //Replace this with any color you want, or comment it out for black.
        //I used grey for testing =)
        $fill = imagecolorallocate($new, 255, 255, 255);
        imagefill($new, 0, 0, $fill);

        //compute resize ratio
        $hratio = $box_h / imagesy($img);
        $wratio = $box_w / imagesx($img);
        $ratio = min($hratio, $wratio);

        //if the source is smaller than the thumbnail size,
        //don't resize -- add a margin instead
        //(that is, dont magnify images)
        if($ratio > 1.0)
            $ratio = 1.0;

        //compute sizes
        $sy = floor(imagesy($img) * $ratio);
        $sx = floor(imagesx($img) * $ratio);

        //compute margins
        //Using these margins centers the image in the thumbnail.
        //If you always want the image to the top left,
        //set both of these to 0
        $m_y = floor(($box_h - $sy) / 2);
        $m_x = floor(($box_w - $sx) / 2);

        //Copy the image data, and resample
        //
        //If you want a fast and ugly thumbnail,
        //replace imagecopyresampled with imagecopyresized
        if (!imagecopyresampled($new, $img,
            $m_x, $m_y, //dest x, y (margins)
            0, 0, //src x, y (0,0 means top left)
            $sx, $sy,//dest w, h (resample to this size (computed above)
            imagesx($img), imagesy($img)) //src w, h (the full size of the original)
        ) {
            //copy failed
            imagedestroy($new);
            return false;
        }

        //copy successful. replace original image.
        switch ($ext) {
            case 'jpg':
                imagejpeg($new,$image_path,100);
                break;
            case 'gif':
                imagegif($new,$image_path);
                break;
            case 'png':
                imagepng($new,$image_path);
                break;
        }

        return true;
    }
}
