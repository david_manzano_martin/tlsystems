<?php
namespace Core\Utils;

/**
 * @class RandomString
 * @version 2.0.0
 */
class RandomString
{
    /**
     * Generate random string
     * @param int $length The length for the string.
     * @return String
     */
    public static function generate($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * Generate random string but only letters
     * @param int $length The length for the generated string.
     * @return string
     */
    public static function generateOnlyLetters($length = 10) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * Generate UUID.
     * @return string
     */
    public static function getUUID()
    {
        return self::generate(11) . '-' . self::generate(7) . '-' . self::generateOnlyLetters(7);
    }
}