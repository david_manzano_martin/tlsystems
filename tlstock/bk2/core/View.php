<?php

namespace Core;

class View
{
    public $controller;
    private $pageSlug = '';

    public function __construct($slug)
    {
        $this->pageSlug = $slug;
    }

    public function loadInc($name)
    {
        $path = BASE_ROOT . 'inc/' . $name . '.html';

        if (!file_exists($path) || !is_file($path)) {
            return '';
        }

        if (filesize($path) < 1) {
            return '';
        }

        $fo = fopen($path, 'r');
        $html = fread($fo, filesize($path));
        fclose($fo);

        return $html;
    }

    public function render($htmlContent)
    {
        $html = '<!DOCTYPE HTML>' . PHP_EOL;
        $html.= '<html>' . PHP_EOL;
        $html.= $this->loadHeader('metaHeader') . PHP_EOL;
        $html.= '<body>';
        $html.= $this->loadHeader('topHeader') . PHP_EOL;
        $html.= '<div style="margin-right:25px;">' . PHP_EOL;
        $html.= '<div class="row">' . PHP_EOL;
        $html.= '<div class="col-md-2">' . PHP_EOL;
        $html.= $this->loadHeader('menuHeader') . PHP_EOL;
        $html.= '</div>';
        $html.= '<div class="col-md-10">' . PHP_EOL;
        $html.= $htmlContent . PHP_EOL;
        $html.= '</div>';
        $html.= '</div>';
        $html.= '</div>';
        $html.= $this->loadHeader('footerHeader') . PHP_EOL;
        $html.= '</body>' . PHP_EOL;
        $html.= '</html>';
        
        $html = str_replace('{::BASE_URI::}', BASE_URI, $html);

        switch (BASE_SLUG) {
            case 'supplier':
                $html = str_replace('{::SUPPLIER_ACTIVE::}', ' class="active"', $html);
                $html = str_replace('{::CATEGORY_ACTIVE::}', '', $html);
                $html = str_replace('{::IMPORT_ACTIVE::}', '', $html);
                $html = str_replace('{::EXPORT_ACTIVE::}', '', $html);
                $html = str_replace('{::SETTINGS_ACTIVE::}', '', $html);
            break;
            case 'categories':
                $html = str_replace('{::CATEGORY_ACTIVE::}', ' class="active"', $html);
                $html = str_replace('{::SUPPLIER_ACTIVE::}', '', $html);
                $html = str_replace('{::IMPORT_ACTIVE::}', '', $html);
                $html = str_replace('{::EXPORT_ACTIVE::}', '', $html);
                $html = str_replace('{::SETTINGS_ACTIVE::}', '', $html);
            break;
            case 'import':
                $html = str_replace('{::IMPORT_ACTIVE::}', ' class="active"', $html);
                $html = str_replace('{::SUPPLIER_ACTIVE::}', '', $html);
                $html = str_replace('{::CATEGORY_ACTIVE::}', '', $html);
                $html = str_replace('{::EXPORT_ACTIVE::}', '', $html);
                $html = str_replace('{::SETTINGS_ACTIVE::}', '', $html);
            break;
            case 'export':
                $html = str_replace('{::EXPORT_ACTIVE::}', ' class="active"', $html);
                $html = str_replace('{::SUPPLIER_ACTIVE::}', '', $html);
                $html = str_replace('{::CATEGORY_ACTIVE::}', '', $html);
                $html = str_replace('{::IMPORT_ACTIVE::}', '', $html);
                $html = str_replace('{::SETTINGS_ACTIVE::}', '', $html);
            break;
            case 'settings':
                $html = str_replace('{::SETTINGS_ACTIVE::}', ' class="active"', $html);
                $html = str_replace('{::SUPPLIER_ACTIVE::}', '', $html);
                $html = str_replace('{::CATEGORY_ACTIVE::}', '', $html);
                $html = str_replace('{::IMPORT_ACTIVE::}', '', $html);
                $html = str_replace('{::EXPORT_ACTIVE::}', '', $html);
            break;
            default:

        }

        die($html);
    }

    private function loadHeader($name)
    {
        $path = BASE_ROOT . 'headers/' . $name . '.php';

        if (!file_exists($path) || !is_file($path)) {
            return '';
        }

        if (filesize($path) < 1) {
            return '';
        }

        $fo = fopen($path, 'r');
        $data = fread($fo, filesize($path));
        fclose($fo);

        return $data;
    }
}
