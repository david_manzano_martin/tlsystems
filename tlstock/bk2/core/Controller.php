<?php

namespace Core;

class Controller
{
    public $gateway;

    public function __construct()
    {
        $this->initializeGateway();
    }


    private function initializeGateway()
    {
        $gatewayPath = BASE_ROOT . 'core/gateway/' . BASE_SLUG . '.php';

        if (!file_exists($gatewayPath) || !is_file($gatewayPath)) {
            throw new \Exception('Page gateway does not exists.');
        }
     

        require_once $gatewayPath;
        $gateway = 'Gateway\\' . ucfirst(BASE_SLUG);
        $this->gateway = new $gateway;
    }
}
