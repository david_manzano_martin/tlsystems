<?php

namespace Core;

class Gateway
{
    public $connect;
    private $insertId = 0;
    private $rows;

    public function __construct()
    {
        $this->initializeConnect();
    }

    private function initializeConnect()
    {
        $configPath = BASE_ROOT . 'core/__config__.php';

        if (!file_exists($configPath) || !is_file($configPath)) {
            throw new \Exception('Invalid database configuration. Config file does not exists.');
        }

        require_once $configPath;

        $this->connect = mysqli_connect($__DATABASE_HOSTNAME, $__DATABASE_USERNAME, $__DATABASE_PASSWORD, $__DATABASE_NAME, $__DATABASE_PORT);
        
        if (!$this->connect) {
            throw new \Exception('Unable to connect to database. ' . mysqli_connect_error());
        }
    }
    
    public function getErrorMessage()
    {
        return mysqli_error($this->connect);
    }
    
    public function multiquery($sql) 
    {
        $result = mysqli_multi_query($this->connect, $sql);
        $qType = $this->getQueryType($sql);
        $this->rows = 0;
        $this->insertId = 0;
        
        if (empty($qType)) {
            return false;
        }
        
        if (mysqli_affected_rows($this->connect) < 0) {
            return false;
        }
        
        $this->rows = @mysqli_num_rows($result);
        $this->insertId = mysqli_insert_id($this->connect);
        
        if ($this->rows < 1) {
            return array();
        }
        
        $rList = array();
        
        if ($this->rows == 1) {
            $r = mysqli_fetch_assoc($result);
            mysqli_free_result($result);
            
            array_push($rList, $r);
            
            return $rList;
        }
        
        while ($r = mysqli_fetch_assoc($result)) {
            array_push($rList, $r);
        }
        
        mysqli_free_result($result);
        unset($r);
        return $rList;
    }
    
    public function query($sql)
    {
        $result = mysqli_query($this->connect, $sql . '--');
        $qType = $this->getQueryType($sql);
        $this->rows = 0;
        $this->insertId = 0;
        
        if (empty($qType)) {
            return false;
        }
        
        if (mysqli_affected_rows($this->connect) < 0) {
            return false;
        }
        
        $this->rows = @mysqli_num_rows($result);
        $this->insertId = mysqli_insert_id($this->connect);
        
        if ($this->rows < 1) {
            return array();
        }
        
        $rList = array();
        
        if ($this->rows == 1) {
            $r = mysqli_fetch_assoc($result);
            mysqli_free_result($result);
            
            array_push($rList, $r);
            
            return $rList;
        }
        
        while ($r = mysqli_fetch_assoc($result)) {
            array_push($rList, $r);
        }
        
        mysqli_free_result($result);
        unset($r);
        return $rList;
    }

    public function escape_string($string)
    {
        return mysqli_escape_string($this->connect, $string);
    }

    public function inserted_id()
    {
        return $this->insertId;
    }
    
    /**
     * Get the query type.
     * @param String $sql
     * @return String
     */
    private function getQueryType($sql)
    {
        $query = strtolower($sql);
        
        if (preg_match('/^select/i', $query)) {
            return 'select';
        }
        
        if (preg_match('/^insert/i', $query)) {
            return 'insert';
        }
        
        if (preg_match('/^update/i', $query)) {
            return 'update';
        }
        
        if (preg_match('/^drop/i', $query)) {
            return 'drop';
        }
        
        if (preg_match('/^delete/i', $query)) {
            return 'delete';
        }
        
        if (preg_match('/^show/i', $query)) {
            return 'show';
        }
        
        if (preg_match('/^create/i', $query)) {
            return 'create';
        }
        
        if (preg_match('/^alter/i', $query)) {
            return 'alter';
        }
        
        return 'unknown';
    }
}
