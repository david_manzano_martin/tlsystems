<?php

class BasePath
{
    public function __construct()
    {
        $this->initializePath();
    }

    private function initializePath()
    {
        # DEFINE CONSTANTS
        if (!defined('BASE_ROOT')) {
            define('BASE_ROOT', str_replace('/core', '', __DIR__) . '/');
        }

        if (!defined('BASE_SLUG')) {
            // script name.
            $fileName = 'supplier';

            if (filter_input(INPUT_GET, 'url', FILTER_SANITIZE_STRING)) {
                $fName = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_STRING);

                if (!empty($fName)) {
                    $fName = explode('/', $fName);
                    $fName = $fName[0];
                }

                if (file_exists(BASE_ROOT . 'core/view/' . $fName . '.php')) {
                    $fileName =  $fName;
                }
            }

            define('BASE_SLUG', $fileName);
        }

        if (!defined('BASE_URI')) {
            define('BASE_URI', $this->siteURL());
        }
    }

    private function siteURL()
    {
        $phpSelf = '';
        
        if (isset($_SERVER['PHP_SELF'])) {
            $phpSelf = $_SERVER['PHP_SELF'];
        }
        
        $relDir = dirname($phpSelf);
        $rel = str_replace('app', '', $relDir); 
        $relTrim = rtrim($rel, '/');

        if ($relTrim == '/') {
            $relTrim = '';
        }

        $protocol = filter_input(INPUT_SERVER, 'HTTPS');

        if (isset($protocol)) {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }

        if (isset($_SERVER['HTTP_HOST'])) {
            $domain = $_SERVER['HTTP_HOST'];
        } else {
            $domain = '';
        }
        
        if (empty($domain)) {
            $protocol = '';
        }
        
        $domainName = $domain . $relTrim . '/';

        return $protocol.$domainName;
    }
}
    