<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once __DIR__ . '/BasePath.php';
require_once __DIR__ . '/View.php';
require_once __DIR__ . '/Controller.php';
require_once __DIR__ . '/Gateway.php';

# Initialize system.
new BasePath();
loadPage();

/**
 * Initialize requested page.
 * @return void
 */
function loadPage()
{
	die("dir") ;
	error_reporting(1);
	echo "enabled error reporting";	
    $viewPath = BASE_ROOT . 'core/view/' . BASE_SLUG . '.php';
    $gatewayPath = BASE_ROOT . 'core/gateway/' . BASE_SLUG . '.php';
    $controllerPath = BASE_ROOT . 'core/controller/' . BASE_SLUG . '.php';
    $utilsPath = BASE_ROOT . 'core/utils/';
    
    if (!file_exists($viewPath) || !is_file($viewPath)) {
        throw new Exception('View  missing for requested page FOR FILE ' . $viewPath);
    }

    if (!file_exists($gatewayPath) || !is_file($gatewayPath)) {
        throw new Exception('Gateway missing for requested page.');
    }

    if (!file_exists($controllerPath) || !is_file($controllerPath)) {
        throw new Exception('Controller missing for requested page.');
    }
    
    load($utilsPath, false, '\Utils\\');

    require_once $gatewayPath;
    require_once $controllerPath;
    require_once $viewPath;

    $className = '\Controller\\' . ucfirst(BASE_SLUG);
    $controller = new $className;

    $className = '\View\\' . ucfirst(BASE_SLUG);
    new $className($controller);
}

/**
* Load package libraries.
* @param String $absPath The absolute path to the library's directory.
* @param Boolean $initializeInstance Ether to initialize the class or not.
* @param String $namespace The namespace used by the class.
* @return void
*/
function load($absPath, $initializeInstance=false, $namespace='')
{
    if (empty($absPath) || !file_exists($absPath)) {
        throw new \Exception("Invalid autoload request. The path must be a directory different than root. Path provided: $absPath");
    }

    $fileList = loadLibFiles($absPath);

    if (count($fileList) < 1 || $fileList === false) {
        return;
    }

    for ($i=0; $i < count($fileList); $i++) {
        if (!file_exists($fileList[$i]['src'])) {
            continue;
        }

        include $fileList[$i]['src'];

        if (!$initializeInstance) {
            continue;
        }

        $className = $fileList[$i]['cname'];

        if (!class_exists($namespace . $className)) {
            continue;
        }

        $className = $namespace . $className;
        $className::getInstance();
    }
}

/**
* Load file list from requested directory.
* @param String $path The absolute path to the destination directory.
* @return bool:false|array
*/
function loadLibFiles($path)
{
	
    if (!file_exists($path) || empty($path) || !is_dir($path)) {
        return false;
    }

    $dir = opendir($path);

    if ($dir === false) {
        throw new Exception("Unable to open directory $path. Permission denied.");
    }

    $fileList = array();

    while (($file = readdir($dir)) !== false) {
        $ext = explode('.',$file);

        if (count($ext) != 2) {
            continue;
        }

        $className = $ext[0];

        if (strtolower($ext[1]) != 'php') {
            continue;
        }

        array_push($fileList, array(
                    'src' => $path . $file,
                    'cname' => $className
                ));
    }

    closedir($dir);

    return $fileList;
}
