<?php

namespace View;

use \Core\View;

class Categories extends View
{
    private $CONTROLLER;
    private $pageContent = '';

    public function __construct($controller)
    {
        $this->CONTROLLER = $controller;

        // get supplier page.
        $this->pageContent = $this->loadInc('inc.categoryMain');
        
        $this->generateCategoryForm();
        $this->generateCategoryManager();
        $this->renderPage();
    }
    
    /**
     * @return void
     */
    private function generateCategoryForm()
    {
        $this->pageContent = str_replace('{::CATEGORY_FORM_UI::}', $this->loadInc('inc.categoryForm'), $this->pageContent);
    }
    
    /**
     * @return void
     */
    private function generateCategoryManager()
    {
        $categoryData = $this->CONTROLLER->getCategoryData();
        
        if (count($categoryData) < 1 || $categoryData === false) {
            $html = '<div class="alert alert-info">There are no categories at the moment.</div>';
            $this->pageContent = str_replace('{::CATEGORY_MANAGER_LIST_UI::}', $html, $this->pageContent);
            return;
        }
                
        $html = '';
        $row = $this->loadInc('inc.categoryRow');
        
        for ($i = 0; $i < count($categoryData); $i++) {
            $html .= $row;
            $html = str_replace('{::CATEGORY_ID_VALUE::}', $categoryData[$i]['category_id'], $html);
            $html = str_replace('{::ORIGIN_CATEGORY_ID_VALUE::}', $categoryData[$i]['presto_shop_id'], $html);
            $html = str_replace('{::CATEGORY_NAME_VALUE::}', $categoryData[$i]['category_name'], $html);
        }
        
        $this->pageContent = str_replace('{::CATEGORY_MANAGER_LIST_UI::}', $html, $this->pageContent);
    }
    
    /**
     * @return void
     */
    private function renderPage()
    {
        $this->render($this->pageContent);
    }
}
