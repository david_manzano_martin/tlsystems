<?php
namespace View;

use \Core\View;

class Export extends View
{
    private $CONTROLLER;
    private $pageContent = '';

    public function __construct($controller)
    {
        $this->CONTROLLER = $controller;
        
        // get main page.
        $this->pageContent = $this->loadInc('inc.exportMain');
        $this->renderExportForm();
        $this->renderPage();
    }
    
    /**
     * @return void
     */
    private function renderExportForm()
    {
        $supplier = $this->CONTROLLER->getSupplier();
        
        if (count($supplier) < 1 || $supplier === false) {
            $html = '<div class="alert alert-info">There are no suppliers at the moment. Please add at least one supplier before import products.</div>';
            $this->pageContent = str_replace('{::EXPORT_FORM_UI::}', $html, $this->pageContent);
            return;
        }
        
        $this->pageContent = str_replace('{::EXPORT_FORM_UI::}', $this->loadInc('inc.exportForm'), $this->pageContent);
        $this->pageContent = str_replace('{::SUPPLIER_LIST::}', $this->generateSupplierList($supplier), $this->pageContent);  
    }
    
    /**
     * @param array $supplier
     * @return string
     */
    private function generateSupplierList($supplier) 
    {
        $html = '';
        
        for ($i = 0; $i < count($supplier); $i++) {
            $html .= '<option value="' . $supplier[$i]['supplier_id'] . '">' . $supplier[$i]['company_name'] . '</option>';
        }
        
        return $html;
    }
    
    /**
     * @return void
     */
    private function renderPage()
    {
        $this->render($this->pageContent);
    }
}