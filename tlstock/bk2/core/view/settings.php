<?php
namespace View;

use \Core\View;

class Settings extends View
{
    private $CONTROLLER;
    private $pageContent = '';

    public function __construct($controller)
    {
        $this->CONTROLLER = $controller;
        
        // get main page.
        $this->pageContent = $this->loadInc('inc.settingsMain');
        
        $this->generateSettingsForm();
        $this->generateSettingsManager();
        $this->renderPage();
    }
    
    /**
     * @return void
     */
    private function generateSettingsForm()
    {
        $this->pageContent = str_replace('{::SETTING_FORM_UI::}', $this->loadInc('inc.settingsForm'), $this->pageContent);
    }
    
    /**
     * @return void
     */
    private function generateSettingsManager()
    {
        $settings = $this->CONTROLLER->getSettings();
                
        if (count($settings) < 1 || $settings === false) {
            $html = '<div class="alert alert-info">There are no settings at the moment.</div>';
            $this->pageContent = str_replace('{::SETTINGS_MANAGER_LIST_UI::}', $html, $this->pageContent);
            return;
        }
                
        $html = '';
        $row = $this->loadInc('inc.settingsRow');
        
        for ($i = 0; $i < count($settings); $i++) {
            $html .= $row;
            $html = str_replace('{::ID_VALUE::}', $settings[$i]['setting_id'], $html);
            $html = str_replace('{::PRICE_VALUE::}', $settings[$i]['price'], $html);
            $html = str_replace('{::PERCENT_VALUE::}', $settings[$i]['percent'], $html);
        }
        
        $this->pageContent = str_replace('{::SETTINGS_MANAGER_LIST_UI::}', $html, $this->pageContent);
    }
    
    /**
     * @return void
     */
    private function renderPage()
    {
        $this->render($this->pageContent);
    }
}