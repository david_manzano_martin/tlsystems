<?php

namespace View;

use \Core\View;
use Core\Utils\CountryList;

class Supplier extends View
{
    private $CONTROLLER;
    private $pageContent = '';

    public function __construct($controller)
    {
        $this->CONTROLLER = $controller;

        // get supplier page.
        $this->pageContent = $this->loadInc('inc.supplierMain');
        
        $this->generateSupplierForm();
        $this->generateSupplierManager();
        $this->renderPage();
    }
    
    /**
     * @return void
     */
    private function generateSupplierForm()
    {
        $this->pageContent = str_replace('{::SUPPLIER_FORM_UI::}', $this->loadInc('inc.supplierForm'), $this->pageContent);
        $this->pageContent = str_replace('{::COUNTRY_LIST::}', $this->generateCountryList(''), $this->pageContent);
    }
    
    /**
     * @return void
     */
    private function generateSupplierManager()
    {
        $supplierData = $this->CONTROLLER->getSupplierData();
        
        if (count($supplierData) < 1 || $supplierData === false) {
            $html = '<div class="alert alert-info">There are no suppliers at the moment.</div>';
            $this->pageContent = str_replace('{::SUPPLIER_MANAGER_LIST_UI::}', $html, $this->pageContent);
            return;
        }
                
        $html = '';
        $row = $this->loadInc('inc.supplierRow');
        
        for ($i = 0; $i < count($supplierData); $i++) {
            $html .= $row;
            $html = str_replace('{::SUPPLIER_ID_VALUE::}', $supplierData[$i]['supplier_id'], $html);
            $html = str_replace('{::ORIGIN_ID_VALUE::}', $supplierData[$i]['supplier_origin_id'], $html);
            $html = str_replace('{::COMPANY_NAME_VALUE::}', $supplierData[$i]['company_name'], $html);
            $html = str_replace('{::ADDRESS_VALUE::}', $supplierData[$i]['address'], $html);
            $html = str_replace('{::CITY_VALUE::}', $supplierData[$i]['city'], $html);
            $html = str_replace('{::ZIP_VALUE::}', $supplierData[$i]['zip'], $html);
            $html = str_replace('{::CONTACT_VALUE::}', $supplierData[$i]['contact_name'], $html);
            $html = str_replace('{::EMAIL_VALUE::}', $supplierData[$i]['email'], $html);
            $html = str_replace('{::PHONE_VALUE::}', $supplierData[$i]['phone'], $html);
            $html = str_replace('{::FAX_VALUE::}', $supplierData[$i]['fax'], $html);
            $html = str_replace('{::DDI_VALUE::}', $supplierData[$i]['ddi'], $html);
            $html = str_replace('{::NOTES_VALUE::}', $supplierData[$i]['notes'], $html);
            $html = str_replace('{::COUNTRY_LIST::}', $this->generateCountryList($supplierData[$i]['country_iso']), $html);
        }
        
        $this->pageContent = str_replace('{::SUPPLIER_MANAGER_LIST_UI::}', $html, $this->pageContent);
    }
    
    /**
     * @param string $pIso
     * @return string
     */
    private function generateCountryList($pIso)
    {
        $langList = CountryList::countries();
        
        if (count($langList) < 1) {
            return '';
        }
        
        $html = '';
        foreach ($langList as $iso => $countryName) { 
            
            $selected = '';
            if (!empty($pIso) && strtolower($pIso) == strtolower($iso)) {
                $selected = "selected='selected'";
            }
            
            $html .= '<option ' . $selected . ' value="' . strtolower($iso) . '">' . $countryName . '</option>';
        }
        
        return $html;
    }
    
    /**
     * @return void
     */
    private function renderPage()
    {
        $this->render($this->pageContent);
    }
}
