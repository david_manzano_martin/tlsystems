<?php
namespace View;

use \Core\View;

class Import extends View
{
    private $CONTROLLER;
    private $pageContent = '';
    
    public function __construct($controller)
    {
        $this->CONTROLLER = $controller;
        
        // get main page.
        $this->pageContent = $this->loadInc('inc.importMain');
        $this->renderImportForm();
        $this->renderManagerForm();
        $this->renderPage();
    }
    
    /**
     * @return void
     */
    private function renderManagerForm()
    {
        $tmpProducts = $this->CONTROLLER->getTmpProducts();
        $cateogries = $this->CONTROLLER->getCategories();
        
        if (count($cateogries) < 1 || $cateogries === false) {
            $html = '<div class="alert alert-info">There are no categories at the moment. Please add at least one category before import products.</div>';
            $this->pageContent = str_replace('{::IMPORT_MANAGER::}', $html, $this->pageContent);
            return;
        }
        
        if (count($tmpProducts) < 1 || $tmpProducts === false) {
            $this->pageContent = str_replace('{::IMPORT_MANAGER::}', '', $this->pageContent);
            return;
        }
        
        $html = '';
        $item = $this->loadInc('inc.importRow');
        
        for ($i = 0; $i < count($tmpProducts); $i++) {
            $html .= $item;
            $html = str_replace('{::IMG_SRC::}', $tmpProducts[$i]['image'], $html);
            $html = str_replace('{::PROD_TITLE::}', base64_decode($tmpProducts[$i]['description']), $html);
            $html = str_replace('{::DESCRIPTION::}', $tmpProducts[$i]['description'], $html);
            $html = str_replace('{::PROD_CODE::}', $tmpProducts[$i]['product_code'], $html);
            $html = str_replace('{::PROD_PRICE::}', $tmpProducts[$i]['price'], $html);
            $html = str_replace('{::QUANTITY::}', $tmpProducts[$i]['quantity'], $html);
            $html = str_replace('{::SUPPLIER_NAME::}', $tmpProducts[$i]['company_name'], $html);
            $html = str_replace('{::SUPPLIER_ID::}', $tmpProducts[$i]['supplier_id'], $html);
            $html = str_replace('{::MANUF_CODE::}', $tmpProducts[$i]['manuf_code'], $html);
            $html = str_replace('{::DETAILS::}', $tmpProducts[$i]['details'], $html);
            $html = str_replace('{::WEIGHT::}', $tmpProducts[$i]['weight'], $html);
            $html = str_replace('{::CATEGORY_LIST::}', $this->generateCategoryList($cateogries), $html);
        }
        
        $this->pageContent = str_replace('{::IMPORT_MANAGER::}', $html, $this->pageContent);
    }
    
    /**
     * @param array $cateogries
     * @return string
     */
    private function generateCategoryList($cateogries)
    {
        $html = '';
        for ($i = 0; $i < count($cateogries); $i++) {
            $html .= '<option value="' . $cateogries[$i]['category_id'] . '">' . $cateogries[$i]['category_name'] . '</option>';
        }
        
        return $html;
    }
    
    /**
     * @return void
     */
    private function renderImportForm()
    {
        $supplier = $this->CONTROLLER->getSupplier();
        
        if (count($supplier) < 1 || $supplier === false) {
            $html = '<div class="alert alert-info">There are no suppliers at the moment. Please add at least one supplier before import products.</div>';
            $this->pageContent = str_replace('{::IMPORT_FORM_UI::}', $html, $this->pageContent);
            return;
        }
        
        $this->pageContent = str_replace('{::IMPORT_FORM_UI::}', $this->loadInc('inc.importForm'), $this->pageContent);
        $this->pageContent = str_replace('{::SUPPLIER_LIST::}', $this->generateSupplierList($supplier), $this->pageContent);        
    }
    
    /**
     * @param array $supplier
     * @return string
     */
    private function generateSupplierList($supplier) 
    {
        $html = '';
        
        for ($i = 0; $i < count($supplier); $i++) {
            $html .= '<option value="' . $supplier[$i]['supplier_id'] . '">' . $supplier[$i]['company_name'] . '</option>';
        }
        
        return $html;
    }
    
    /**
     * @return void
     */
    private function renderPage()
    {
        $this->render($this->pageContent);
    }
}