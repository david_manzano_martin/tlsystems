<?php
namespace View;

use \Core\View;

class SAMPLE extends View
{
    private $CONTROLLER;
    private $pageContent = '';

    public function __construct($controller)
    {
        $this->CONTROLLER = $controller;
        
        // get main page.
        $this->pageContent = $this->loadInc('inc.SAMPLEMain');
        $this->renderPage();
    }
    
    /**
     * @return void
     */
    private function renderPage()
    {
        $this->render($this->pageContent);
    }
}