<?php
namespace Controller;

use Core\Controller;

class Export extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @return boolean|array
     */
    public function getSupplier()
    {
        return $this->gateway->loadSupplier();
    }
}