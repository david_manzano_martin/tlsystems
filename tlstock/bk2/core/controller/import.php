<?php
namespace Controller;

use Core\Controller;
use Core\Utils\FileUpload;
use Core\Utils\RandomString;

class Import extends Controller
{   
    private $tmpProducts = [];
    
    public function __construct()
    {
        parent::__construct();
        $this->handleSaveForm();
        $this->handleDeleteForm();
        $this->handleImportForm();
        $this->loadTmpProducts();
    }
        
    /**
     * @return void
     */
    public function handleImportForm()
    {   
        if (!isset($_POST['importBtn'])) {
            return;
        }
        
        $supplier = filter_input(INPUT_POST, 'supplierInput', FILTER_SANITIZE_STRING);
        $template = filter_input(INPUT_POST, 'templateInput');
        
        if ($template != '1') {
            if ($template != '2') {
                return;
            }
        }
        
        if (empty($supplier)) {
            return;
        }
        
        if (!isset($_FILES['csvInput']['name']) ||
            empty($_FILES['csvInput']['name']) ||
            $_FILES['csvInput']['size'] <= 0) {
            return;
        }
        
        $csvFile = $this->uploadCsv($_FILES['csvInput']);
        
        if ($csvFile === false) {
            
            if (file_exists(BASE_ROOT . $csvFile)) {
                unlink(BASE_ROOT . $csvFile);
            }
            
            return;
        }
        
        $fo = fopen(BASE_ROOT . $csvFile, 'r');
        
        $result = [];
        while(($data = fgetcsv($fo, filesize(BASE_ROOT.$csvFile), ',')) !== false) {
            $result[] = $data;
        }
        
        fclose($fo);
        
        if (file_exists(BASE_ROOT . $csvFile)) {
            unlink(BASE_ROOT . $csvFile);
        }
                
        $this->gateway->checkProduct($result, $template, $supplier);
        header('Location: import');
        exit();
    }
    
    private function loadTmpProducts()
    {
        $this->tmpProducts = $this->gateway->loadTmpProducts();
    }
    
    /**
     * @param $_FILES $file
     * @return string|false
     */
    private function uploadCsv($file)
    {
        $extension = FileUpload::getFileExtension($file['name']);
        $fileName = RandomString::generate(15) . '.' . $extension;
        $path = __DIR__ . '/../../tmp_upload/';

        if (!$this->createDirectory($path)) {
            return false;
        }

        if (!move_uploaded_file($file['tmp_name'], $path . $fileName)) {
            return false;
        }
        
        return 'tmp_upload/' . $fileName;
    }
    
    /**
     * @param string $path
     * @return bool
     */
    private function createDirectory($path)
    {
        if (!empty($path) && file_exists($path)) {
            return true;
        }

        if (empty($path)) {
            return false;
        }

        if (!mkdir($path, 0775)) {
            return false;
        }

        $fo = fopen($path . 'index.html', 'w');
        fwrite($fo, '');
        fclose($fo);

        unset($fo);

        return true;
    }
    
    /**
     * @return boolean|array
     */
    public function getSupplier()
    {
        return $this->gateway->getSupplier();
    }
    
    /**
     * @return boolean|array
     */
    public function getCategories()
    {
        return $this->gateway->getCategories();
    }
    
    /**
     * @return boolean|array
     */
    public function getTmpProducts()
    {
       return $this->tmpProducts; 
    }
    
    /**
     * @return void
     */
    public function handleDeleteForm()
    {
        if (!isset($_POST['deleteBtn'])) {
            return;
        }
        
        $prodCode = filter_input(INPUT_POST, 'prodCodeHid', FILTER_SANITIZE_STRING);
        
        // remove from product tmp db
        $this->gateway->removeTmpProduct($prodCode);
    }
    
    /**
     * @return void
     */
    public function handleSaveForm()
    {
        if (!isset($_POST['saveBtn'])) {
            return;
        }
                
        $prodCode = filter_input(INPUT_POST, 'prodCodeHid', FILTER_SANITIZE_STRING);
        $manufCode = filter_input(INPUT_POST, 'manufCodeHid', FILTER_SANITIZE_STRING);
        $desc = filter_input(INPUT_POST, 'descriptionHid', FILTER_SANITIZE_STRING);
        $price = filter_input(INPUT_POST, 'priceHid');
        $quantity = filter_input(INPUT_POST, 'quantityHid');
        $details = filter_input(INPUT_POST, 'detailsHid', FILTER_SANITIZE_STRING);
        $image = filter_input(INPUT_POST, 'imageHid', FILTER_SANITIZE_STRING);
        $weight = filter_input(INPUT_POST, 'weightHid');
        $supplier = filter_input(INPUT_POST, 'supplierHid', FILTER_SANITIZE_STRING);
        $category = filter_input(INPUT_POST, 'catSelect', FILTER_SANITIZE_STRING);
                
        if (empty($category) || $category == '') {
            return;
        }
        
        $priceExcludeTaxes = $this->gateway->calculatePrice($price);
                
        // save to main product db
        $res = $this->gateway->saveMainProduct(
                $prodCode,
                $manufCode,
                $desc,
                $price,
                $quantity,
                $details,
                $image,
                $weight,
                $supplier,
                $priceExcludeTaxes,
                $category
                );
        
        if ($res === false) {
            return;
        }
        
        // remove from product tmp db
        $this->gateway->removeTmpProduct($prodCode);
    }
}