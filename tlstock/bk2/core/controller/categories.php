<?php
namespace Controller;

use Core\Controller;
use Core\Utils\RandomString;

class Categories extends Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->handleSaveForm();
        $this->handleUpdateForm();
        $this->handleRemoveForm();
    }
    
    /**
     * Save new category data
     * @return void
     */
    private function handleSaveForm()
    {
        if (!isset($_POST['saveBtn'])) {
            return;
        }
        
        $categoryId = RandomString::getUUID();
        $originIdInput = filter_input(INPUT_POST, 'originIdInput', FILTER_SANITIZE_STRING);
        $categoryName = filter_input(INPUT_POST, 'categoryNameInput', FILTER_SANITIZE_STRING);
        
        if (empty($categoryId) || empty($originIdInput) || empty($categoryName)) {
            return;
        }
         
        $this->gateway->saveCategory($categoryId, $originIdInput, $categoryName);
    }
    
    /**
     * Update category data
     * @return void
     */
    private function handleUpdateForm()
    {
        if (!isset($_POST['changeBtn'])) {
            return;
        }
        
        $categoryId = filter_input(INPUT_POST, 'categoryIdHid', FILTER_SANITIZE_STRING);
        $originIdInput = filter_input(INPUT_POST, 'originIdInput', FILTER_SANITIZE_STRING);
        $categoryName = filter_input(INPUT_POST, 'categoryNameInput', FILTER_SANITIZE_STRING);
        
        if (empty($categoryId) || empty($originIdInput) || empty($categoryName)) {
            return;
        }
         
        $this->gateway->updateCategory($categoryId, $originIdInput, $categoryName);
    }
    
    /**
     * Remove cateory data
     * @return void
     */
    private function handleRemoveForm()
    {
        if (!isset($_POST['deleteBtn'])) {
            return;
        }
        
        $categoryId = filter_input(INPUT_POST, 'categoryIdHid', FILTER_SANITIZE_STRING);
        
        if (empty($categoryId)) {
            return;
        }
        
        $this->gateway->removeCategory($categoryId);
    }
    
    /**
     * Get category data
     * @return array
     */
    public function getCategoryData()
    {
        return $this->gateway->loadCategoryData();
    }
}
