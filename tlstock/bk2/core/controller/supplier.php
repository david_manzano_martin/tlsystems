<?php
namespace Controller;

use Core\Controller;
use Core\Utils\RandomString;
use Core\Utils\CountryList;

class Supplier extends Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->handleSaveForm();
        $this->handleUpdateForm();
        $this->handleRemoveForm();
    }
    
    /**
     * Save new supplier data
     * @return void
     */
    private function handleSaveForm()
    {
        if (!isset($_POST['saveBtn'])) {
            return;
        }
        
        $supplierId = RandomString::getUUID();
        $originSupplierId = filter_input(INPUT_POST, 'originIdInput', FILTER_SANITIZE_STRING);
        $companyName = filter_input(INPUT_POST, 'companyNameInput', FILTER_SANITIZE_STRING);
        $address = filter_input(INPUT_POST, 'addressInput', FILTER_SANITIZE_STRING);
        $city = filter_input(INPUT_POST, 'cityInput', FILTER_SANITIZE_STRING);
        $zip = filter_input(INPUT_POST, 'zipInput', FILTER_SANITIZE_STRING);
        $countryIso = filter_input(INPUT_POST, 'countryInput', FILTER_SANITIZE_STRING);
        $country = CountryList::getCountryNameByISO($countryIso);
        $contact = filter_input(INPUT_POST, 'contactNameInput', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'emailInput', FILTER_SANITIZE_EMAIL);
        $phone = filter_input(INPUT_POST, 'phoneInput', FILTER_SANITIZE_STRING);
        $fax = filter_input(INPUT_POST, 'faxInput', FILTER_SANITIZE_STRING);
        $ddi = filter_input(INPUT_POST, 'ddiInput', FILTER_SANITIZE_STRING);
        $notes = filter_input(INPUT_POST, 'notesInput', FILTER_SANITIZE_STRING);
        
        if (empty($supplierId) || empty($originSupplierId) || empty($companyName)) {
            return;
        }
         
        $this->gateway->saveSupplier($supplierId, $originSupplierId, $companyName, $address, $city,
                $zip, $countryIso, $country, $contact, $email, $phone, $fax, $ddi, $notes);
    }
    
    /**
     * Update supplier data
     * @return void
     */
    private function handleUpdateForm()
    {
        if (!isset($_POST['changeBtn'])) {
            return;
        }
        
        $supplierId = filter_input(INPUT_POST, 'supplierId', FILTER_SANITIZE_STRING);
        $originSupplierId = filter_input(INPUT_POST, 'originIdInput', FILTER_SANITIZE_STRING);
        $companyName = filter_input(INPUT_POST, 'companyNameInput', FILTER_SANITIZE_STRING);
        $address = filter_input(INPUT_POST, 'addressInput', FILTER_SANITIZE_STRING);
        $city = filter_input(INPUT_POST, 'cityInput', FILTER_SANITIZE_STRING);
        $zip = filter_input(INPUT_POST, 'zipInput', FILTER_SANITIZE_STRING);
        $countryIso = filter_input(INPUT_POST, 'countryInput', FILTER_SANITIZE_STRING);
        $country = CountryList::getCountryNameByISO($countryIso);
        $contact = filter_input(INPUT_POST, 'contactNameInput', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'emailInput', FILTER_SANITIZE_EMAIL);
        $phone = filter_input(INPUT_POST, 'phoneInput', FILTER_SANITIZE_STRING);
        $fax = filter_input(INPUT_POST, 'faxInput', FILTER_SANITIZE_STRING);
        $ddi = filter_input(INPUT_POST, 'ddiInput', FILTER_SANITIZE_STRING);
        $notes = filter_input(INPUT_POST, 'notesInput', FILTER_SANITIZE_STRING);
        
        if (empty($supplierId) || empty($originSupplierId) || empty($companyName)) {
            return;
        }
         
        $this->gateway->updateSupplier($supplierId, $originSupplierId, $companyName, $address, $city,
                $zip, $countryIso, $country, $contact, $email, $phone, $fax, $ddi, $notes);
        
    }
    
    /**
     * Remove supplier data
     * @return void
     */
    private function handleRemoveForm()
    {
        if (!isset($_POST['deleteBtn'])) {
            return;
        }
        
        $supplierId = filter_input(INPUT_POST, 'supplierId', FILTER_SANITIZE_STRING);
        
        if (empty($supplierId)) {
            return;
        }
        
        $this->gateway->removeSupplier($supplierId);
    }
    
    /**
     * Get supplierData
     * @return array
     */
    public function getSupplierData()
    {
        return $this->gateway->loadSupplierData();
    }
}
