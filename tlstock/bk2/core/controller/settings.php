<?php
namespace Controller;

use Core\Controller;

class Settings extends Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->handleSaveForm();
        $this->handleUpdateForm();
        $this->handleRemoveForm();
    }
    
    /**
     * @return void
     */
    private function handleUpdateForm()
    {
        if (!isset($_POST['changeBtn'])) {
            return;
        }
                
        $id = filter_input(INPUT_POST, 'idHid', FILTER_SANITIZE_NUMBER_INT);
        $price = filter_input(INPUT_POST, 'priceInput');
        $percent = filter_input(INPUT_POST, 'percentInput');
        
        if (empty($id) || empty($percent) || empty($price)) {
            return;
        }
        
        $this->gateway->updateSetting($id, $price, $percent);
    }
    
    /**
     * @return void
     */
    private function handleRemoveForm()
    {
        if (!isset($_POST['deleteBtn'])) {
            return;
        }
        
        $id = filter_input(INPUT_POST, 'idHid', FILTER_SANITIZE_NUMBER_INT);
        
        if (empty($id)) {
            return;
        }
        
        $this->gateway->removeSetting($id);
    }
    
    /**
     * @void
     */
    private function handleSaveForm()
    {
        if (!isset($_POST['saveBtn'])) {
            return;
        }
        
        $price = filter_input(INPUT_POST, 'priceInput');
        $percent = filter_input(INPUT_POST, 'percentInput');
        
        if (empty($price) || empty($percent)) {
            return;
        }
        
        $this->gateway->saveSetting($price, $percent);
    }
    
    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->gateway->loadSettings();
    }
}