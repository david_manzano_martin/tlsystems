<?php
namespace Gateway;

use Core\Gateway;

class Categories extends Gateway
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @param string $categoryId
     * @param string $originIdInput
     * @param string $categoryName
     * @return boolean
     */
    public function saveCategory($categoryId, $originIdInput, $categoryName)
    {
        $categoryId = $this->escape_string($categoryId);
        $originIdInput = $this->escape_string($originIdInput);
        $categoryName = $this->escape_string($categoryName);
        
        $query = "  INSERT INTO `category` (`category_id`, `presto_shop_id`, `category_name`) 
                    VALUES ('$categoryId', '$originIdInput', '$categoryName')";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * @param string $categoryId
     * @param string $originIdInput
     * @param string $categoryName
     * @return boolean
     */
    public function updateCategory($categoryId, $originIdInput, $categoryName)
    {
        $categoryId = $this->escape_string($categoryId);
        $originIdInput = $this->escape_string($originIdInput);
        $categoryName = $this->escape_string($categoryName);
        
        $query = "  UPDATE `category` SET `presto_shop_id`='$originIdInput', `category_name`='$categoryName' WHERE `category_id`='$categoryId' LIMIT 1;";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    }
    
    
    /**
     * @param type $categoryId
     * @return boolean
     */
    public function removeCategory($categoryId)
    {
        $categoryId = $this->escape_string($categoryId);
        
        $query = "  DELETE FROM `category` WHERE `category_id` = '$categoryId' LIMIT 1;";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Get category data
     * @return boolean|array
     */
    public function loadCategoryData()
    {
        $query = "  SELECT * FROM `category` ORDER BY `category_name`";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return $sql;
    }
}
