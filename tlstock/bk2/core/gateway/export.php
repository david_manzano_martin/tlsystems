<?php
namespace Gateway;

use Core\Gateway;

class Export extends Gateway
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Load supplier data products
     * @param string $supplierId
     * @return boolean|array
     */
    public function loadSupplierData($supplierId) 
    {
        $query = "  SELECT * 
                    FROM 
                        `products` AS p
                    LEFT JOIN
                        `supplier` AS s
                    ON
                        s.`supplier_id`=p.`supplier_id`
                    LEFT JOIN
                        `category` AS c
                    ON
                        c.`category_id`=p.`category_id`
                    WHERE p.`supplier_id`='$supplierId'";
                
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return $sql;
    }
    
    /**
     * @return boolean|array
     */
    public function loadSupplier()
    {
        $query = "  SELECT * FROM `supplier` ORDER BY `company_name` ASC";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return $sql;
    }
}