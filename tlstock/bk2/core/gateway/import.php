<?php
namespace Gateway;

use Core\Gateway;
use Core\Utils\RandomString;

class Import extends Gateway
{
    private $settings = [];
    private $suppliers = [];
    private $categories = [];
    
    public function __construct()
    {
        parent::__construct();
        $this->loadSettings();
        $this->loadSupplier();
        $this->loadCategories();
    }
    
    public function getSupplier()
    {
        return $this->suppliers;
    }
    
    public function getCategories()
    {
        return $this->categories;
    }
    
    /**
     * @return array
     */
    public function loadSettings()
    {
        $query = "  SELECT * FROM `settings` ORDER BY `price` ASC";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return [];
        }
        
        $this->settings = $sql;
    }
    
    /**
     * @return boolean|array
     */
    public function loadTmpProducts()
    {
        $query = "  SELECT * FROM `products_tmp` AS p LEFT JOIN `supplier` AS s ON s.`supplier_id`=p.`supplier_id` ORDER BY `company_name` ASC LIMIT 100;";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return $sql;
    }


    /**
     * @return boolean|array
     */
    private function loadSupplier()
    {
        $query = "  SELECT * FROM `supplier` ORDER BY `company_name` ASC";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        $this->suppliers = $sql;
    }
    
    /**
     * @return boolean|array
     */
    private function loadCategories()
    {
        $query = "  SELECT * FROM `category` ORDER BY `category_name` ASC";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        $this->categories = $sql;
    }
    
    /**
     * Calculate price based on settings
     * @param float $price
     * @return float
     */
    public function calculatePrice($price)
    {
        if (count($this->settings) < 1) {
            return $price;
        }
        
        $costsCalc = $price;
        for ($i = 0; $i < count($this->settings); $i++) {
            if ($price <= $this->settings[$i]['price']) {
                $costs = ($price * $this->settings[$i]['percent']) / 100;
                $costsCalc = $price + $costs;
                break;
            }
        }
        
        return $costsCalc;
    }
    
    public function checkProduct($data, $template, $supplier)
    {   
        $insertProductTmpQuery = ' INSERT INTO `products_tmp` (
                                    `product_code`,
                                    `manuf_code`,
                                    `description`,
                                    `price`,
                                    `quantity`,
                                    `details`,
                                    `image`,
                                    `weight`,
                                    `supplier_id`
                              ) VALUES ';
        $updateProductQuery = '';
        $updateProductTmpQuery = '';
        $insertProductTmp = false;
            
        for ($i = 1; $i < count($data); $i++) {
            if ($template == '1') {
                // spire
                $productCode = $data[$i][0];
                $manufCode = $data[$i][1];
                $desc = base64_encode($data[$i][4]);
                $price = $data[$i][5];
                $quantity = $data[$i][6];
                $details = base64_encode($data[$i][7] . " " . $data[$i][8]);
                $image = $data[$i][11];
                $weight = $data[$i][12];
                $priceExcludeTaxes = $this->calculatePrice($price);
            } else {
                // tagetprice
                $productCode = $data[$i][14];
                $manufCode = $data[$i][10];
                $desc = base64_encode($data[$i][6]);
                $price = $data[$i][4];
                $quantity = $data[$i][13];
                $details = base64_encode($data[$i][5]);
                $image = $data[$i][8];
                $weight = $data[$i][15];
                $priceExcludeTaxes = $this->calculatePrice($price);
            }
            
            $query = "SELECT * FROM `products` WHERE `product_code`='$productCode' LIMIT 1;";
        
            $sql = $this->query($query);

            if ($sql === false) {
                return false;
            }

            if (count($sql) < 1) {
                // if not exists in products, check if exists in tmp table
                $query = "SELECT * FROM `products_tmp` WHERE `product_code`='$productCode' LIMIT 1;";
        
                $sql = $this->query($query);

                if ($sql === false) {
                    return false;
                }
                
                if (count($sql) < 1) {
                    // insert into tmp table if not exists in tmp table
                    $insertProductTmpQuery .= "(  
                                '$productCode',
                                '$manufCode',
                                '$desc',
                                '$price',
                                '$quantity',
                                '$details',
                                '$image',
                                '$weight',
                                '$supplier'
                            ),";
                    $insertProductTmp = true;
                } else {
                    // update in tmp if exists
                    $updateProductTmpQuery .= "UPDATE `products_tmp` SET 
                                `manuf_code` = '$manufCode',
                                `description` = '$desc',
                                `price` = '$price',
                                `quantity` = '$quantity',
                                `details` = '$details',
                                `image` = '$image',
                                `weight` = '$weight',
                                `supplier_id` = '$supplier'
                                WHERE `product_code`='$productCode'; ";
                }
            } else {
                // if exists in products, update products table
                $updateProductQuery .= "UPDATE `products` SET
                                `manuf_code` = '$manufCode',
                                `description` = '$desc',
                                `price` = '$price',
                                `quantity` = '$quantity',
                                `details` = '$details',
                                `image` = '$image',
                                `weight` = '$weight',
                                `supplier_id` = '$supplier',
                                `price_exclude_taxes`='$priceExcludeTaxes'
                                WHERE `product_code`='$productCode'; ";
            }
        }
           
        if ($insertProductTmp) {
            $insertProductTmpQuery = rtrim($insertProductTmpQuery, ',');
            
            $sql = $this->query($insertProductTmpQuery);
                    
            if ($sql === false) {
                return false;
            }
        }
        
        if (!empty($updateProductQuery)) {
            $sql = $this->multiquery($updateProductQuery);
                        
            if ($sql === false) {
                return false;
            }
        }
        
        if (!empty($updateProductTmpQuery)) {
        
            $result = $this->multiquery($updateProductTmpQuery);
            
            if ($result === false) {
                return false;
            }
        }
        
        return true;
    }
        
    /**
     * Insert to main product or update if already exists
     * @param string $prodCode
     * @param string $manufCode
     * @param string $desc
     * @param string $price
     * @param string $quantity
     * @param string $details
     * @param string $image
     * @param string $weight
     * @param string $supplier
     * @param string $priceExcludeTaxes
     * @param string $category
     * @return boolean
     */
    public function saveMainProduct(
                $prodCode,
                $manufCode,
                $desc,
                $price,
                $quantity,
                $details,
                $image,
                $weight,
                $supplier,
                $priceExcludeTaxes,
                $category
                )
    {
        
        $query = "  INSERT INTO `products` (
                        `product_code`,
                        `manuf_code`,
                        `description`,
                        `price`,
                        `quantity`,
                        `details`,
                        `image`,
                        `weight`,
                        `supplier_id`,
                        `price_exclude_taxes`,
                        `category_id`
                    ) VALUES (
                        '$prodCode',
                        '$manufCode',
                        '$desc',
                        '$price',
                        '$quantity',
                        '$details',
                        '$image',
                        '$weight',
                        '$supplier',
                        '$priceExcludeTaxes',
                        '$category'
                    )";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    }
    
    
    /**
     * Remove product from tmp table
     * @param string $prodCode
     * @return boolean
     */
    public function removeTmpProduct($prodCode)
    {
        $query = "DELETE FROM `products_tmp` WHERE `product_code`='$prodCode' LIMIT 1;";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    } 
}