<?php
namespace Gateway;

use Core\Gateway;

class Supplier extends Gateway
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Get supplier data
     * @return boolean|array
     */
    public function loadSupplierData()
    {
        $query = "  SELECT * FROM `supplier` ORDER BY `company_name` ASC";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return $sql;
    }
    
    /**
     * Save new supplier data
     * @param string $supplierId
     * @param string $originSupplierId
     * @param string $companyName
     * @param string $address
     * @param string $city
     * @param string $zip
     * @param string $countryIso
     * @param string $country
     * @param string $contact
     * @param string $email
     * @param string $phone
     * @param string $fax
     * @param string $ddi
     * @param string $notes
     * @return boolean
     */
    public function saveSupplier($supplierId, $originSupplierId, $companyName, $address, $city,
                $zip, $countryIso, $country, $contact, $email, $phone, $fax, $ddi, $notes)
    {
        $originSupplierId = $this->escape_string($originSupplierId);
        $companyName = $this->escape_string($companyName);
        $address = $this->escape_string($address);
        $city = $this->escape_string($city);
        $zip = $this->escape_string($zip);
        $countryIso = $this->escape_string($countryIso);
        $country = $this->escape_string($country);
        $contact = $this->escape_string($contact);
        $email = $this->escape_string($email);
        $phone = $this->escape_string($phone);
        $fax = $this->escape_string($fax);
        $ddi = $this->escape_string($ddi);
        $notes = $this->escape_string($notes);
        
        $query = "  INSERT INTO `supplier`
                        (`supplier_id`, 
                        `supplier_origin_id`,
                        `company_name`,
                        `address`,
                        `city`,
                        `zip`,
                        `country_iso`,
                        `country`,
                        `contact_name`,
                        `email`,
                        `phone`,
                        `fax`,
                        `ddi`,
                        `notes`) VALUES 
                        ('$supplierId',
                        '$originSupplierId',
                        '$companyName',
                        '$address',
                        '$city',
                        '$zip',
                        '$countryIso',
                        '$country',
                        '$contact',
                        '$email',
                        '$phone',
                        '$fax',
                        '$ddi',
                        '$notes')";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Update supplier data
     * @param string $supplierId
     * @param string $originSupplierId
     * @param string $companyName
     * @param string $address
     * @param string $city
     * @param string $zip
     * @param string $countryIso
     * @param string $country
     * @param string $contact
     * @param string $email
     * @param string $phone
     * @param string $fax
     * @param string $ddi
     * @param string $notes
     * @return boolean
     */
    public function updateSupplier($supplierId, $originSupplierId, $companyName, $address, $city,
                $zip, $countryIso, $country, $contact, $email, $phone, $fax, $ddi, $notes) 
    {
        $originSupplierId = $this->escape_string($originSupplierId);
        $companyName = $this->escape_string($companyName);
        $address = $this->escape_string($address);
        $city = $this->escape_string($city);
        $zip = $this->escape_string($zip);
        $countryIso = $this->escape_string($countryIso);
        $country = $this->escape_string($country);
        $contact = $this->escape_string($contact);
        $email = $this->escape_string($email);
        $phone = $this->escape_string($phone);
        $fax = $this->escape_string($fax);
        $ddi = $this->escape_string($ddi);
        $notes = $this->escape_string($notes);
        
        $query = "  UPDATE `supplier` 
                    SET 
                        `supplier_origin_id`='$originSupplierId',
                        `company_name`='$companyName',
                        `address`='$address',
                        `city`='$city',
                        `zip`='$zip',
                        `country_iso`='$countryIso',
                        `country`='$country',
                        `contact_name`='$contact',
                        `email`='$email',
                        `phone`='$phone',
                        `fax`='$fax',
                        `ddi`='$ddi',
                        `notes`='$notes'
                    WHERE
                        `supplier_id`='$supplierId' LIMIT 1;
                 ";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Remove supplier
     * @param string $supplierId
     * @return boolean
     */
    public function removeSupplier($supplierId)
    {
        $query = "  DELETE FROM `supplier` WHERE `supplier_id`='$supplierId' LIMIT 1;";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    }
}
