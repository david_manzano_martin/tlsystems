<?php
namespace Gateway;

use Core\Gateway;

class Settings extends Gateway
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Update setting
     * @param int $id
     * @param float $price
     * @param float $percent
     * @return boolen
     */
    public function updateSetting($id, $price, $percent)
    {
        $query = "  UPDATE `settings` SET `price`='$price', `percent`='$percent' WHERE `setting_id`='$id' LIMIT 1;";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Remove setting
     * @param int $id
     * @return boolean
     */
    public function removeSetting($id)
    {
        $query = "  DELETE FROM `settings` WHERE `setting_id`='$id' LIMIT 1;";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Save setting
     * @param float $price
     * @param float $percent
     * @return boolean
     */
    public function saveSetting($price, $percent)
    {
        $query = "  INSERT INTO `settings` (`price`, `percent`) VALUES ('$price', '$percent')";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Load settings data
     * @return boolean|array
     */
    public function loadSettings()
    {
        $query = "  SELECT * FROM `settings` ORDER BY `price` ASC";
        
        $sql = $this->query($query);
        
        if ($sql === false) {
            return false;
        }
        
        return $sql;
    }
}