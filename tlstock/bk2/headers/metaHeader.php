<head>
    <title>Manager</title>
    <script>
        var basePath = '{::BASE_URI::}';
    </script>
    <script src="{::BASE_URI::}media/3rdparty/jquery.min.js"></script>
    <script src="{::BASE_URI::}media/3rdparty/bootstrap/js/bootstrap.min.js"></script>
    <script src="{::BASE_URI::}media/js/main.js"></script>
    <link rel="stylesheet" type="text/css" href="{::BASE_URI::}media/3rdparty/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{::BASE_URI::}media/3rdparty/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="{::BASE_URI::}media/3rdparty/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{::BASE_URI::}media/css/main.css">
</head>