    <ul class="menu-wrap">
        <li{::SUPPLIER_ACTIVE::}>
            <a href="{::BASE_URI::}supplier">
                <span class="fa fa-users"></span>
                Supplier
            </a>
        </li>
        <li{::CATEGORY_ACTIVE::}>
            <a href="{::BASE_URI::}categories">
                <span class="fa fa-shopping-basket"></span>
                Categories
            </a>
        </li>
        <li{::IMPORT_ACTIVE::}>
            <a href="{::BASE_URI::}import">
                <span class="fa fa-upload"></span>
                Import stock
            </a>
        </li>
        <li{::EXPORT_ACTIVE::}>
            <a href="{::BASE_URI::}export">
                <span class="fa fa-download"></span>
                Export
            </a>
        </li>
        <li{::SETTINGS_ACTIVE::}>
            <a href="{::BASE_URI::}settings">
                <span class="fa fa-cog"></span>
                Settings
            </a>
        </li>
    </ul>