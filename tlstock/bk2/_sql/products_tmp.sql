CREATE TABLE `products_tmp` (
  `product_code` varchar(255) NOT NULL COMMENT 'Product number / StockCode',
  `manuf_code` varchar(255) NOT NULL COMMENT 'Manuf. number / ManuCode',
  `description` text NOT NULL COMMENT 'Description / DescShort',
  `price` decimal(16,2) NOT NULL COMMENT 'Cost / Price',
  `quantity` int(11) NOT NULL COMMENT 'Stock lvl / Stock',
  `details` text NOT NULL COMMENT 'Product details + Specs. / DescLong',
  `image` varchar(255) NOT NULL COMMENT 'Main image / ProdImage',
  `weight` varchar(255) NOT NULL COMMENT 'Weight / ProdWeight',
  `supplier_id` VARCHAR(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `products_tmp`
  ADD PRIMARY KEY (`product_code`),
  ADD INDEX (`supplier_id`);