CREATE TABLE `category` (
  `category_id` varchar(60) NOT NULL,
  `presto_shop_id` varchar(255) NOT NULL,
  `category_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);