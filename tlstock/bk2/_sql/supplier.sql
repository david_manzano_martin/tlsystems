CREATE TABLE `supplier` (
  `supplier_id` varchar(60) NOT NULL,
  `supplier_origin_id` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(30) NOT NULL,
  `country_iso` VARCHAR(2) NOT NULL,
  `country` varchar(255) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `fax` varchar(60) NOT NULL,
  `ddi` varchar(60) NOT NULL COMMENT 'direct dial - phone format',
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`);
