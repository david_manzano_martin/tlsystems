CREATE TABLE `settings` (
  `setting_id` int(11) NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `percent` decimal(16,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

ALTER TABLE `settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT;