<?php

$supplierId = filter_input(INPUT_GET, 'supplierInput', FILTER_SANITIZE_STRING);

if (empty($supplierId) || $supplierId == '') {
    return;
}

require_once 'core/BasePath.php';
require_once 'core/Gateway.php';
require_once 'core/gateway/export.php';
require_once 'core/utils/Slugfy.php';

new BasePath();
$gateway = new Gateway\Export();

$data = $gateway->loadSupplierData($supplierId);

if (count($data) < 1) {
    header('Location: export');
    exit();
}

$path = __DIR__ . '/export_download/';

if (!createDirectory($path)) {
    return false;
}

$fname = 'tmp_export_' . date('y-m-d_H_i_s') . '_' . \Core\Utils\Slugfy::slugify($data[0]['company_name']) . '.csv';
$filepath = __DIR__ . '/export_download/' . $fname;
$fo = fopen($filepath, 'w');

$fileInput = "Product ID|Active (0/1)|Name *|Categories (x,y,z...)|Price tax included|Tax rules ID|Wholesale price|On sale (0/1)|Discount amount|Discount percent|Discount from (yyyy-mm-dd)|Discount to (yyyy-mm-dd)|Reference #|Supplier reference #|Supplier|Manufacturer|EAN13|UPC|Ecotax|Width|Height|Depth|Weight|Quantity|Minimal quantity|Low stock level|Visibility|Additional shipping cost|Unity|Unit price|Short description|Description|Tags (x,y,z...)|Meta title|Meta keywords|Meta description|URL rewritten|Text when in stock|Text when backorder allowed|Available for order (0 = No, 1 = Yes)|Product available date|Product creation date|Show price (0 = No, 1 = Yes)|Image alt texts (x,y,z...)|Delete existing images (0 = No, 1 = Yes)|Feature(Name:Value:Position)|Available online only (0 = No, 1 = Yes)|Condition|Customizable (0 = No, 1 = Yes)|Uploadable files (0 = No, 1 = Yes)|Text fields (0 = No, 1 = Yes)|Out of stock|ID / Name of shop|Advanced stock management|Depends On Stock|Warehouse\r\n";

for ($i = 0; $i < count($data); $i++) {
    $fileInput .= $data[$i]['product_code'] . '|||';
    $fileInput .= $data[$i]['category_name'] . '|';
    $fileInput .= $data[$i]['price_exclude_taxes'] . '|||||||||';
    $fileInput .= $data[$i]['supplier_origin_id'] . '|';
    $fileInput .= $data[$i]['company_name'] . '|';
    $fileInput .= $data[$i]['manuf_code'] . '|||||||';
    $fileInput .= $data[$i]['weight'] . '|';
    $fileInput .= $data[$i]['quantity'] . '||||||';
    $fileInput .= $data[$i]['price_exclude_taxes'] . '|';
    $fileInput .= base64_decode($data[$i]['description']) . '|';
    $fileInput .= base64_decode($data[$i]['details']) . '||||||||||||';
    $fileInput .= $data[$i]['image'] . "|||||||||||||\r\n";
}

fwrite($fo, $fileInput);
fclose($fo);

header('Content-Type: text/comma-separated-values');
header('Content-Disposition: attachment; filename="' . basename($fname) . '"');
header('Content-Length: ' . filesize($filepath));
readfile($filepath);

/**
 * @param string $path
 * @return bool
 */
function createDirectory($path)
{
    if (!empty($path) && file_exists($path)) {
        return true;
    }

    if (empty($path)) {
        return false;
    }

    if (!mkdir($path, 0775)) {
        return false;
    }

    $fo = fopen($path . 'index.html', 'w');
    fwrite($fo, '');
    fclose($fo);

    unset($fo);

    return true;
}
