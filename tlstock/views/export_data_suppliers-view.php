<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<div class="container text-center">
	<div class="row">
		<div class="col-12">
			<h1>Export data</h1>
		</div>
		<form class="col-12" method="POST" action="?option=export_data_supplier">
			<div class="col-12">
				<select class="custom-select" name="supplierId" id="supplierId">
                  	<?php
                     	for($i=0; $i<=count($suppliersArray)-1; $i++) {
                     
                     		?>
                  	<option value="<?= $suppliersArray[$i]->getSupplier_origin_id() ?>"><?= $suppliersArray[$i]->getCompany_name() ?></option>
               	   <?php
            	         }
                     
                     ?>   
         	      </select>
			</div>
			<div class="col-12">
				<button class="btn btn-primary" >Export</button>
			</div>
		</form>
	</div>
</div>