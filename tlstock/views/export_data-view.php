<?php require(__DIR__ . "/partials/header-partial-view.php") ?>

<div class="container">
	<div class="row text-center">
		<div class="col-12">
			<h1>Export data</h1>
		</div>
		<div class="col-12">
			<button type="button" class="btn btn-primary">Export</button>
		</div>
	</div>
</div>