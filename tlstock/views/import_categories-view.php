

<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<div class="container">
   <div class="row text-center">
      <div class="col-12">
         <h1>Categories</h1>
      </div>
   </div>
   <div class="row justify-content-center">
      <?php
      
      	for($k=0; $k<=count($categoriesArray)-1; $k++){
				require(__DIR__ . "/partials/categories-partial-view.php");
      	}
      
      ?>
      <div class="col-12" style="margin:10px">
            <div class="row">
               <div class="col-6 text-right">
                  <button id="buttonBefore" type="button" class="btn btn-info"><--</button>
               </div>
               <div class="col-6 text-left">
                  <button id="buttonNext" type="button" class="btn btn-info">--></button>
               </div>
               <div class="col-12 text-center">
						<?php
							require(__DIR__ . "/partials/pagesNumber-partial-view.php");
						?>
					</div>
            </div>
         </div>
   </div>
</div>

