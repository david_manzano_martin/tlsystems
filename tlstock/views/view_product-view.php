

<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<div class="container">
   <form action="?option=bugProducts" method="POST">
   <input type="hidden" value="<?= $product->getProductName() ?>" name="name">
   <input type="hidden" value="<?= $product->getEanNumber() ?>" name="EanNumber">
      <div class="row text-center">
         <div class="col-12">
            <h1 id="productName"><?= $product->getProductName() ?></h1> <h6>(<?= $product->getEanNumber() ?>)</h6>
         </div>
      </div>
      <div class="row text-center">
         <div class="col-12">
            <img src="<?= $product->getImageUrl() ?>" alt="">
         </div>
      </div>
      <div class="row">
         <div class="col-12 col-lg-8">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Category</label>
               </div>
               <select class="custom-select" name="categoriesId" id="categoriesId">
                  <?php
                     for($i=0; $i<=count($categoriesArray)-1; $i++) {
                     	?>
                  <option value="<?= $categoriesArray[$i]->getCategory_id() ?>" <?= checkSelected($categoriesArray[$i]->getCategory_id(), $product->getCategoriesId()) ?> ><?= $categoriesArray[$i]->getCategory_name() ?></option>
                  <?php
                     }
                     
                     ?>   
               </select>
            </div>
         </div>
         <div class="col-12 col-lg-4">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Supplier</label>
               </div>
               <select class="custom-select" name="supplier_id" id="inputGroupSelect01">
                  <?php
                     for($i=0; $i<=count($suppliersArray)-1; $i++) {
                     
                     	?>
                  <option value="<?= $suppliersArray[$i]->getSupplier_origin_id() ?>" <?= checkSelected($suppliersArray[$i]->getSupplier_origin_id(), $product->getSupplier_id()) ?> ><?= $suppliersArray[$i]->getCompany_name() ?></option>
                  <?php
                     }
                     
                     ?>   
               </select>
            </div>
         </div>
         <div class="col-12 col-md-4">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Manufacturer</span>
               </div>
               <input type="text" name="manufacturer" value="<?= $product->getManufacturer() ?>" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 col-md-4">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Weight</span>
               </div>
               <input type="number" step="0.01" name="weight" value="<?= $product->getWeight() ?>" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 col-md-4">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Stock</span>
               </div>
               <input type="number" step="0.01" name="quantity" value="<?= $product->getQuantity() ?>" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
            </div>
         </div>
         <div class="col-12 text-center">
            <div class="col-12">
               <h3>Short Description</h3>
            </div>
            <div class="col-12">
               <div class="input-group">
                  <textarea class="form-control" name="shortDescription" aria-label="With textarea"><?= $product->getShortDescription() ?></textarea>
               </div>
            </div>
         </div>
         <div class="col-12 text-center">
            <div class="col-12">
               <h3>Long Description</h3>
            </div>
            <div class="col-12">
               <div class="input-group">
                  <textarea style="height: 400px" name="description" class="form-control" aria-label="With textarea"><?= $product->getDescription() ?></textarea>
               </div>
            </div>
         </div>
         <div class="col-12 text-center">
            <button id="saveButton" type="button" data="<?= $product->getButtonUrl() ?>" class="btn btn-primary"><?= $product->getButtonText() ?></button>
         </div>
      </div>
   </form>
</div>

