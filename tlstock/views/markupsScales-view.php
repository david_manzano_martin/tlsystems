<?php require(__DIR__ . "/partials/header-partial-view.php") ?>

<div class="container">
	<div class="row text-center">
		<div class="col-12">
			<h1>Markups management</h1>
		</div>
	</div>
	<div class="row justify-content-center">
		<?php
			for($k=0; $k<=count($markupsArray)-1; $k++) {
				require(__DIR__ . "/partials/markupsCard-partial-view.php");
			}
		?>
	</div>
</div>