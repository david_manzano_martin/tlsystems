<div class="col-12">
   <div class="row">
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getSupplier_id() ?>" <?= $suppliersArray[$k]->getDisabled() ?> placeholder="Id" aria-label="Id"  class="form-control" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Id</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getSupplier_origin_id() ?>" <?= $suppliersArray[$k]->getDisabled() ?> placeholder="Original ID" aria-label="Original ID"  class="form-control" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Origin Id</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getCompany_name() ?>" class="form-control" placeholder="Company name" aria-label="Company name" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Company name</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getAddress() ?>" class="form-control" placeholder="Address" aria-label="Address" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Address</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getCity() ?>" class="form-control" placeholder="City" aria-label="City" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">City</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getZip() ?>" class="form-control" placeholder="Zip" aria-label="Zip" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Zip</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getCountry_iso() ?>" class="form-control" placeholder="Country ISO" aria-label="Country ISO" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Country ISO</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getCountry() ?>" class="form-control" placeholder="Country" aria-label="Country" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Country</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getContact_name() ?>" class="form-control" placeholder="Contact name" aria-label="Contact name" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Contact name</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getEmail() ?>" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Email</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getPhone() ?>" class="form-control" placeholder="Phone" aria-label="Phone" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Phone</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getFax() ?>" class="form-control" placeholder="Fax" aria-label="Fax" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Fax</span>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-4 select">
         <div class="input-group mb-3">
            <input type="text" value="<?= $suppliersArray[$k]->getDdi() ?>" class="form-control" placeholder="Did" aria-label="Did" aria-describedby="basic-addon2">
            <div class="input-group-append">
               <span class="input-group-text" id="basic-addon2">Did</span>
            </div>
         </div>
      </div>
      <div class="input-group groupArea div.select">
         <div class="input-group-prepend">
            <span class="input-group-text">Notes</span>
         </div>
         <textarea value="dd" class="form-control" aria-label="With textarea"> <?= $suppliersArray[$k]->getNotes() ?> </textarea>
      </div>
      <div class="col-12 text-center">
         
         <button type="button" class="btn btn-primary">Update</button>
         
      </div>
   </div>
</div>