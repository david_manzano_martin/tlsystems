<form class="row" style="margin: 10px 0px; border:solid gray; padding: 5px;">
   <div class="col-12 col-md-4" style="display: none">
      <div class="input-group mb-3">
         <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">idMarkup</span>
         </div>
         <input type="number" name="idMarkup" value="<?= $markupsArray[$k]->getIdMarkup() ?>" class="form-control" placeholder="idMarkup" aria-label="Username">
      </div>
   </div>
   <div class="col-12 col-md-6">
      <div class="input-group mb-3">
         <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Max price</span>
         </div>
         <input type="number" name="maxQuantity" value="<?= $markupsArray[$k]->getMaxQuantity() ?>" class="form-control" placeholder="Max Quantity" aria-label="Username">
      </div>
   </div>
   <div class="col-12 col-md-6">
      <div class="input-group mb-3">
         <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Increment (%)</span>
         </div>
         <input type="number" name="increment" value="<?= $markupsArray[$k]->getIncrement() ?>" class="form-control" placeholder="Increment" aria-label="Username">
      </div>
   </div>
   <div class="col-12 text-center">
   	<button type="button" class="btn btn-primary">Save</button>
   </div>
</form>