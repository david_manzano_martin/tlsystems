<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <!-- FontAwesome -->    
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
      <!-- Bootstrap -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <!-- PapaParse -->
      <script src="scripts/papaparse/papaparse.min.js"></script>
      <!-- My styles -->
      <LINK href="styles/myStyles/common.css" rel="stylesheet" type="text/css">
      <LINK href="styles/myStyles/<?= $option ?>.css" rel="stylesheet" type="text/css">
      <!-- My scripts -->
      <script src="scripts/myScripts/common.js"></script>
      <script src="scripts/myScripts/<?= $option ?>.js"></script>
      <title><?= $pageName?></title>
   </head>
   <div id="globalMenu">
      <span><i class="fas fa-times"></i></span>
      <ul>
      	<li><a href="?" >Main</a></li>
         <?php
            for($k=0;$k<=count($optionsArray); $k++) {
            ?>
         <li><a href="<?= $optionsArray[$k]["url"] ?>" ><?= $optionsArray[$k]["title"] ?></a></li>
         <?php   	
            }
            
            ?>
      </ul>
   </div>
   <span><i class="fas fa-bars"></i></span>
   
   <div id="loadMenu">
	<div class="centered">
		<img src="imgs/loading.gif" alt="">
		<p>Updating</p>	
	</div>
</div>