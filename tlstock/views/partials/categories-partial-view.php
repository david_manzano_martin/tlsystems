<form class="row" style="border: solid gray; padding: 10px; margin: 10px 0px;">
   <div class="col-12 col-md-4" style="display: none">
      <div class="input-group mb-3">
         <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">category_id</span>
         </div>
         <input type="number" disabled="" name="category_id" value="<?= $categoriesArray[$k]->getCategory_id() ?>" class="form-control" placeholder="category id"aria-describedby="basic-addon1">
      </div>
   </div>
   <div class="col-12 col-md-6">
      <div class="input-group mb-3">
         <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">PrestaShop ID</span>
         </div>
         <input type="number" name="presto_shop_id" value="<?= $categoriesArray[$k]->getPresto_shop_id() ?>" class="form-control" placeholder="PrestaShop ID" aria-describedby="basic-addon1">
      </div>
   </div>
   <div class="col-12 col-md-6">
      <div class="input-group mb-3">
         <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Name</span>
         </div>
         <input type="text" name="category_name" value="<?= $categoriesArray[$k]->getCategory_name() ?>" class="form-control" placeholder="Name" aria-describedby="basic-addon1">
      </div>
   </div>
   <div class="col-12 text-center">
      <button type="button" class="btn btn-primary"><?= $categoriesArray[$k]->getButtonName() ?></button>
   </div>
</form>