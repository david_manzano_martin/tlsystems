<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<div class="container text-center">
   <div class="row">
      <div class="col-12">
         <h1>Import products from CSV</h1>
      </div>
   </div>
   <div class="row justify-content-center">
      <form action="?option=import_products_model&type=array" method="POST" class="col-12 col-md-12" enctype="multipart/form-data">
         <div class="col-12 col-lg-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Choose the supplier</label>
               </div>
               <select class="custom-select" name="supplierId" id="supplierId">
                  <?php
                     for($i=0; $i<=count($suppliersArray)-1; $i++) {
                     
                     	?>
                  <option value="<?= $suppliersArray[$i]->getSupplier_origin_id() ?>"><?= $suppliersArray[$i]->getCompany_name() ?></option>
                  <?php
                     }
                     
                     ?>   
               </select>
            </div>
         </div>
         <div class="col-12">
            <div class="input-group mb-3">
               <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
               </div>
               <div class="custom-file">
                  <input type="file" class="custom-file-input" id="file" name="file" aria-describedby="inputGroupFileAddon01">
                  <label class="custom-file-label" for="inputGroupFile01">Products CSV</label>
               </div>
            </div>
         </div>
      </form>
   </div>
   <hr>
   <div class="row">
   	<div class="col-12">
			<h1>Download files</h1>
   	</div>
   	<div class="col-12">
   		<?= showGetData() ?>
   	</div>
   </div>
   <hr>
   <div class="row">
      <div class="col-12">
         <h1>Add products manually and modify them</h1>
      </div>
      <div class="col-12">
      	<h6> <?= productsLeft($db, $tableTmpName) ?> products remaining</h6>
      </div>
   </div>
   <div class="row justify-content-center">
      <div class="col-12 col-md-12" enctype="multipart/form-data">
         <div class="col-12">
            <?php
               for($k=0; $k<=count($arrayProducts)-1; $k++) {
               
               	require(__DIR__ . "/partials/productCard-partial-view.php");
               
               }
               
               ?>            
         </div>
         <div class="col-12" style="margin:10px">
            <div class="row">
               <div class="col-6 text-right">
                  <button id="buttonBefore" type="button" class="btn btn-info"><--</button>
               </div>
               <div class="col-6 text-left">
                  <button id="buttonNext" type="button" class="btn btn-info">--></button>
               </div>
               <div class="col-12 text-center">
						<?php
							require(__DIR__ . "/partials/pagesNumber-partial-view.php");
						?>
					</div>
            </div>
         </div>
      </div>
   </div>
</div>