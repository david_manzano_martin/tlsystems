

<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<div class="container text-center">
   <div class="row">
      <div class="col-12">
         <h1>View products</h1>
      </div>
   </div>
   <div class="col-12">
      <form method="POST" action="?option=view_products" class="input-group mb-3">
         <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Search by product name or category name</span>
         </div>
         <input type="text" class="form-control" name="productName" placeholder="Product name / category name" aria-describedby="basic-addon1">
      </form>
   </div>
   <div class="col-12">
      <form method="POST" action="?option=view_products" id="formCategory" class="input-group mb-3">
         <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">Category</label>
         </div>
         <select class="custom-select" name="categoryId" id="categoryId">
            <?php
               for($i=0; $i<=count($categoriesArray)-1; $i++) {
               
               	?>
            <option value="<?= $categoriesArray[$i]->getCategory_id() ?>" <?= checkSelected($categoriesArray[$i]->getCategory_id(), $_POST["categoryId"]) ?> ><?= $categoriesArray[$i]->getCategory_name() ?></option>
            <?php
               }
               
               ?>   
         </select>
      </form>
   </div>
   <div class="row justify-content-center">
      <div class="col-12 col-md-12" enctype="multipart/form-data">
         <div class="col-12">
            <?php
               for($k=0; $k<=count($arrayProducts)-1; $k++) {
               
               	require(__DIR__ . "/partials/productCard-partial-view.php");
               
               }
               
               ?>            
         </div>
         <div class="col-12" style="margin:10px">
            <div class="row">
               <div class="col-6 text-right">
                  <button id="buttonBefore" type="button" class="btn btn-info"><--</button>
               </div>
               <div class="col-6 text-left">
                  <button id="buttonNext" type="button" class="btn btn-info">--></button>
               </div>
               <div class="col-12 text-center">
                  <?php
                     require(__DIR__ . "/partials/pagesNumber-partial-view.php");
                     ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

