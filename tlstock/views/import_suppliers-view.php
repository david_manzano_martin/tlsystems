<?php require(__DIR__ . "/partials/header-partial-view.php") ?>

<div class="container text-center">

	<div class="row">

		<div class="col-12">

			<h1>Import suppliers from CSV</h1>
		
		</div>
	
	</div>
		
	<div class="row justify-content-center">
	
		<div class=" col-12 col-md-12">
		
			<div class="input-group mb-3">
				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
  				</div>
  				<div class="custom-file">
    				<input type="file" class="custom-file-input" id="suppliersFleInput" aria-describedby="inputGroupFileAddon01">
    				<label class="custom-file-label" for="inputGroupFile01">Suppliers CSV</label>
  				</div>
			</div>
		
		</div>
	
	</div>

	<hr>
	<hr>	
	
	<div class="row">

		<div class="col-12">

			<h1>Add and modify suppliers manually</h1>
		
		</div>
	
	</div>
	
	<hr>
		
	<div class="row suppliersMainRow">
	
		<?php
		
			for($k=0; $k<=count($suppliersArray)-1; $k++) {

				require(__DIR__ . "/partials/suppliersCard-partial-view.php");
			
			}
		
		?>
	
	</div>	
	
</div>

</body>
</html>