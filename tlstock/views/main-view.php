<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<div class="container text-center">
   <div class="row">
      <div class="col-12">
         <h1>Choose your option</h1>
      </div>
   </div>
   <?php
   
   	for($k=0;$k<=count($optionsArray); $k++) {
			?>
			<div class="row justify-content-center">
      <div class=" col-12 col-md-4">
         <h6><a href="<?= $optionsArray[$k]["url"] ?>" ><?= $optionsArray[$k]["title"] ?></h6>
      </div>
   </div>
			<?php   	
   	}
   ?>
</div>