<?php require(__DIR__ . "/partials/header-partial-view.php") ?>
<div class="container text-center">
   <div class="row">
      <div class="col-12">
         <h1>Transform file</h1>
      </div>
      <form class="col-12" method="POST" action="?option=convertXml" enctype="multipart/form-data">
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text" id="inputGroupFileAddon01">Choose file</span>
            </div>
            <div class="custom-file">
               <input type="file" id="inputFile" name="file" class="custom-file-input" id="file" aria-describedby="inputGroupFileAddon01">
               <label class="custom-file-label" name="file" id="fileName" for="inputGroupFile01">Choose file</label>
            </div>
         </div>
         <div class="col-12">
            <button class="btn btn-primary" >Transform</button>
         </div>
      </form>
   </div>
</div>