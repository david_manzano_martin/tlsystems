window.addEventListener("load", () => {
   
   var updateButtons = document.querySelectorAll("button");
   var buttonNext = document.getElementById("buttonNext");
	var buttonBefore = document.getElementById("buttonBefore");
	var categoryFilter = document.getElementById("categoryId");

	let locationPath = window.location.toString();

	var offsetValue =locationPath.substring((locationPath.indexOf("offsetValue") + 12), locationPath.length); 
	
	if(isNaN(offsetValue)){
		offsetValue = 0;
	}
	
	categoryFilter.addEventListener("change", ()=>{

		document.getElementById("formCategory").submit();
	
	});
	
	buttonNext.addEventListener("click", ()=>{

		window.location.href = "?option=view_products&offsetValue=" + (parseInt(offsetValue) + 1);
	
	});
	
	
	
	buttonBefore.addEventListener("click", ()=>{

		if(offsetValue>0){
			window.location.href = "?option=view_products&offsetValue=" + (parseInt(offsetValue) - 1);
		}
	
	});
	
    var cont = 0;

    //For update the information manually
    for (var k = 0; k <= updateButtons.length - 3; k++) {

        updateButtons[k].addEventListener("click", move);

    }

    function move(event) {

        let parent = event.target.parentNode.parentNode.parentNode.parentNode.querySelectorAll("div")[0].querySelectorAll("span")[0];
        console.log(parent.innerHTML);
        window.location.href = "?option=view_product&name=" + encodeURIComponent(parent.innerHTML);
        
        /*?option=view_product&name=JZ074A*/
        //console.log(parent.attr.href);
        //window.location.href = parent["href"];
        

    }


});