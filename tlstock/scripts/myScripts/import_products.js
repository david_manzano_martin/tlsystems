window.addEventListener("load", () => {

   var fileInput = document.getElementById("file");
   
   var updateButtons = document.querySelectorAll("button");
   var buttonNext = document.getElementById("buttonNext");
	var buttonBefore = document.getElementById("buttonBefore");

	let locationPath = window.location.toString();	

	var offsetValue =locationPath.substring((locationPath.indexOf("offsetValue") + 12), locationPath.length); 
	
	if(isNaN(offsetValue)){
		offsetValue = 0;
	}
	
	buttonNext.addEventListener("click", ()=>{

		window.location.href = "?option=import_products&offsetValue=" + (parseInt(offsetValue) + 1);
	
	});
	
	buttonBefore.addEventListener("click", ()=>{

		if(offsetValue>0){
			window.location.href = "?option=import_products&offsetValue=" + (parseInt(offsetValue) - 1);
		}
	
	});
	

    var cont = 0;

    //For update the information manually
    for (var k = 0; k <= updateButtons.length - 3; k++) {

        updateButtons[k].addEventListener("click", getData);

    }

    function getData(event) {

        //let parent = event.target.parentNode.parentNode.querySelectorAll("div")[1].querySelectorAll("select")[0].value;
        let parent = event.target.parentNode.parentNode.parentNode.parentNode;      

			let data = {
				categoryCode: parent.querySelectorAll("select")[0].value,
				productNumber: parent.querySelectorAll("div.name span")[0].innerHTML
			};
			
			console.log("Enviado: " + JSON.stringify(data));
			
			
        changeProduct(data);

    }
    
    

    //For read and send the ajax information to the model to update the database with csv file 	

    fileInput.addEventListener("change", (evt) => {

        var files = fileInput.files;
        
        for(var i=0; i<files.length; i++){
        uploadFile(fileInput.files[i]); // call the function to upload the file
    	  }

    });

    function uploadFile(file) {
		  let menu = document.getElementById("loadMenu");        
        var url = '?option=import_products_model&type=array&supplierId=' + document.getElementById("supplierId").value;
        var xhr = new XMLHttpRequest();        	
        var fd = new FormData();
        xhr.open("POST", url, true);
        menu.style.display = "block";
        xhr.onreadystatechange = function() {
        	console.log("Status: " + xhr.status);
            if (xhr.readyState === 4 && xhr.status === 200) {
                // Every thing ok, file uploaded
                console.log("OK");
                console.log(this.responseText);
                alert("All products were added!!!");
            } else if (xhr.readyState == 4) {
                // with some error
                console.log(xhr.responseText); // handle response.
            }
            menu.style.display = "none";
        };
        fd.append("file", file);
        xhr.send(fd);
    }


});

function changeProduct(data) {
		var xhttp = new XMLHttpRequest();
  		xhttp.onreadystatechange = function() {
  			
  			console.log(this.status);
    		if (this.readyState == 4 && this.status == 200) {
     			console.log(this.responseText);
     			location.reload();
    		}
 		};
 		
  		xhttp.open("GET", "?option=modify_Product&action=move&data=" + encodeURIComponent(JSON.stringify(data)), true);
  		xhttp.send();
	}