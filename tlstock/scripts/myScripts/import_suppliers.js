window.addEventListener("load", ()=>{

	var suppliersFleInput = document.getElementById("suppliersFleInput");
	var updateButtons = document.querySelectorAll(".suppliersMainRow button");
	
	var cont = 0;
	
	//For update the information manually
	for (var k=0; k<=updateButtons.length-1;k++) {

		updateButtons[k].addEventListener("click", getData);
		
		
	
	}
	
	function getData(event){

		let parent = event.target.parentNode.parentNode;
		
		let data = {

			supplier_id:parent.querySelectorAll("div.select")[0].querySelectorAll("input")[0].value,
			supplier_origin_id:parent.querySelectorAll("div.select")[1].querySelectorAll("input")[0].value,
			company_name:parent.querySelectorAll("div.select")[2].querySelectorAll("input")[0].value, 
			address:parent.querySelectorAll("div.select")[3].querySelectorAll("input")[0].value, 
			city:parent.querySelectorAll("div.select")[4].querySelectorAll("input")[0].value, 
			zip:parent.querySelectorAll("div.select")[5].querySelectorAll("input")[0].value, 
			country_iso:parent.querySelectorAll("div.select")[6].querySelectorAll("input")[0].value, 
			country:parent.querySelectorAll("div.select")[7].querySelectorAll("input")[0].value, 
			contact_name:parent.querySelectorAll("div.select")[8].querySelectorAll("input")[0].value, 
			email:parent.querySelectorAll("div.select")[9].querySelectorAll("input")[0].value, 
			phone:parent.querySelectorAll("div.select")[10].querySelectorAll("input")[0].value, 
			fax:parent.querySelectorAll("div.select")[11].querySelectorAll("input")[0].value, 
			ddi:parent.querySelectorAll("div.select")[12].querySelectorAll("input")[0].value, 
			notes:parent.querySelectorAll("textArea")[0].value
		
		};

		/*data.supplier_id = parent.querySelectorAll("div.select")[0].querySelectorAll("input")[0].value;
		data.supplier_origin_id = parent.querySelectorAll("div.select")[1].querySelectorAll("input")[0].value;
		data.company_name = parent.querySelectorAll("div.select")[2].querySelectorAll("input")[0].value;
		data.address = parent.querySelectorAll("div.select")[3].querySelectorAll("input")[0].value;
		data.city = parent.querySelectorAll("div.select")[4].querySelectorAll("input")[0].value;
		data.zip = parent.querySelectorAll("div.select")[5].querySelectorAll("input")[0].value;
		data.country_iso = parent.querySelectorAll("div.select")[6].querySelectorAll("input")[0].value;
		data.country = parent.querySelectorAll("div.select")[7].querySelectorAll("input")[0].value;
		data.contact_name = parent.querySelectorAll("div.select")[8].querySelectorAll("input")[0].value;
		data.email = parent.querySelectorAll("div.select")[9].querySelectorAll("input")[0].value;
		data.phone = parent.querySelectorAll("div.select")[10].querySelectorAll("input")[0].value;
		data.fax = parent.querySelectorAll("div.select")[11].querySelectorAll("input")[0].value;
		data.ddi = parent.querySelectorAll("div.select")[12].querySelectorAll("input")[0].value;
		data.notes = parent.querySelectorAll("textArea")[0].value;*/
		
		adddSuppliersModel(data, "object");
		
	}

	//For read and send the ajax information to the model to update the database with csv file 	
	
	suppliersFleInput.addEventListener("change", (evt)=>{

		var data = null;
		var file = evt.target.files[0];
		var reader = new FileReader();
		reader.readAsText(file);
		reader.onload = function(event) {
		
			var csvData = event.target.result;
			var data = Papa.parse(csvData, {header : true});
			//console.log(data);
			adddSuppliersModel(data["data"], "array");
 
		};
		
		reader.onerror = function() {
			alert('Unable to read ' + file.fileName);
		};
 
	});
	
	function adddSuppliersModel(data, type) {
		var xhttp = new XMLHttpRequest();
  		xhttp.onreadystatechange = function() {
    		if (this.readyState == 4 && this.status == 200) {
     			console.log(this.responseText);
    		}
 		};
 		
  		xhttp.open("GET", "?type=" + type + "&option=import_suppliers_model&data=" + JSON.stringify(data), true);
  		xhttp.send();
	}
	
	
	
});