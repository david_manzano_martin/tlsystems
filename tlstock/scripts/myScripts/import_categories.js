window.addEventListener("load", () => {
   
   var saveButtons = document.querySelectorAll("button");
   var buttonNext = document.getElementById("buttonNext");
	var buttonBefore = document.getElementById("buttonBefore");

	let locationPath = window.location.toString();	

	var offsetValue =locationPath.substring((locationPath.indexOf("offsetValue") + 12), locationPath.length); 
	
	if(isNaN(offsetValue)){
		offsetValue = 0;
	}
	
	buttonNext.addEventListener("click", ()=>{

		window.location.href = "?option=import_categories&offsetValue=" + (parseInt(offsetValue) + 1);
	
	});
	
	buttonBefore.addEventListener("click", ()=>{

		if(offsetValue>0){
			window.location.href = "?option=import_categories&offsetValue=" + (parseInt(offsetValue) - 1);
		}
	
	});
	

    var cont = 0;

	saveButtons[0].addEventListener("click", (event)=>{

		let form = event.target.parentNode.parentNode;
		addCategory(serialize(form));
	
	});

    //For update the information manually
    for (var k = 1; k <= saveButtons.length - 3; k++) {

        saveButtons[k].addEventListener("click", (event)=>{
		  		
		  		let form = event.target.parentNode.parentNode;
		  		//console.log(JSON.stringify(serialize(form)));
		  		updateCategory(serialize(form));
		  		
        });

    }


});

function addCategory(data) {
		var xhttp = new XMLHttpRequest();
  		xhttp.onreadystatechange = function() {
  			console.log(this.status);
    		if (this.readyState == 4 && this.status == 200) {
     			console.log(this.responseText);
     			alert("Category added");
     			location.reload();
    		}
 		};
 		
  		xhttp.open("POST", "?option=add_category", true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("data=" + JSON.stringify(data)); 
		//console.log(JSON.stringify(data));
  		
	}

function updateCategory(data) {
		var xhttp = new XMLHttpRequest();
  		xhttp.onreadystatechange = function() {
  			console.log(this.status);
    		if (this.readyState == 4 && this.status == 200) {
     			console.log(this.responseText);
     			alert("Category updated properly");
     			//location.reload();
    		}
 		};
 		
  		xhttp.open("POST", "?option=modify_category", true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("data=" + JSON.stringify(data)); 
		//console.log(JSON.stringify(data));
  		
	}