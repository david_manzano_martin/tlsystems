window.addEventListener("load", ()=>{

	let fileName = document.getElementById("fileName");
	let inputFile = document.getElementById("inputFile");
	
	inputFile.addEventListener("change", (e)=>{

		fileName.innerText = e.target.files[0].name;
	
	});

});