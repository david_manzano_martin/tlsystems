<?php

require(__DIR__ . "/Classes/Product_tmp.php");
require(__DIR__ . "/Classes/Category.php");
require(__DIR__ . "/Classes/Supplier.php");

(!isset($_GET["offsetValue"])) ? $offsetValue = "0" : $offsetValue = $_GET["offsetValue"];

$offsetValue *= 6;

$pageName     = "Export products";
$tableName    = "products_David";
$tableTmpName = "products_tmp_David";

$productsArray = getProducts($db, $tableName);

/* All needed to export data */
$delimiter = ",";
$filename  = "data.csv";

$f = fopen("php://memory", "w");

//Settings headers
$fields = array(
    "productId",
    "name",
    "categoriesId",
    //"supplierCode",
    "price_tax_exclude",
    "reference",
    "supplier_id",
    "manufacturer",
    "weight",
    "quantity",
    "shortDescription",
    "description",
    "tags",
    "urlRewritten",
    "imageUrl",
    "EanNumber"
);
fputcsv($f, $fields, $delimiter);

for ($k = 0; $k <= count($productsArray) - 1; $k++) {
    
    $lineData = array(
        $productsArray[$k]->getProductId(),
        //substr($productsArray[$k]->getProductName(), 0, strlen($productsArray[$k]->getProductName()) - 5),
        //substr($productsArray[$k]->getProductName(), 0, 50),
        clean(getName($productsArray[$k]->getProductName())),
        getPsCategory($db, $productsArray[$k]->getCategoriesId(), "category_David"),
        //"??",
        $productsArray[$k]->getPrice_tax_exclude(),
        $productsArray[$k]->getReference(),
        $productsArray[$k]->getSupplier_id(),
        $productsArray[$k]->getManufacturer(),
        $productsArray[$k]->getWeight(),
        $productsArray[$k]->getQuantity(),
        $productsArray[$k]->getShortDescription(),
        $productsArray[$k]->getDescription(),
        $productsArray[$k]->getTags(),
        $productsArray[$k]->getUrlRewritten(),
        $productsArray[$k]->getImageUrl(),
        //("http://www.tlsystems.uk/shop/stockimages" . substr($productsArray[$k]->getImageUrl(), strrpos($productsArray[$k]->getImageUrl(), "/"), strlen($productsArray[$k]->getImageUrl()))),
        //$productsArray[$k]->getEanNumber()
        checkEaNumber($productsArray[$k]->getEanNumber())
        
    );
    fputcsv($f, $lineData, $delimiter);
    
}

fseek($f, 0);

header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=" . $filename . ";");

fpassthru($f);

/* Functions */

function getPsCategory($db, $product, $tableName){

	$query = "SELECT * FROM " . $tableName . " WHERE category_id = " . $product;
	$result = $db->query($query);
	
	while($row = $result->fetch_assoc()){
		return $row["presto_shop_id"];
	}
	
}

function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
   $string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
   return preg_replace('/-+/', ' ', $string);
}


function getName($name){
	
	//Max length 128

	$nameLength = strlen($name) - 5;
	
	if($nameLength < 120){

		 /*$result = $name;
		 die("True: " . $result);*/
		return substr($name, 0, strlen($name) - 5);
	
	} else{

		/*die("False: " . substr($name, 0, strlen($name) - 5));*/
		return substr($name, 0, 127);

	}

}

function checkEaNumber($eaNumber){

	if(is_nan($eaNumber)){
		return "12";
	} else{
		return substr($eaNumber, 0, 12);
	}
	
}

/*
Function used to get the suppliers of the database

@param $db Mysqli object connected to the db
@param $tableName String containing the name of the table

@return Array containing all of the products of the database

*/

function getProducts($db, $tableName)
{
    $arrayResults = [];
    
    
    $query = "SELECT * FROM " . $tableName;
    
    $result = $db->query($query);
    
    while ($row = $result->fetch_assoc()) {
        
        $currentProduct = new Product_tmp($row["productId"], $row["name"], $row["categoriesId"], $row["price_tax_exclude"], $row["reference"], $row["supplier_id"], $row["manufacturer"], $row["weight"], $row["quantity"], $row["shortDescription"], $row["description"], $row["tags"], $row["urlRewritten"], $row["imageUrl"], $row["EanNumber"]);
        
        array_push($arrayResults, $currentProduct);
        
    }
    
    return $arrayResults;
    
}

?> 