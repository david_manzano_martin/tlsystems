<?php

require(__DIR__ . "/Classes/Product_tmp.php");
require(__DIR__ . "/Classes/Category.php");
require(__DIR__ . "/Classes/Supplier.php");

(!isset($_GET["offsetValue"])) ? $offsetValue = "0" : $offsetValue = $_GET["offsetValue"];

$pageName = "Product: " . $_GET["name"];
$product  = getProductByName($db, format(htmlspecialchars_decode($_GET["name"])), "products_David");
$product->setButtonText("Update data");
$product->setButtonUrl($product->getProductName());

$categoriesArray = getCategories($db, "category_David");
$suppliersArray  = getSuppliers($db, "supplier_David");

require(__DIR__ . "/../views/view_product-view.php");

/* Functions */
function checkSelected($val1, $val2)
{
    
    if ($val1 == $val2) {
        echo "selected='true'";
    }
    
}

/* Functions */

/*
Function used to get all the suppliers from the database

@param $db Mysqli object connected to the db 
@param $tableName String that contains the name of the table in the database

@return Array containing all the suppliers

*/

function getSuppliers($db, $tableName)
{
    
    $arrayResults = [];
    
    $query = "SELECT * FROM " . $tableName;
    
    $result = $db->query($query);
    
    while ($row = $result->fetch_assoc()) {
        $currentSupplier = new Supplier($row["supplier_id"], $row["supplier_origin_id"], $row["company_name"], $row["address"], $row["city"], $row["zip"], $row["country_iso"], $row["country"], $row["contact_name"], $row["email"], $row["phone"], $row["fax"], $row["ddi"], $row["notes"]);
        array_push($arrayResults, $currentSupplier);
    }
    
    return $arrayResults;
    
}

/*
Function used to get all the categories from the database

@param $db Mysqli object connected to the db 
@param $tableName String that contains the name of the table in the database

@return Array containing all the categories

*/

function getCategories($db, $tableName)
{
    
    $arrayResults = [];
    
    $query = "SELECT * FROM " . $tableName . " ORDER BY category_name ASC";
    
    $result = $db->query($query);
    
    while ($row = $result->fetch_assoc()) {
        
        $currentCategory = new Category($row["category_id"], $row["presto_shop_id"], $row["category_name"]);
        array_push($arrayResults, $currentCategory);
        
    }
    
    return $arrayResults;
    
}

function format($string) {
	
   $string = str_replace('"', '\"', $string); // Replaces all spaces with hyphens.
   $string = str_replace("'", "\'", $string); // Replaces all spaces with hyphens.
   return $string;
   
}

/*
Function used to find a product searching by his name

@param $db Mysqli object connected to the db 
@param $name String containing the name of the product
@param $tableName String that contains the name of the table in the database

@return Product_tmp object of the database

*/

function getProductByName($db, $name, $tableName)
{
    
    $query = "SELECT * FROM " . $tableName . " WHERE name = '" . $name . "'";
    $result = $db->query($query);
    
    while ($row = $result->fetch_assoc()) {
        
        $currentProduct = new Product_tmp($row["productId"], $row["name"], $row["categoriesId"], $row["price_tax_exclude"], $row["reference"], $row["supplier_id"], $row["manufacturer"], $row["weight"], $row["quantity"], $row["shortDescription"], $row["description"], $row["tags"], $row["urlRewritten"], $row["imageUrl"], $row["EanNumber"]);
        
    }
    
    return $currentProduct;
    
}

?> 