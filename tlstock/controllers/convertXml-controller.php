<?php

if($_SERVER["REQUEST_METHOD"] == "POST"){

	$delimiter = ",";
	$filename  = "data.csv";
	//$xmldata = simplexml_load_file("tagetpricelist.xml.xml") or die("Failed to load");
	//die($_FILES["file"]["tmp_name"]);
	$xmldata = simplexml_load_file($_FILES["file"]["tmp_name"]) or die("Failed to load");
	$fields = getColumns($xmldata);
	$data = getData($xmldata, $fields);

	$f = fopen("php://memory", "w");

	fputcsv($f, $fields, $delimiter);



	//Data
	for ($k = 0; $k <= count($data)-1; $k++) {

   	 $localArray = array();
    
    	for ($i = 0; $i <= count($fields)-1; $i++){

			array_push($localArray, $data[$k][$i]);
    
    	}
    
    	fputcsv($f, $localArray, $delimiter);
    
	}

	fseek($f, 0);

	header("Content-Type: text/csv");
	header("Content-Disposition: attachment; filename=" . $filename . ";");

	fpassthru($f);

	} else{

		require(__DIR__ . "/../views/convertXml-CSV-view.php");
	
	}


/* Functions */

function getData($xmlElement, $columns){

	$arrayResults = [];
	//print_r($columns);
	
	for($k=0; $k<=count($xmlElement->children())-1; $k++){
		
		$localArray = [];

		//echo($k . ": ");

		for($i=0; $i<=count($columns)-1; $i++){
			$attribute =$columns[$i]; 
			//array_push($arrayResults, strval($xmlElement->children()[$k]->$attribute));
			//echo($columns[$i] . " ");
			//echo(strval($xmlElement->children()[$k]->$attribute));
			array_push($localArray, strval($xmlElement->children()[$k]->$attribute));
		}
		
		//echo("<br>");
		array_push($arrayResults, $localArray);
		
	}

	//echo($xmldata->children()[1]->stockcode);
	//print_r($arrayResults);
	return $arrayResults;
	

}

function getColumns($xmlElement){
	
	$arrayResults = [];

	foreach($xmlElement->children()->children() as $child){

		//echo($child->getName() . " ");
		array_push($arrayResults, $child->getName());

	}
	
	return $arrayResults;

}

?> 