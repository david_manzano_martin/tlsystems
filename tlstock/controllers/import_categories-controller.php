<?php

	require(__DIR__ . "/Classes/Product_tmp.php");
	require(__DIR__ . "/Classes/Category.php");
	require(__DIR__ . "/Classes/Supplier.php");
	
	(!isset($_GET["offsetValue"]))? $offsetValue = "0" : $offsetValue = $_GET["offsetValue"];	
	
	$offsetValue *= 6;

	$pageName = "Import & modify categories";
	$tableName="category_David";
	
	$categoriesArray = getCategories($db, $tableName, $offsetValue);
	array_unshift($categoriesArray, new Category ("", "", ""));
	$categoriesArray[0]->setButtonName("Add");
	$pagesNumber = getPagesNumbers($db, $tableName, 6);
	
	require(__DIR__ . "/../views/import_categories-view.php");
	
	/* Functions */
	
		function getPagesNumbers($db, $tableName, $offsetValue){

		$query = "SELECT * FROM " . $tableName;
		$result = $db->query($query);
		
		//$result = ceil(round(($result->num_rows / $offsetValue), 0));
		$valueResult = ceil(round($result->num_rows / $offsetValue, 1));
			
		return $valueResult;

	}

	/*
Function used to get the categories of the database

@param $db Mysqli object connected to the db
@param $tableName String containing the name of the table
@param $offSetValue Int that contains the value offset to make the query

@return Array containing all of the categories of the database

*/
	
	function getCategories($db, $tableName, $offsetValue){
		
		$arrayResults = [];

		$query = "SELECT * FROM " . $tableName . " ORDER BY category_name ASC LIMIT 6 OFFSET " . $offsetValue;		
		$result = $db->query($query);
		
		
		while($row = $result->fetch_assoc()){

			$currentCategory = new Category ($row["category_id"], $row["presto_shop_id"], $row["category_name"]);
			array_push($arrayResults, $currentCategory);
			
		}
		
		return $arrayResults;
	
	}
	
?>