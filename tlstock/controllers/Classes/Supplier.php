<?php

	class Supplier{

		public $supplier_id;
		public $supplier_origin_id;
		public $company_name;
		public $address;
		public $city;
		public $zip;
		public $country_iso;
		public $country;
		public $contact_name;
		public $email;
		public $phone;
		public $fax;
		public $ddi;
		public $notes;
		public $disabled;
		
		/*public function __construct($contact_name){

			$this->contact_name = $contact_name;
		
		}
		
		public function getContact_name(){

			return ($this->contact_name);
		
		}*/
		
		public function __construct($supplier_id, $supplier_origin_id, $company_name, $address, $city, $zip, $country_iso, $country, $contact_name, $email, $phone, $fax, $ddi, $notes){

			$this->supplier_id = $supplier_id;
			$this->supplier_origin_id = $supplier_origin_id;
			$this->company_name = $company_name;
			$this->address = $address;
			$this->city = $city;
			$this->zip = $zip;
			$this->country_iso = $country_iso;
			$this->country = $country;
			$this->contact_name = $contact_name;
			$this->email = $email;
			$this->phone = $phone;
			$this->fax = $fax;
			$this->ddi = $ddi;
			$this->notes = $notes;
			$this->disabled = true;
		
		}
		
		function setDisabled($status){

			$this->disabled = $status;
		
		}
		
		function getDisabled(){

			if($this->disabled){
				return 'disabled=""';			
			} else{
				return '';			
			}
		
		}
		
		
		function getSupplier_id(){

			return ($this->supplier_id);
		
		}
		
		function getSupplier_origin_id(){

			return ($this->supplier_origin_id);
		
		}
		
		function getCompany_name(){

			return ($this->company_name);
		
		}
		
		function getAddress(){

			return ($this->address);
		
		}
		
		function getCity(){

			return ($this->city);
		
		}
		
		function getZip(){

			return ($this->zip);
		
		}
		
		function getCountry_iso(){

			return ($this->country_iso);
		
		}
		
		function getCountry(){

			return ($this->country);
		
		}
		
		function getContact_name(){

			return ($this->contact_name);
		
		}
		
		function getEmail(){

			return ($this->email);
		
		}
		
		function getPhone(){

			return ($this->phone);
		
		}
		
		function getFax(){

			return ($this->fax);
		
		}
		
		function getDdi(){

			return ($this->ddi);
		
		}
		
		function getNotes(){

			return ($this->notes);
		
		}
	
	}

?>