<?php

class MarkupScale
{
    
    public $idMarkup;
    public $maxQuantity;
    public $increment;
    
    function __construct($idMarkup, $maxQuantity, $increment)
    {
        
        $this->idMarkup    = $idMarkup;
        $this->maxQuantity = $maxQuantity;
        $this->increment   = $increment;
        
    }
    
    function setIdMarkup($idMarkup)
    {
        $this->idMarkup = $idMarkup;
    }
    function getIdMarkup()
    {
        return $this->idMarkup;
    }
    function setMaxQuantity($maxQuantity)
    {
        $this->maxQuantity = $maxQuantity;
    }
    function getMaxQuantity()
    {
        return $this->maxQuantity;
    }
    function setIncrement($increment)
    {
        $this->increment = $increment;
    }
    function getIncrement()
    {
        return $this->increment;
    }
    
}



?>