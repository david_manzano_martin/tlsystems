 <?php

class Product_tmp
{
    
    public $productId;
    public $productName;
    public $categoriesId;
    public $price_tax_exclude;
    public $reference;
    public $supplier_id;
    public $manufacturer;
    public $weight;
    public $quantity;
    public $shortDescription;
    public $description;
    public $tags;
    public $urlRewritten;
    public $imageUrl;
    public $buttonText;
    public $buttonUrl;
    public $eanNumber;
    
    public function __construct($productId, $productName, $categoriesId, $price_tax_exclude, $reference, $supplier_id, $manufacturer, $weight, $quantity, $shortDescription, $description, $tags, $urlRewritten, $imageUrl, $eanNumber = "000000000"){

		$this->productId = $productId;
		$this->productName = $productName;
		$this->categoriesId = $categoriesId;
		$this->price_tax_exclude = $price_tax_exclude;
		$this->reference = $reference;
		$this->supplier_id = $supplier_id;
		$this->manufacturer = $manufacturer;
		$this->weight = $weight;
		$this->quantity = $quantity;
		$this->shortDescription = $shortDescription;
		$this->description = $description;
		$this->tags = $tags;
		$this->urlRewritten = $urlRewritten;
		$this->imageUrl = $imageUrl;
		$this->buttonText = "";
		$this->buttonUrl = "";
		
		if(!intval($eanNumber)){
			$this->eanNumber = 0;
		} else{
			$this->eanNumber = $eanNumber;
		}
    
    }
    
    function setButtonText($buttonText)
    {
        $this->buttonText = $buttonText;
    }
    function getButtonText()
    {
        return $this->buttonText;
    }
    
    function setButtonUrl($buttonUrl)
    {
        $this->buttonUrl = $buttonUrl;
    }
    function getButtonUrl()
    {
        return $this->buttonUrl;
    }
    
    function setProductId($productId)
    {
        $this->productId = $productId;
    }
    function getProductId()
    {
        return $this->productId;
    }
    function setProductName($productName)
    {
        $this->name = $productName;
    }
    function getProductName()
    {
        return $this->productName;
    }
    function setCategoriesId($categoriesId)
    {
        $this->categoriesId = $categoriesId;
    }
    function getCategoriesId()
    {
        return $this->categoriesId;
    }
    function setPrice_tax_exclude($price_tax_exclude)
    {
        $this->price_tax_exclude = $price_tax_exclude;
    }
    function getPrice_tax_exclude()
    {
        return $this->price_tax_exclude;
    }
    function setReference($reference)
    {
        $this->reference = $reference;
    }
    function getReference()
    {
        return $this->reference;
    }
    function setSupplier_id($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }
    function getSupplier_id()
    {
        return $this->supplier_id;
    }
    function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }
    function getManufacturer()
    {
        return $this->manufacturer;
    }
    function setWeight($weight)
    {
        $this->weight = $weight;
    }
    function getWeight()
    {
        return $this->weight;
    }
    function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
    function getQuantity()
    {
        return $this->quantity;
    }
    function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }
    function getShortDescription()
    {
        return $this->shortDescription;
    }
    function setDescription($description)
    {
        $this->description = $description;
    }
    function getDescription()
    {
        return $this->description;
    }
    function setTags($tags)
    {
        $this->tags = $tags;
    }
    function getTags()
    {
        return $this->tags;
    }
    function setUrlRewritten($urlRewritten)
    {
        $this->urlRewritten = $urlRewritten;
    }
    function getUrlRewritten()
    {
        return $this->urlRewritten;
    }
    function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }
    function getImageUrl()
    {
        return $this->imageUrl;
    }
    function setEanNumber($eanNumber)
    {
        $this->eanNumber = $eanNumber;
    }
    function getEanNumber()
    {
        return $this->eanNumber;
    }
    
    function calculeDate($db, $tableName, $tableTmpName){
    	$query = "SELECT * FROM " . $tableName;
    	$result = $db->query($query);
    	while($row = $result->fetch_assoc()){
			$date = $row["dateAdded"];
    	}
    	$query = "SELECT * FROM " . $tableTmpName;
    	$result = $db->query($query);
    	while($row = $result->fetch_assoc()){
			$date = $row["dateAdded"];
    	}
    	return $date;
    }
    
    
}

?> 