 <?php

class Category
{
    
    public $category_id;
    public $presto_shop_id;
    public $category_name;
    public $buttonName;
    
    function __construct($category_id, $presto_shop_id, $category_name, $buttonName = "Save"){
		
		$this->category_id=$category_id;
		$this->presto_shop_id=$presto_shop_id;
		$this->category_name=$category_name;
		$this->buttonName=$buttonName;
		    
    }
    
    function setCategory_id($category_id)
    {
        $this->category_id = $category_id;
    }
    function getCategory_id()
    {
        return $this->category_id;
    }
    function setPresto_shop_id($presto_shop_id)
    {
        $this->presto_shop_id = $presto_shop_id;
    }
    function getPresto_shop_id()
    {
        return $this->presto_shop_id;
    }
    function setCategory_name($category_name)
    {
        $this->category_name = $category_name;
    }
    function getCategory_name()
    {
        return $this->category_name;
    }
    function setButtonName($buttonName)
    {
        $this->buttonName = $buttonName;
    }
    function getButtonName()
    {
        return $this->buttonName;
    }
    
    
}

?> 