<?php

	class Test{

		public $variable;
		
		public function __construct($variable){

			$this->variable = $variable;
		
		}
		
		public function getVariable(){
			return($this->variable);		
		}
	
	}

?>