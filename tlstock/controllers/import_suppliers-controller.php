<?php

require(__DIR__ . "/Classes/Supplier.php");

$pageName       = "Import suppliers";
$suppliersArray = getSuppliers($db);
array_unshift($suppliersArray, new Supplier("", "", "", "", "", "", "", "", "", "", "", "", "", ""));
$suppliersArray[0]->setDisabled(false);


require(__DIR__ . "/../views/import_suppliers-view.php");

/* Functions */

/*
Function used to get the suppliers of the database

@param $db Mysqli object connected to the db

@return Array containing the suppliers of the database

*/

function getSuppliers($db)
{
    
    $data = [];
    
    $result = $db->query("SELECT * FROM supplier_David");
    
    while ($row = $result->fetch_assoc()) {
        
        $currentSupplier = new Supplier($row["supplier_id"], $row["supplier_origin_id"], $row["company_name"], $row["address"], $row["city"], $row["zip"], $row["country_iso"], $row["country"], $row["contact_name"], $row["email"], $row["phone"], $row["fax"], $row["ddi"], $row["notes"]);
        
        array_push($data, $currentSupplier);
        
    }
    
    return $data;
    
}

?> 