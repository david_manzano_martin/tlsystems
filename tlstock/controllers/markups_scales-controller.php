<?php

	require(__DIR__ . "/Classes/MarkupScale.php");

	$pageName = "Controll markups scale";
	$tableName="markupScale_David";
	
	$markupsArray = getMarkupScales($db, $tableName);
	
	require(__DIR__ . "/../views/markupsScales-view.php");
	
	/* Functions */

	/*
Function used to get the categories of the database

@param $db Mysqli object connected to the db
@param $tableName String containing the name of the table
@param $offSetValue Int that contains the value offset to make the query

@return Array containing all of the categories of the database

*/
	function getMarkupScales($db, $tableName){
		
		$arrayResults = array();

		$query = "SELECT * FROM " . $tableName;
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()){
			$localMarkup = new MarkupScale($row["idMarkup"], $row["maxQuantity"], $row["increment"]);
			array_push($arrayResults, $localMarkup);
		}
		
		return $arrayResults;
		
	}

	
?>