<?php

	require(__DIR__ . "/Classes/Product_tmp.php");
	require(__DIR__ . "/Classes/Category.php");
	require(__DIR__ . "/Classes/Supplier.php");
	
	(!isset($_GET["offsetValue"]))? $offsetValue = "0" : $offsetValue = $_GET["offsetValue"];
	
	$offsetValue *= 6;
	
	$tableName="products_David";
	$tableTmpName="products_tmp_David";

	// Common getters	
	
	$categoriesArray= getCategories($db, "category_David");
	$suppliersArray=getSuppliers($db,"supplier_David");


	
	//Indeppendent getters
	
	switch($option){

		case"import_products":{
			
			$pageName = "Import products";
			$arrayProducts = getProducts($db, $tableTmpName, $option, $offsetValue);
			$pagesNumber = getPagesNumbers($db, $tableTmpName, 6);
			require(__DIR__ . "/../views/import_products-view.php");
			break;
			
		}
		
		case"view_products":{
			$pageName = "View products";
			
			
			if(isset($_POST["productName"])){
			
				$productName = trim($_POST["productName"]);
				
				if($productName == ""){
				
					$arrayProducts = getProducts($db, $tableName, $option, $offsetValue);
					$pagesNumber = getPagesNumbers($db, $tableName, 6);
			
				} else{
					
					$arrayProducts = searchProductsByName($db, $tableName, $productName);
				
				}
		
			} elseif(isset($_POST["categoryId"])) {

				$arrayProducts = searchProductsByCategory($db, $tableName, $_POST["categoryId"]);
				$pagesNumber = getPagesNumbers($db, $tableName, 6);
			
			} else{

				$arrayProducts = getProducts($db, $tableName, $option, $offsetValue);
				$pagesNumber = getPagesNumbers($db, $tableName, 6);
			
			}
			
			
			require(__DIR__ . "/../views/view_products-view.php");
			break;
			
		}
		
		
		
	
	}
	
	
	
	/* Functions */
	
		function searchProductsByCategory($db, $tableName, $categoryId){
		
		$arrayResults = [];
		
		$query = "SELECT * FROM " . $tableName . " WHERE categoriesId = " . $categoryId;
		
		$result = $db->query($query);
		
		
		while($row = $result->fetch_assoc()){

			$currentProduct = new Product_tmp ($row["productId"], $row["name"], $row["categoriesId"], $row["price_tax_exclude"], $row["reference"], $row["supplier_id"], $row["manufacturer"], $row["weight"], $row["quantity"], $row["shortDescription"], $row["description"], $row["tags"], $row["urlRewritten"], $row["imageUrl"]);
			$currentProduct->setButtonText("View full product");
			array_push($arrayResults, $currentProduct);
		
		}
		
		return $arrayResults;
	
	}

	function showGetData(){

		$arrayData = array(
			array(
				"name" => "spire",
				"ftpSite"=>"217.35.64.145/",
				"username"=>"gamma",
				"password"=>'$$VT7624tc'
				), 
			array(
				"name"=>"target",
				"ftpSite"=>"ftp.targetcomponents.co.uk/",
				"username"=>"TLC00003",
				"password"=>'XC14BQS7'
				)
		);
		
		for($k=0; $k<=count($arrayData)-1; $k++) {

			echo('<a href=ftp://' . $arrayData[$k]["username"] . ":" . $arrayData[$k]["password"] . "@" . $arrayData[$k]["ftpSite"] . ' >' . $arrayData[$k]["name"] . '</a> ');
			echo(" ");
		
		}
	
	}
	
	
	function searchProductsByName($db, $tableName, $name){
		
		$arrayResults = [];
		//$regex = "[" . $name . "]{" . strlen($name) . "}";
		
		$query = "SELECT * FROM " . $tableName . " WHERE name LIKE '%" . $name . "%' OR categoriesId IN (SELECT category_id FROM category_David WHERE category_name LIKE '%" . $name . "%')";
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()){

			$currentProduct = new Product_tmp ($row["productId"], $row["name"], $row["categoriesId"], $row["price_tax_exclude"], $row["reference"], $row["supplier_id"], $row["manufacturer"], $row["weight"], $row["quantity"], $row["shortDescription"], $row["description"], $row["tags"], $row["urlRewritten"], $row["imageUrl"]);
			$currentProduct->setButtonText("View full product");
			array_push($arrayResults, $currentProduct);
		
		}
		
		return $arrayResults;
	
	}
	
	function productsLeft($db, $tableName){
	
		$query = "SELECT * FROM " . $tableName;
		$result = $db->query($query);
		return $result->num_rows;
	
	}
	
	function getPagesNumbers($db, $tableName, $offsetValue){

		$query = "SELECT * FROM " . $tableName;
		$result = $db->query($query);
		
		//$result = ceil(round(($result->num_rows / $offsetValue), 0));
		$valueResult = ceil(round($result->num_rows / $offsetValue, 1));
			
		return $valueResult;

	}
	
	/*
Function used to compare if two elements are the same

@param $val1 Element to be compared
@param $val2 Element to be compared

@return String "selected" if the elements are the same

*/

	function checkSelected($val1, $val2){

		if($val1 == $val2){
			echo "selected";
		}
			
	}
	
	/*
Function used to get the suppliers of the database

@param $db Mysqli object connected to the db
@param $tableName String containing the name of the table

@return Array containing all of the suppliers of the database

*/

	function getSuppliers($db, $tableName){
		
		$arrayResults = [];
		
		$query = "SELECT * FROM " . $tableName;
		
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()) {
			$currentSupplier = new Supplier($row["supplier_id"], $row["supplier_origin_id"], $row["company_name"], $row["address"], $row["city"], $row["zip"], $row["country_iso"], $row["country"], $row["contact_name"], $row["email"], $row["phone"], $row["fax"], $row["ddi"], $row["notes"]);
			array_push($arrayResults, $currentSupplier);
		}
		
		return $arrayResults;
			
	}
	
	/*
Function used to get the categories of the database

@param $db Mysqli object connected to the db
@param $tableName String containing the name of the table

@return Array containing all of the categories of the database

*/
	
	function getCategories($db, $tableName){

		$arrayResults = [];
		
		$query = "SELECT * FROM " . $tableName . " ORDER BY category_name ASC";
		
		$result = $db->query($query);
	
		while($row = $result->fetch_assoc()){

			$currentCategory = new Category ($row["category_id"], $row["presto_shop_id"], $row["category_name"]);
			array_push($arrayResults, $currentCategory);
		
		}
		
		return $arrayResults;
			
	}
	
/*
Function used to get the suppliers of the database

@param $db Mysqli object connected to the db
@param $tableName String containing the name of the table
@param $option String containing the actions that will do
@param $offSetValue Int that contains the value offset to make the query

@return Array containing all of the products of the database

*/

	
	function getProducts($db, $tableName, $option, $offsetValue){
		
		global $option;
		
		if($option == "view_products"){
			$query = "SELECT * FROM " . $tableName  . " ORDER BY productId DESC LIMIT 6 OFFSET " . $offsetValue;
			//$query = "SELECT * FROM " . $tableName  . " LIMIT 6 OFFSET " . $offsetValue;
		} else{
			$query = "SELECT * FROM " . $tableName  . " LIMIT 6 OFFSET " . $offsetValue;
		}
		

		
		$arrayResults = [];
	
		
		
		$result = $db->query($query);
		
		while($row = $result->fetch_assoc()){

			$currentProduct = new Product_tmp ($row["productId"], $row["name"], $row["categoriesId"], $row["price_tax_exclude"], $row["reference"], $row["supplier_id"], $row["manufacturer"], $row["weight"], $row["quantity"], $row["shortDescription"], $row["description"], $row["tags"], $row["urlRewritten"], $row["imageUrl"]);
			switch($option) {
				case "import_products":{
					$currentProduct->setButtonText("Update category");
					break;
				}
				case "view_products":{
					$currentProduct->setButtonText("View full product");
					$currentProduct->setButtonUrl("?option=view_product&name=" . $currentProduct->getProductName());
					break;
				}
			}			
			array_push($arrayResults, $currentProduct);
		
		}
		
		return $arrayResults;
	
	}

?>