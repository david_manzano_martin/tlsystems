 <?php

require(__DIR__ . "/../database/connection.php");
require(__DIR__ . "/../controllers/Classes/Supplier.php");
require(__DIR__ . "/../controllers/Classes/Test.php");

$arraySuppliers = [];
$data = json_decode($_GET["data"], true);
$type = $_GET["type"];


switch($type) {

	case"array":{

	for ($k = 0; $k <= count($data) - 1; $k++) {
    
    	try {
      	$currentSupplier = new Supplier($data[$k]["supplier_id"], $data[$k]["supplier_origin_id"], $data[$k]["company_name"], $data[$k]["address"], $data[$k]["city"], $data[$k]["zip"], $data[$k]["country_iso"], $data[$k]["country"], $data[$k]["contact_name"], $data[$k]["email"], $data[$k]["phone"], $data[$k]["fax"], $data[$k]["ddi"], $data[$k]["notes"]);
        
        	array_push($arraySuppliers, $currentSupplier);
        
   	}
    catch (Exception $e) {
        echo ("Error controlled");
    }
    
   for ($k = 0; $k <= count($arraySuppliers) - 2; $k++) {
   	addSuppliers($db, $arraySuppliers[$k]);
	}
    
}
		break;
	
	}
	
	case "object":{

		$currentSupplier = new Supplier($data["supplier_id"], $data["supplier_origin_id"], $data["company_name"], $data["address"], $data["city"], $data["zip"], $data["country_iso"],
		 		$data["country"], $data["contact_name"], $data["email"], $data["phone"], $data["fax"], $data["ddi"], $data["notes"]);
		 		
		 		addSuppliers($db, $currentSupplier);
		 		
		 	break;
	
	}

}


/* Functions */

/*
	Function used to add or update suppliers depending if exist or not
	
	@param $db Mysqli object connected to the db
	@param $supplier Supplier object that has to be added or updated
	
*/

function addSuppliers($db, $supplier)
{
    
    if (checkIfExist($db, $supplier)) {
        
        $query = "UPDATE supplier_David SET supplier_origin_id='" . $supplier->getSupplier_origin_id() . "', company_name='" . $supplier->getCompany_name() . "', address='" . $supplier->getAddress() . "', city='" . $supplier->getCity() . "', zip='" . $supplier->getZip() . "', country_iso='" . $supplier->getCountry_iso() . "', country='" . $supplier->getCountry() . "', contact_name='" . $supplier->getContact_name() . "', email='" . $supplier->getEmail() . "', phone='" . $supplier->getPhone() . "', fax='" . $supplier->getFax() . "', ddi='" . $supplier->getDdi() . "', notes='" . $supplier->getNotes() . "' WHERE supplier_id = '" . $supplier->getSupplier_id() . "'";    
        
    } else {
        
        $query = "INSERT INTO supplier_David (supplier_id, supplier_origin_id, company_name, address, city, zip, country_iso, country, contact_name, email, phone, fax, ddi, notes) VALUES ('" . $supplier->getSupplier_id() . "', '" . $supplier->getSupplier_origin_id() . "', '" . $supplier->getCompany_name() . "', '" . $supplier->getAddress() . "', '" . $supplier->getCity() . "', '" . $supplier->getZip() . "',  '" . $supplier->getCountry_iso() . "',  '" . $supplier->getCountry() . "',  '" . $supplier->getContact_name() . "',  '" . $supplier->getEmail() . "',  '" . $supplier->getPhone() . "',  '" . $supplier->getFax() . "',  '" . $supplier->getDdi() . "',  '" . $supplier->getNotes() . "' )";
        
    }
    
    if (!$db->query($query)) {
        die($query);
        echo ("Some problem adding records");
    } else {
        
    }
    
}

/*
	Function used to check if one supplier exist or not
	
	@param $db Mysqli object connected to the db
	@param $supplier Supplier object to be compared
	
	@return true if exist the supplier | false if the supplier not exist
	
*/

function checkIfExist($db, $supplier)
{
    
    $query = "SELECT * FROM supplier_David WHERE supplier_id = '" . $supplier->getSupplier_id() . "'";
    
    $result = $db->query($query);
    
    while ($row = $result->fetch_assoc()) {
        return true;
    }
    
    return false;
    
}

?> 