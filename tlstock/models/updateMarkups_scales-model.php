<?php

require(__DIR__ . "/../controllers/Classes/Category.php");

$data      = json_decode($_POST["data"], true);
$tableName = "markupScale_David";
$flag = true;

for($k=0; $k<=count($data)-1; $k++){

	updateAttribute($db, $data[0]["value"], $data[$k]["name"], $data[$k]["value"], $tableName);

}

if($flag){
	echo("OK");
} else{
	echo("ERROR");
}

/* Functions */

/*
Function used to update an attribute on the database

@param $db Mysqli object connected to the db
@param $id Id of the category to be compared 
@param $attrName String that contains the name of the attribute to be updated
@param $attrVal String that contains the value of the attribute to be updated 
@param $tableName String that contains the name of the table in the database

@return Die if the query fails

*/

function updateAttribute($db, $id, $attrName, $attrVal, $tableName)
{
	
	global $flag;
	
    $query = "UPDATE " . $tableName . " SET " . $attrName . "='" . $attrVal . "' WHERE idMarkup = '" . $id . "'";
    
    if(!$db->query($query)){

		$flag = false;
    
    }
    
}




?>