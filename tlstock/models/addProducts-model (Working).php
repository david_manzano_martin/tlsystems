<?php

require(__DIR__ . "/../database/connection.php");
require(__DIR__ . "/../controllers/Classes/Product_tmp.php");
require(__DIR__ . "/../controllers/Classes/Test.php");

$arrayNewProducts = [];
$arrayUpdateProducts = [];
$arrayUpdateTmpProducts = [];
$arrayAllProducts = [];
$type = $_GET["type"];

$tableTmpName = "products_tmp_David";
$tableName = "products_David";



//die($_FILES["file"]["tmp_name"] . "/" . $_FILES["file"]["name"]);

switch($type) {

	case "array":{
		
		/* To read the CSV file and get the array with the data */
		$rows = array_map('str_getcsv', file($_FILES["file"]["tmp_name"]));
		$header = array_shift($rows);
		$csv = array();
		
		foreach ($rows as $row) {
  			$data[] = array_combine($header, $row);
		}
		
		

		for($k=0; $k<=count($data)-1;$k++) {

			$currentProduct = new Product_tmp ($data[$k]["Product Number"], $data[$k]["Manufacturer Number"], $data[$k]["Category"], "??", "??", "??", $data[$k]["Manufacturer"], "", $data[$k]["Cost"], $data[$k]["Description"], $data[$k]["Product Details"], "", "", $data[$k]["Main Image"]);
			
			
			
			if((!checkIfExistCSV($db, $currentProduct, $tableTmpName)) && (!checkIfExistCSV($db, $currentProduct, $tableName))){
				
				array_push($arrayNewProducts, $currentProduct);
				array_push($arrayAllProducts, $currentProduct->getProductName());
				
			}else if(checkIfExistCSV($db, $currentProduct, $tableTmpName)) {
				
				array_push($arrayUpdateTmpProducts, $currentProduct);
				array_push($arrayAllProducts, $currentProduct->getProductName());
				
			}else if(checkIfExistCSV($db, $currentProduct, $tableName)){
				
				array_push($arrayUpdateProducts, $currentProduct);
				array_push($arrayAllProducts, $currentProduct->getProductName());
				
			}
		
		}
		
		
		
		compareCsvToDb($db, $arrayAllProducts, $tableTmpName);
		compareCsvToDb($db, $arrayAllProducts, $tableName);
		
		
		
		for($k=0; $k<=count($arrayNewProducts)-1;$k++) {	

			addProduct($db, $arrayNewProducts[$k], $tableTmpName);
		
		}
		
		for($k=0; $k<=count($arrayUpdateTmpProducts)-1;$k++) {	

			updateProduct($db, $arrayUpdateTmpProducts[$k], $tableTmpName);
		
		}
		
		for($k=0; $k<=count($arrayUpdateProducts)-1;$k++) {	

			updateProduct($db, $arrayUpdateProducts[$k], $tableName);
		
		}

		break;
	
	}
	
	case "object":{

		$currentProduct = new Product_tmp ($data["Product Number"], $data["Manufacturer Number"], $data["Category"], "??", "??", "??", $data["Manufacturer"], "", $data["Cost"], $data["Description"], $data["Product Details"], "", "", $data["Main Image"]);
		
		addProduct($db, $currentProduct, $tableName);
	
	}

}

/* Functions */

function updateProduct($db,$product, $tableName){

	$query = "UPDATE " . $tableName . " SET name='" . $product->getProductName() . "', price_tax_exclude='" . $product->getPrice_tax_exclude() . "', reference='" . $product->getReference() . "', manufacturer='" . $product->getManufacturer() . "', weight='" . $product->getWeight() . "', quantity='" . $product->getQuantity() . "', shortDescription='" . $product->getShortDescription() . "', description='" . $product->getDescription() . "', tags='" . $product->getTags() . "', urlRewritten='" . $product->getUrlRewritten() . "', imageUrl='" . $product->getImageUrl() . "' WHERE name = '" . $product->getProductName() . "'";
	//die($query);
	if(!$db->query($query)){
		die("Error updating productson table " . $tableName);
	} else {
		echo("OK");	
	}

}


function addProduct($db, $product, $tableName){

	$query= "INSERT INTO " .$tableName . " (productId, name, categoriesId, price_tax_exclude, reference, supplier_id, manufacturer, weight, quantity, shortDescription, description, tags,urlRewritten, imageUrl) VALUES ('" . $product->getProductId() . "', '" . $product->getProductName() . "', '" . $product->getCategoriesId() . "', '" . $product->getPrice_tax_exclude() . "', '" . $product->getReference() . "', '" . $product->getSupplier_id() . "', '" . $product->getManufacturer() . "', '" . $product->getWeight() . "', '" . $product->getQuantity() . "', '" . $product->getShortDescription() . "', '" . $product->getDescription() . "', '" . $product->getTags() . "', '" . $product->getUrlRewritten() . "', '" . $product->getImageUrl() . "')";
	
	if(!$db->query($query)){

		die("Problem adding data to the db");
	
	} else{

		echo("OK");
	
	}
	
}

function compareCsvToDb($db, $arrayData, $tableName){

	$dbProducts = [];
	$arrayDiff = [];

	$query = "SELECT * FROM " . $tableName;
	
	
	$result= $db->query($query);
	
	while($row = $result->fetch_assoc()) {
			
			array_push($dbProducts, $row["name"]);
	}
	
	//$arrayDiff = array_diff($dbProducts, $arrayData);

	for($k=0; $k<=count($dbProducts)-1;$k++) {

		$flag = false;

		for($i=0; $i<=count($arrayData)-1;$i++){
			if($dbProducts[$k]==$arrayData[$i]) {
				//echo("Product " . $arrayData[$i]. " exist ");
				$flag = true;
			} else{
				//echo("Product " . $arrayData[$i]. " doesn't exist ");			
			}
			
		}
		
		if(!$flag){
			array_push($arrayDiff, $dbProducts[$k]);		
		} 
	
	}
	for($k=0; $k<=count($arrayDiff)-1;$k++) {
		$query = "UPDATE " . $tableName . " SET quantity = 0 WHERE  name = '" . $arrayDiff[$k] . "'";
		$db->query($query);
	}
		
}

function checkIfExistCSV($db, $product, $tableName){
	
	$query = "SELECT * FROM " . $tableName . " WHERE name = '" . $product->getProductName() . "'";
	$result = $db->query($query);
	
	while($row = $result->fetch_assoc()){
		return true;
	}
	
	return false;

}

?>