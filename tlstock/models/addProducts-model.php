<?php

require(__DIR__ . "/../database/connection.php");
require(__DIR__ . "/../controllers/Classes/Product_tmp.php");
require(__DIR__ . "/../controllers/Classes/Test.php");

error_reporting(1);

$arrayNewProducts = [];
$arrayUpdateProducts = [];
$arrayUpdateTmpProducts = [];
$arrayAllProducts = [];
$type = $_GET["type"];

$supplierId = $_GET["supplierId"];

$tableTmpName = "products_tmp_David";
$tableName = "products_David";

switch($type) {

	case "array":{		
		/* To read the CSV file and get the array with the data */
		$rows = array_map('str_getcsv', file($_FILES["file"]["tmp_name"]));
		//$rows = array_map('str_getcsv', file("spire.csv"));
		$header = array_shift($rows);
		$csv = array();
		
		foreach ($rows as $row) {
  			$data[] = array_combine($header, $row);
		}

		for($k=0; $k<=count($data)-1;$k++) {
			//echo("<br><br><br><br>Error on product " . ($k+2));
			//echo("<br>Length: " . checkV2($data[$k]["Description"]));
			//die();
			//die("");
			$flag = true;
					
			switch($supplierId) {

				case "TL04":{
					//$currentProduct = new Product_tmp ($data[$k]["Product Number"], (clean($data[$k]["Description"]) . "-" . $supplierId), $data[$k]["Category"], calculePrice($db, $data[$k]["Cost"], "markupScale_David"), $data[$k]["Product Number"], $supplierId, $data[$k]["Manufacturer"], "", $data[$k]["Stock Level"], $data[$k]["Description"], ($data[$k]["Product Details"] . $data[$k]["Specification"]), "", "", $data[$k]["Main Image"], $data[$k]["EAN Number"]);
					//$currentProduct = new Product_tmp ("null", (str_replace("'", "\'", $data[$k]["Description"]) . "-" . $supplierId), $data[$k]["Category"], calculePrice($db, $data[$k]["Cost"], "markupScale_David"), $data[$k]["ProductNumber"], $supplierId, $data[$k]["Manufacturer"], "", $data[$k]["StockLevel"], str_replace("'", "\'", $data[$k]["Description"]), ($data[$k]["Description"] . $data[$k]["ExtendedDetails"]), "", "", $data[$k]["MainImage"], $data[$k]["EANNumber"]);
					
					if(checkV2($data[$k]["Description"]) > 1) {
						$currentProduct = new Product_tmp ("null", (str_replace("'", "\'", $data[$k]["Description"]) . "-" . $supplierId), $data[$k]["Category"], calculePrice($db, $data[$k]["Cost"], "markupScale_David"), $data[$k]["ProductNumber"], $supplierId, $data[$k]["Manufacturer"], $data[$k]["Weight"], $data[$k]["StockLevel"], str_replace("'", "\'", $data[$k]["Description"]), ($data[$k]["Description"] . $data[$k]["ExtendedDetails"]), "", "", $data[$k]["MainImage"], $data[$k]["EANNumber"]);
						//echo(" | Position " . $k . " OK");
						$flag = true;
					} else{
						$flag = false;
						unset($data[$k]);
						unset($data[($k + 1)]);
						$data = array_values($data);
						//echo(" | Position " . $k . " Error");
					}
					
					break;
				
				}
				
				/* THIS IS THE STRUCTURE OF ALL THE CSV FORMATS */
				default:{

					if(checkV2($data[$k]["description"]) > 1) {
						$currentProduct = new Product_tmp ("null", str_replace("'", "\'", $data[$k]["description"] . "-" . $supplierId), str_replace("'", "\'", $data[$k]["Category"]), str_replace("'", "\'", calculePrice($db, $data[$k]["price"], "markupScale_David")), $data[$k]["stockcode"], str_replace("'", "\'", $supplierId), str_replace("'", "\'", $data[$k]["manufacturer"]),  str_replace("'", "\'", $data[$k]["weight"]), clean(str_replace("'", "\'", $data[$k]["stock"])), str_replace("'", " ", $data[$k]["description"]), str_replace("'", "\'", $data[$k]["extendeddescription"]), "", "", str_replace("'", "\'", $data[$k]["largeimageurl"]));					
					} else{
						$flag = false;
						/*unset($data[$k]);
						unset($data[($k + 1)]);*/
						//$data = array_values($data);
						//echo(" | Position " . $k . " Error");
					}
					
					
					
					break;
				
				}
			
			}
			
			
			//print_r($currentProduct);
			
			if($flag){
				if((!checkIfExistCSV($db, $currentProduct, $tableTmpName)) && (!checkIfExistCSV($db, $currentProduct, $tableName))){
				
				array_push($arrayNewProducts, $currentProduct);
				array_push($arrayAllProducts, $currentProduct);
				
			}else if(checkIfExistCSV($db, $currentProduct, $tableTmpName)) {
				
				array_push($arrayUpdateTmpProducts, $currentProduct);
				array_push($arrayAllProducts, $currentProduct);
				
			}else if(checkIfExistCSV($db, $currentProduct, $tableName)){
				
				array_push($arrayUpdateProducts, $currentProduct);
				array_push($arrayAllProducts, $currentProduct);
				
			}			
			} else{
				echo("No entra");
			}

		
		}
		
		echo("Out");
		
		compareCsvToDb($db, $arrayAllProducts, $tableTmpName);
		compareCsvToDb($db, $arrayAllProducts, $tableName);
		
		for($k=0; $k<=count($arrayNewProducts)-1;$k++) {	

			addProduct($db, $arrayNewProducts[$k], $tableTmpName);
		
		}
		
		for($k=0; $k<=count($arrayUpdateTmpProducts)-1;$k++) {	

			updateProduct($db, $arrayUpdateTmpProducts[$k], $tableTmpName);
		
		}
		
		for($k=0; $k<=count($arrayUpdateProducts)-1;$k++) {	

			updateProduct($db, $arrayUpdateProducts[$k], $tableName);
		
		}

		break;
	
	}
	
	case "object":{

		$currentProduct = new Product_tmp ($data["Product Number"], $data["Manufacturer Number"], $data["Category"], "??", "??", "??", $data["Manufacturer"], "", $data["Cost"], $data["Description"], $data["Product Details"], "", "", $data["Main Image"]);
		
		addProduct($db, $currentProduct, $tableName);
		break;
	
	}

}

/* Functions */

function checkV2($val){

	$value = $val;
	return strlen($value);

}

function check($value = null){

	if($value != null){

		return true;
	
	} else{
		return false;	
	}

}

/*
	Function used to update a product receiving the database name
	
	@param $db Mysqli object connected to the db
	@param $product Product_tmp object
	@param $tableName String that contains the name of the table in the database to update the product
	
*/

function updateProduct($db,$product, $tableName){
	
	$date = date("Y-m-d");

	$query = "UPDATE " . $tableName . " SET name='" . $product->getProductName() . "', price_tax_exclude='" . $product->getPrice_tax_exclude() . "', reference='" . $product->getReference() . "', manufacturer='" . $product->getManufacturer() . "', weight='" . $product->getWeight() . "', quantity='" . $product->getQuantity() . "', shortDescription='" . $product->getShortDescription() . "', description='" . $product->getDescription() . "', tags='" . $product->getTags() . "', urlRewritten='" . $product->getUrlRewritten() . "', imageUrl='" . $product->getImageUrl() . "', supplier_id='" . $product->getSupplier_id() . "', dateAdded = '" . $date . "' WHERE name = '" . $product->getProductName() . "'";
	
	if(!$db->query($query)){
		die("Error updating productson table " . $tableName);
	} else {
		echo("OK1");	
	}

}

/*
	Function used to add a product to the database
	
	@param $db Mysqli object connected to the db
	@param $product Product_tmp object
	@param $tableName String that contains the name of the table in the database to add the product
	
*/


function addProduct($db, $product, $tableName){

	$query= "INSERT INTO " . $tableName . " (productId, name, categoriesId, price_tax_exclude, reference, supplier_id, manufacturer, weight, quantity, shortDescription, description, tags,urlRewritten, imageUrl, dateAdded, EanNumber) VALUES ('NULL', '" . $product->getProductName() . "', '" . $product->getCategoriesId() . "', '" . $product->getPrice_tax_exclude() . "', '" . $product->getReference() . "', '" . $product->getSupplier_id() . "', '" . $product->getManufacturer() . "', '" . $product->getWeight() . "', '" . $product->getQuantity() . "', '" . $product->getShortDescription() . "', '" . $product->getDescription() . "', '" . $product->getTags() . "', '" . $product->getUrlRewritten() . "', '" . $product->getImageUrl() . "', '" . date("Y-m-d") . "', '" . $product->getEanNumber() . "')";
	
	if(!$db->query($query)){
		echo("Error adding product " . $product->getProductName());
	} else {
		echo("OK1");	
	}
	
}

/*
	Function used to check if all the products allowed on the db has to be updated or not, if has to be, put the stock to 0 
	
	@param $db Mysqli object connected to the db
	@param $arrayData Array with all the products allowed on the CSV file
	@param $tableName String that contains the name of the table in the database
	
*/


function compareCsvToDb($db, $arrayData, $tableName){

	$dbProducts = [];
	$arrayDiff = [];
	$date = date("Y-m-d");
	global $supplierId;

	$query = "SELECT * FROM " . $tableName . " WHERE supplier_id = '" . $supplierId . "'";
	
	$result= $db->query($query);
	
	while($row = $result->fetch_assoc()) {

			$localObject = new stdClass();
			$localObject->name = $row["name"];
			$localObject->date = $row["dateAdded"];
			
			array_push($dbProducts, $localObject);
			
	}
	

	for($k=0; $k<=count($dbProducts)-1;$k++) {

		$flag = false;

		for($i=0; $i<=count($arrayData)-1;$i++){
			if($dbProducts[$k]->name==$arrayData[$i]->getProductName()) {
				$flag = true;
			}
			if($dbProducts[$k]->date == $date){
					$flag = true;
				}			
		}
		
		if(!$flag){
			
			array_push($arrayDiff, $dbProducts[$k]->name);		
		} 
	
	}
	
	for($k=0; $k<=count($arrayDiff)-1;$k++) {
		$query = "UPDATE " . $tableName . " SET quantity = 0 WHERE name = '" . $arrayDiff[$k] . "'";
		$db->query($query);
		$query = "UPDATE " . $tableName . " SET dateAdded = '" . date("Y-m-d") . "' WHERE name = '" . $arrayDiff[$k] . "'";
		$db->query($query);
	}
	
}

function calculePrice($db, $priceBefore, $tableName){

	$query = "SELECT *  FROM " . $tableName . " WHERE maxQuantity >= " . $priceBefore . " ORDER BY maxQuantity  ASC";
	$result = $db->query($query);
	
	while($row = $result->fetch_assoc()){

		$price = ($row["increment"] * $priceBefore) / 100;
		return $price + $priceBefore;
		
	}

}

/*
	Function used to check if the object exist on the database, search by reference
	
	@param $db Mysqli object connected to the db
	@param $product Product_tmp object
	@param $tableName String that contains the name of the table in the database
	
	@return true if exist | false if not exist
	
*/


function checkIfExistCSV($db, $product, $tableName){
	
	$query = "SELECT * FROM " . $tableName . " WHERE reference = '" . $product->getReference() . "'";
	
	$result = $db->query($query);
	
	while($row = $result->fetch_assoc()){
		return true;
	}
	
	return false;

}

function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   //$string = str_replace('+', ' ', $string); // Replaces all spaces with hyphens.
   $string = str_replace("'", ' ', $string); // Replaces all spaces with hyphens.
   $string = preg_replace('/[^A-Z+-a-z0-9\-]/', '', $string); // Removes special chars.
   $string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
   return preg_replace('/-+/', ' ', $string);
}

?>