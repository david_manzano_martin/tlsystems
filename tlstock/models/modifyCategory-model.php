<?php

require(__DIR__ . "/../controllers/Classes/Category.php");

$data      = json_decode($_POST["data"], true);
$tableName = "category_David";

$category = getCategoryById($db, $data[0]["value"], $tableName);

for ($k = 0; $k <= count($data) - 1; $k++) {
    updateAttribute($db, $category->getCategory_id(), $data[$k]["name"], str_replace("'", "\'", $data[$k]["value"]), $tableName);
}

/* Functions */

/*
Function used to get the category searching by the id

@param $db Mysqli object connected to the db
@param $id Id of the category to be compared 
@param $tableName String that contains the name of the table in the database

@return The category object with the data on the database

*/

function getCategoryById($db, $id, $tableName)
{
    
    $query  = "SELECT * FROM " . $tableName . " WHERE category_id = " . $id;
    $result = $db->query($query);
    
    while ($row = $result->fetch_assoc()) {
        
        $currentCategory = new Category($row["category_id"], $row["presto_shop_id"], $row["category_name"]);
        
    }
    
    return $currentCategory;
    
}

/*
Function used to update an attribute on the database

@param $db Mysqli object connected to the db
@param $id Id of the category to be compared 
@param $attrName String that contains the name of the attribute to be updated
@param $attrVal String that contains the value of the attribute to be updated 
@param $tableName String that contains the name of the table in the database

@return Die if the query fails

*/

function updateAttribute($db, $id, $attrName, $attrVal, $tableName)
{
    
    $query = "UPDATE " . $tableName . " SET " . $attrName . "='" . $attrVal . "' WHERE category_id = '" . $id . "'";
    
    if (!$db->query($query)) {
        
        die("Error " . $query);
        
    }
    
}

?>