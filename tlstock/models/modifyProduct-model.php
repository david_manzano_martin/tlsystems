 <?php

require(__DIR__ . "/../controllers/Classes/Product_tmp.php");

$tableName    = "products_David";
$tableTmpName = "products_tmp_David";
$action       = $_GET["action"];
$name = $_GET["name"];

switch ($action) {
    
    case "move": {
    	
    	  $data = json_decode($_GET["data"]);
    	  
        
        $product = getProductByName($db, $data->productNumber, $tableTmpName);
                
        moveProduct($db, $product, $tableTmpName, $tableName); 
        
        updateAttribute($db, $product, "categoriesId", $data->categoryCode, $tableName);
        
        break;
        
    }
    
    case "update": {
		
		$data = json_decode($_POST["data"]);
		
    		$product = getProductByName($db, $data[0]->value, $tableName);

    		
				require(__DIR__ . "/../controllers/bugProducts-controller.php");
			

    		for($k=0; $k<=count($data)-1; $k++){

				updateAttribute($db, $product, $data[$k]->name, str_replace("'", "\'", $data[$k]->value), $tableName);
    		
    		}
        
        break;
        
    }
        
}




/* Functions */
function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
   $string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
   return preg_replace('/-+/', ' ', $string);
}

/*
Function used to update an attribute on the database

@param $db Mysqli object connected to the db
@param $id Id of the category to be compared 
@param $attrName String that contains the name of the attribute to be updated
@param $attrVal String that contains the value of the attribute to be updated 
@param $tableName String that contains the name of the table in the database

@return Die if the query fails

*/

function updateAttribute($db, $product, $attrName, $attrVal, $tableName)
{

    $query = "UPDATE " . $tableName . " SET " . $attrName . "='" . $attrVal . "' WHERE name = '" . $product->getProductName() . "'";
    
    if(!$db->query($query)){

		die("Error". $query);
    
    }
    
}

/*
Function used to move a product from one to other table

@param $db Mysqli object connected to the db
@param $product Product object to be moved 
@param $tableFrom String that contains the name of the origin table
@param $tableTo String that contains the name of the destination table 

*/

function moveProduct($db, $product, $tableFrom, $tableTo)
{
	
	/*$query3 = "SELECT * FROM products_tmp_David WHERE name = '" . str_replace("'", "\'", $product->getProductName()) . "'";
	$result3 = $db->query($query3);
	while($row = $result3->fetch_assoc()){
		$name = str_replace("'", "\'", $row["name"]);
	}*/
	
    /*$query = "DELETE FROM " . $tableFrom . " WHERE name = '" . $product->getProductName() . "'";
    
    $db->query($query);
    echo($query . "      ");*/
    
   $query = ("INSERT INTO products_David (name, categoriesId, price_tax_exclude, reference, supplier_id, manufacturer, weight, quantity, shortDescription, description, tags, urlRewritten, imageUrl, dateAdded, EanNumber)
	SELECT name, categoriesId, price_tax_exclude, reference, supplier_id, manufacturer, weight, quantity, shortDescription, description, tags, urlRewritten, imageUrl, dateAdded, EanNumber
	FROM products_tmp_David WHERE name = '" . str_replace("'", "\'", $product->getProductName()) . "'");
	
	//die($query);
    
    //$query = "INSERT INTO " . $tableTo . " (productId, name, categoriesId, price_tax_exclude, reference, supplier_id, manufacturer, weight, quantity, shortDescription, description, tags,urlRewritten, imageUrl, dateAdded, EanNumber) VALUES ('" . $product->getProductId() . "', '" . $product->getProductName() . "', '" . $product->getCategoriesId() . "', '" . $product->getPrice_tax_exclude() . "', '" . $product->getReference() . "', '" . $product->getSupplier_id() . "', '" . $product->getManufacturer() . "', '" . $product->getWeight() . "', '" . $product->getQuantity() . "', '" . $product->getShortDescription() . "', '" . $product->getDescription() . "', '" . $product->getTags() . "', '" . $product->getUrlRewritten() . "', '" . $product->getImageUrl() . "', '" . date("Y-m-d") . "', '" . $product->getEanNumber() . "')";
    
    $db->query($query);
    
    $query = "DELETE FROM " . $tableFrom . " WHERE name = '" . str_replace("'", "\'", $product->getProductName()) . "'";
    
    $db->query($query);
    
    //echo($query);
    
    
    
}

/*
Function used to get a product searching by his name

@param $db Mysqli object connected to the db
@param $name String that contains the name of the product 
@param $tableName String that contains the name of the table 

@return The Product_tmp object searched

*/

function getProductByName($db, $name, $tableName)
{
	$name = htmlspecialchars_decode($name);
	$name = str_replace("'", "\'", $name);
	
    $query = "SELECT * FROM " . $tableName . " WHERE name = '" . $name . "'";
    $result = $db->query($query);
    
    
    while ($row = $result->fetch_assoc()) {
        
        $currentProduct = new Product_tmp($row["productId"], $row["name"], $row["categoriesId"], $row["price_tax_exclude"], $row["reference"], $row["supplier_id"], $row["manufacturer"], $row["weight"], $row["quantity"], $row["shortDescription"], $row["description"], $row["tags"], $row["urlRewritten"], $row["imageUrl"], $row["EanNumber"]);
        
    }
    
    return $currentProduct;
    
}

?> 