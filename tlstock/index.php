<?php

require(__DIR__ . "/database/connection.php");

(!isset($_SERVER['REDIRECT_URL'])) ? $request = $_SERVER['REDIRECT_URL']="/" :"" ;
$option = $_GET["option"];
$localUrl = "tlstock";

$request = $_SERVER['REDIRECT_URL'];

/* Creating all options */
$optionsArray = [
	[
      "title"   => "Convert XML to CSV",
      "url" => "?option=convertXml"
    ],
    [
      "title"   => "Import Suppliers",
      "url" => "?option=import_suppliers"
    ],
    [
      "title"   => "Add Categories",
      "url" => "?option=import_categories"
    ],
    
    [
      "title"   => "Markups scales",
      "url" => "?option=markups_scales"
    ],
    [
      "title"   => "Import Products",
      "url" => "?option=import_products"
    ],
    [
      "title"   => "View Products",
      "url" => "?option=view_products"
    ],
    /*[
      "title"   => "Export data by supplier",
      "url" => "?option=export_data_supplier"
    ],*/
    [
      "title"   => "Export all data",
      "url" => "?option=export_data"
    ]
  ];

switch($option) {

	case "convertXml":{
		require(__DIR__ . "/controllers/convertXml-controller.php");
		break;
	}
	
	case "import_suppliers":{
		require(__DIR__ . "/controllers/import_suppliers-controller.php");
		break;
	}
	
	case "import_suppliers_model":{
		require(__DIR__ . "/models/addSuppliers-model.php");
		break;	
	}
	case 'import_categories':{
		require(__DIR__ . "/controllers/import_categories-controller.php");
		break;
	}
	case "modify_category":{
		require(__DIR__ . "/models/modifyCategory-model.php");		
		break;
	}
	case "add_category":{
		require(__DIR__ . "/models/addCategory-model.php");		
		break;
	}
	case "markups_scales":{
		require(__DIR__ . "/controllers/markups_scales-controller.php");
		break;
	}
	case "modifyMarkups_scales":{
		require(__DIR__ . "/models/updateMarkups_scales-model.php");
		break;
	}
		break;	
	case "modify_Product":{
		require(__DIR__ . "/models/modifyProduct-model.php");		
		break;	
	}
	
	case "import_products": case 'view_products':{
		require(__DIR__ . "/controllers/import_products-controller.php");
		break;	
	}
	
	case "import_products_model":{
		require(__DIR__ . "/models/addProducts-model.php");
		break;	
	}
	
	case "view_product":{
		require(__DIR__ . "/controllers/view_product-controller.php");
		break;	
	}
	case "export_data_supplier":{
		require(__DIR__ . "/controllers/export_data_supplier-controller.php");
		break;	
	}
	case "export_data":{
		require(__DIR__ . "/controllers/export_data-controller.php");
		break;	
	}
	case"kk":{
		require(__DIR__ . "/controllers/test.php");
		break;
	}
	default:{
		require(__DIR__ . "/controllers/main-controller.php");
		break;	
	}

}


?>